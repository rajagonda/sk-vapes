<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductExtraInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_extra_information', function (Blueprint $table) {
            $table->increments('pe_id');
            $table->string('pe_title')->nullable();
            $table->longText('pe_description')->nullable();
            $table->string('pe_image')->nullable();
            $table->integer('pe_position')->nullable();
            $table->integer('pe_product_id');
            $table->string('pe_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_extra_information');
    }
}
