<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('order_id');
            $table->integer('order_user_id');
            $table->longText('order_delivery_address');
            $table->string('order_status')->default('Pending');
            $table->string('order_shipping_tarck_id')->nullable();
            $table->date('order_delivery_date')->nullable();
            $table->float('order_total_price');
            $table->string('order_reference_number')->nullable();
            $table->string('order_transaction_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
