<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->increments('oitem_id');
            $table->integer('oitem_order_id');
            $table->integer('oitem_product_id');
            $table->string('oitem_product_name');
            $table->string('oitem_product_spl');
            $table->integer('oitem_qty');
            $table->float('oitem_product_price');
            $table->float('oitem_sub_total');
            $table->float('oitem_discount_price')->default(0);
            $table->float('oitem_shipping_price')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
