<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('product_id');
            $table->string('product_name');
            $table->string('product_alias');
            $table->string('product_type')->nullable();
            $table->string('product_availability')->default(0);
            $table->string('product_color')->nullable();
            $table->longText('product_description')->nullable();
            $table->float('product_real_price')->default(0);
            $table->float('product_price')->default(0);
            $table->integer('product_brand')->nullable();
            $table->integer('product_country')->nullable();
            $table->integer('product_category')->nullable();
            $table->float('product_shipping_price')->nullable()->default(0);
            $table->string('product_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
