const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

// images
mix.copyDirectory('resources/assets/backend/images', 'public/backend/images');

// Modules
mix.js('resources/assets/backend/js/main.js', 'public/backend/assets/js')
    .js('resources/assets/backend/js/dashboard.js', 'public/backend/assets/js')
    .js('resources/assets/backend/modules/countries/js/countries.js', 'public/backend/modules/countries/js/')
    .js('resources/assets/backend/modules/banners/js/banners.js', 'public/backend/modules/banners/js/')
    .js('resources/assets/backend/modules/banners/js/footerbanners.js', 'public/backend/modules/banners/js/')
    .js('resources/assets/backend/modules/brands/js/brands.js', 'public/backend/modules/brands/js/')
    .js('resources/assets/backend/modules/products/js/products.js', 'public/backend/modules/products/js/')
    .js('resources/assets/backend/modules/categories/js/categories.js', 'public/backend/modules/categories/js/');
// styles
mix.sass('resources/assets/backend/scss/theme.scss', 'public/backend/assets/css');

mix.styles([
    'resources/assets/backend/css/normalize.css',
    'resources/assets/backend/css/cs-skin-elastic.css',
    'resources/assets/backend/css/themify-icons.css',
    'resources/assets/backend/assets/css/flag-icon.min.css'
], 'public/backend/assets/css/all.css');

// frontend

// images
mix.copyDirectory('resources/assets/frontend/img', 'public/frontend/img');

// mix.styles([
//
//     // 'resources/assets/frontend/css/easy-responsive-tabs.scss',
//     // 'resources/assets/frontend/css/html-5-reset-css.scss',
//     // 'resources/assets/frontend/css/jquery-steps.scss',
//     // 'resources/assets/frontend/css/vertical_carousal.scss',
//     'resources/assets/frontend/css/style.scss',
//
//     // 'resources/assets/frontend/css/notifIt.scss'
// ], 'public/frontend/assets/css/all.css');

mix.sass('resources/assets/frontend/css/style.scss', 'public/frontend/assets/css');
// Modules
mix.js('resources/assets/frontend/js/main.js', 'public/frontend/assets/js')
    .js('resources/assets/frontend/modules/users/js/changePassword.js', 'public/frontend/modules/users/js/')
    .js('resources/assets/frontend/modules/users/js/login.js', 'public/frontend/modules/users/js/')
    .js('resources/assets/frontend/modules/users/js/register.js', 'public/frontend/modules/users/js/')
    .js('resources/assets/frontend/modules/users/js/users.js', 'public/frontend/modules/users/js/');