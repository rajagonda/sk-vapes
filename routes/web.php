<?php
Auth::routes();

/* Home routes */

Route::get('/{locale?}', 'frontend\HomeController@index')->name('home');

/* login module routes */

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('userelogin');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('userlogout');
// Registration Routes...

$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('register', 'Auth\RegisterController@register');
// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset.token');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
//Ajax?/*Route*/

Route::match(array('GET', 'POST'), '/api/ajax/editor/imageupload', 'Controller@editorimageupload')->name('editorImageupload');
// Save Restrictions
Route::match(array('GET', 'POST'), '/ajax/save/restrictions', 'frontend\HomeController@saveRestrictions')->name('saveRestrictions');
// Add to Cart
Route::match(array('GET', 'POST'), '/ajax/addToCart', 'frontend\HomeController@addToCart')->name('addToCart');
// Add to Cart for only one product
Route::match(array('GET', 'POST'), '/product/addTocart', 'frontend\HomeController@buyProduct')->name('buyProduct');
// Remove From Cart
Route::match(array('GET', 'POST'), '/ajax/remove/item/', 'frontend\HomeController@removeItemFromCart')->name('removeItemFromCart');
// update From Cart
Route::match(array('GET', 'POST'), '/ajax/update/item/', 'frontend\HomeController@updateItemFromCart')->name('updateItemFromCart');
Route::match(array('GET', 'POST'), '/ajax/addToWishlist', 'frontend\UserController@addToWishList')->name('addToWishList');
Route::post('/ajax/deleteuserimage', 'frontend\UserController@deleteUserimage')->name('deleteUserimage');
Route::post('/ajax/save/subscribers', 'frontend\HomeController@saveSubscribers')->name('saveSubscribers');
// Product List
Route::match(array('GET', 'POST'), '/brand/products/list/{brand?}', 'frontend\HomeController@userBrandproducts')->name('userBrandproducts');
// Brand Product List
Route::match(array('GET', 'POST'), '/category/{category}/{brand?}', 'frontend\HomeController@products')->name('userproducts');
// Product Overview
Route::match(array('GET', 'POST'), '/product/overview/{product_alias}', 'frontend\HomeController@productOverview')->name('productOverview');
// Product Details
Route::match(array('GET', 'POST'), '/product/view/{product_alias}', 'frontend\HomeController@productDetails')->name('productDetails');
// Cart Details
Route::match(array('GET', 'POST'), '/cart/list', 'frontend\HomeController@cartDetails')->name('cartDetails');
// Buy Individual product
Route::match(array('GET', 'POST'), '/buy/product', 'frontend\HomeController@buyNow')->name('buyNow');
// Order Confirmation Details
Route::match(array('GET', 'POST'), '/order/confirmation', 'frontend\UserController@orderConfirmation')->name('orderConfirmation');
// Support
Route::match(array('GET', 'POST'), '/page/contact-us', 'frontend\HomeController@support')->name('support');
Route::match(array('GET', 'POST'), '/page/careers', 'frontend\HomeController@careers')->name('careers');
Route::match(array('GET', 'POST'), '/page/wholesale-opportunity', 'frontend\HomeController@WholesaleOpportunity')->name('WholesaleOpportunity');
Route::match(array('GET', 'POST'), '/page/warranty', 'frontend\HomeController@warranty')->name('warranty');
Route::match(array('GET', 'POST'), '/page/shipping', 'frontend\HomeController@shipping')->name('shipping');
Route::match(array('GET', 'POST'), '/page/about', 'frontend\HomeController@about')->name('about');
Route::match(array('GET', 'POST'), '/page/faq', 'frontend\HomeController@faq')->name('faq');
Route::match(array('GET', 'POST'), '/page/privacy', 'frontend\HomeController@privacy')->name('privacy');
Route::match(array('GET', 'POST'), '/page/terms', 'frontend\HomeController@terms')->name('terms');

// search product

Route::get('/ajax/search_product', 'frontend\HomeController@search')->name('searchProductInfo');


Route::prefix('user')->group(function () {
    // Add to wish list
    Route::match(array('GET', 'POST'), '/profile', 'frontend\UserController@index')->name('userprofile');
    Route::match(array('GET', 'POST'), '/change/password', 'frontend\UserController@changePassword')->name('userChangePassword');
    Route::match(array('GET', 'POST'), '/orders', 'frontend\UserController@orders')->name('orders');
    Route::match(array('GET', 'POST'), '/order/details/{order}', 'frontend\UserController@orderDetails')->name('orderDetails');
    Route::match(array('GET', 'POST'), '/invoice/details/{order}', 'frontend\UserController@orderInvoice')->name('orderInvoice');
    Route::match(array('GET', 'POST'), '/unprocessed/orders', 'frontend\UserController@unProcessedOrders')->name('unProcessedOrders');
    Route::match(array('GET', 'POST'), '/addres/book', 'frontend\UserController@userAddressBook')->name('userAddressBook');
    Route::match(array('GET', 'POST'), '/manage/user/addres/book', 'frontend\UserController@userAddAddressBook')->name('userAddAddressBook');
    Route::match(array('GET', 'POST'), '/wish/list', 'frontend\UserController@wishList')->name('wishList');
    Route::match(array('GET', 'POST'), '/payment/confirmation', 'frontend\UserController@paymentConfirmation')->name('paymentConfirmation');
    Route::match(array('GET', 'POST'), '/orders/save', 'frontend\UserController@saveOredrs')->name('saveOredrs');
    Route::match(array('GET', 'POST'), '/pay/with/paypal', 'frontend\PaymentController@payWithpaypal')->name('paywithpaypal');
    Route::match(array('GET', 'POST'), '/payment/status/save/', 'frontend\PaymentController@getPaymentStatus')->name('savePaymentStatus');
    Route::match(array('GET', 'POST'), '/make/default/addres', 'frontend\UserController@makeDefaultAddress')->name('makeDefaultAddress');
});
// Error pages
Route::get('errors/404', ['as' => '404', 'uses' => 'ErrorHandlerController@errorCode404']);
Route::get('errors/500', ['as' => '500', 'uses' => 'ErrorHandlerController@errorCode500']);
Route::prefix('admin')->group(function () {
    /* login module routes */
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Auth\LoginController@login');
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
    /* Dashboard routes */
    Route::get('/dashboard', 'backend\AdminController@index')->name('dashboard');
    /* Change Password */
    Route::match(array('GET', 'POST'), '/change/password', 'backend\AdminController@changePassword')->name('changePassword');
    /* update Profile */
    Route::match(array('GET', 'POST'), '/update/profile', 'backend\AdminController@profile')->name('profile');
    /* Countries module routes */
    Route::match(array('GET', 'POST'), '/countries', 'backend\CountriesController@index')->name('countries');
    Route::match(array('GET', 'POST'), '/countries/manage/{id?}', 'backend\CountriesController@addNewCountries')->name('addNewCountries');
    /* Banners module routes */
    Route::match(array('GET', 'POST'), '/banners', 'backend\BannersController@index')->name('banners');
    Route::match(array('GET', 'POST'), '/banners/manage/{id?}', 'backend\BannersController@addNewBanners')->name('addNewBanners');
    Route::post('ajax/deletebannerimage', 'backend\BannersController@deleteBannerimage')->name('deleteBannerimage');
    /* Footer Banners module routes */
    Route::match(array('GET', 'POST'), '/footer/banners', 'backend\FooterBannersController@index')->name('footerBanners');
    Route::match(array('GET', 'POST'), '/footer/banners/manage/{id?}', 'backend\FooterBannersController@addNewBanners')->name('addNewFooterBanners');
    Route::post('ajax/deletefooterbannerimage', 'backend\FooterBannersController@deleteBannerimage')->name('deleteFooterBannerimage');
    /* Brands module routes */
    Route::match(array('GET', 'POST'), '/brands', 'backend\BrandsController@index')->name('brands');
    Route::match(array('GET', 'POST'), '/brands/manage/{id?}', 'backend\BrandsController@addNewBrands')->name('addNewBrands');
    Route::post('ajax/deletebrandimage', 'backend\BrandsController@deleteBrandimage')->name('deleteBrandimage');

    /* Categories module routes */
    Route::match(array('GET', 'POST'), '/categories', 'backend\CategoriesController@index')->name('categories');
    Route::match(array('GET', 'POST'), '/categories/manage/{id?}', 'backend\CategoriesController@addNewCategories')->name('addNewCategories');
    Route::post('ajax/deletecategoryimage', 'backend\CategoriesController@deleteCategoryimage')->name('deleteCategoryimage');
    /* Products module routes */
    Route::match(array('GET', 'POST'), '/products', 'backend\ProductsController@index')->name('products');
    Route::match(array('GET', 'POST'), '/products/manage/{id?}', 'backend\ProductsController@addNewProducts')->name('addNewProducts');
    Route::post('ajax/deleteproductimage', 'backend\ProductsController@deleteproductimage')->name('deleteproductimage');
    /* Products Extra info module routes */
    Route::match(array('GET', 'POST'), '/products/extraInfo/{id?}', 'backend\ProductsController@productExtraInfo')->name('productExtraInfo');

    Route::match(array('GET', 'POST'), '/products/extraInfo/manage/{id?}/{extraid?}', 'backend\ProductsController@addProductExtraInfo')->name('addProductExtraInfo');

    Route::post('ajax/deleteproductextraimage', 'backend\ProductsController@deleteProductExtraimage')->name('deleteProductExtraimage');
    /* Users module routes */
    Route::match(array('GET', 'POST'), '/users', 'backend\UsersController@index')->name('users');
    /* Users Address module routes */
    Route::match(array('GET', 'POST'), '/user/Address/{id}', 'backend\UsersController@userAddress')->name('userAddress');
    /* Users Orders module routes */
    Route::match(array('GET', 'POST'), '/user/orders/{id}', 'backend\UsersController@userOrders')->name('userAdminOrders');
    /* Users Orders module routes */
    Route::match(array('GET', 'POST'), '/user/wishlist/{id}', 'backend\UsersController@userWishlist')->name('userWishlist');
    /* Orders module routes */
    Route::match(array('GET', 'POST'), '/orders', 'backend\UsersController@orders')->name('adminorders');
    /* Product images routes */
    Route::match(array('GET', 'POST'), '/product/images/{id?}', 'backend\ProductsController@addProductImages')->name('addProductImages');
    /* News Letters routes */
    Route::get('/news/letters', 'backend\AdminController@newsLetters')->name('newsLetters');
    /* Contact Us routes */
    Route::get('/contact/us', 'backend\AdminController@contactus')->name('contactus');
});

