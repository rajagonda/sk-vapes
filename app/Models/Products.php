<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'product_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['product_name', 'product_alias','product_type', 'product_availability', 'product_color', 'product_size','product_description', 'product_price', 'product_country', 'product_category', 'product_shipping_price', 'product_status', 'product_real_price', 'product_brand','product_image','product_is_best'];

    public function getCategory()
    {
        return $this->belongsTo(Categories::Class, 'product_category');
    }

    public function getCountry()
    {
        return $this->belongsTo(Countries::Class, 'product_country');
    }

    public function productImages()
    {
        return $this->hasMany(ProductImages::Class, 'pi_product_id', 'product_id');
    }
}
