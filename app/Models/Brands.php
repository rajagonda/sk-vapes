<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brands extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'brands';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'brand_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['brand_name', 'brand_alias', 'brand_catid', 'brand_country', 'brand_image', 'brand_status','brand_future','brand_order','brand_homepage_title'];

    public function getCountry()
    {
        return $this->belongsTo(Countries::Class, 'brand_country');
    }
    public function getCategory()
    {
        return     $this->belongsTo(Categories::Class, 'brand_catid');
    }

}
