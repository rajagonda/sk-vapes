<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductExtraInformation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_extra_information';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'pe_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['pe_title', 'pe_description', 'pe_image', 'pe_position', 'pe_product_id', 'pe_status'];
}
