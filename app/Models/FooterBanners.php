<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FooterBanners extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'footer_banners';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'banner_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['banner_title', 'banner_image', 'banner_link', 'banner_country', 'banner_status'];

    public function getCountry()
    {
        return $this->belongsTo(Countries::Class,'banner_country');
    }
}
