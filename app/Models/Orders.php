<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'order_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['order_user_id', 'order_delivery_address', 'order_status', 'order_shipping_tarck_id', 'order_delivery_date', 'order_total_price', 'order_transaction_id', 'order_reference_number'];

    public function orderItems()
    {
        return $this->hasMany(OrderItems::Class, 'oitem_order_id', 'order_id');
    }
}
