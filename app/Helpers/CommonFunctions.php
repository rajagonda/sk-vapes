<?php

define('ADMIN_EMAIL', 'info@idconnect.com');
function availability($flag = null)
{

    $return_data =
        array('in_stock' => 'In Stock', 'out_of_stock' => 'Out Of Stock');
    if ($flag) {
        $return_data = $return_data[$flag];
    }
    return $return_data;

}

function allStatuses($module, $status = null)
{
    $return_data = "";
    $displayStatus =
        array(
            "general" => array('active' => 'Active', 'inactive' => 'Inactive'),
            "order" => array('Pending' => 'Pending', 'Completed' => 'Completed', 'Cancel' => 'Cancel'),
        );

    if ($module && $status) {
        $return_data = $displayStatus[$module][$status];
    } else {
        $return_data = $displayStatus[$module];
    }
    return $return_data;

}

function getBreadcrumbs($parameters, $title, $url = null)
{
    if ($url) {
        $img = $url;
    } else {
        $img = '/plugins/images/heading-title-bg.jpg';
    }
    $html = '<div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1> ' . $title . '</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">';
    foreach ($parameters as $key => $value) {
        if (is_array($value)) {

            $route = $key;
            $name = '';
            $options = array();

            foreach ($value as $akey => $avalue) {
                if ($akey == 'name') {
                    $name = $avalue;
                } else {
                    $options = $avalue;
                }
            }

            $url = route($route, $options);

            if (!next($parameters)) {
                $html .= '<li class="active">' . $name . '</li>';

            } else {
                $html .= '<li><a href=' . $url . '>' . $name . '</a></li>';
            }

        } else {
            if (!next($parameters)) {
                $html .= '<li class="active">' . $value . '</li>';
            } else {
                $url = route($key);
                $html .= '<li><a href=' . $url . '>' . $value . '</a></li>';
            }
        }
    }
    $html .= '</ol>
                </div>
            </div>
        </div>
    </div>';
    return $html;
}



function snedSMS($mobile, $message, $county = 91, $ruote = 'SKVAPE', $authkey = '248828AyDlWFTTn95bf8f4b8')
{
// {{snedSMS(9848090537, 'Hi test mesasage By developer \n\n\n By skvapee.com')}}

    $mobile = $mobile;
    $message = str_replace('\n', '%0a', $message);
    $message = str_replace('\r', '%0a', $message);
    $message = str_replace('<br/>', '%0a', $message);
    $message = str_replace('<br />', '%0a', $message);
    $message = urlencode($message);

    $response = '';
    if (is_array($mobile)) {
        if (is_numeric($mobile)) {
            $response = \Ixudra\Curl\Facades\Curl::to('http://api.msg91.com/api/sendhttp.php?country=' . $county . '&sender=' . $ruote . '&route=4&mobiles=' . implode(',', $mobile) . '&authkey=' . $authkey . '&message=' . $message . '&unicode=1')->get();
        }
    } else {
        if (is_numeric($mobile)) {
            $response = \Ixudra\Curl\Facades\Curl::to('http://api.msg91.com/api/sendhttp.php?country=' . $county . '&sender=' . $ruote . '&route=4&mobiles=' . $mobile . '&authkey=' . $authkey . '&message=' . $message . '&unicode=1')->get();
        }
    }
    return $response;
}
function gender($id = null)
{
    $results = array();
    $list = array(
        'Male' => 'Male',
        'Female' => 'Female'
    );
    if ($id != '') {
        $results = $list[$id];
    } else {
        $results = $list;
    }
    return $results;

}
function productDescPosottion($id = null)
{

    $results = array();
    $list = array(
        '1' => 'Top',
        '2' => 'Left',
        '3' => 'Right',
        '4' => 'Bottom'
    );
    if ($id != '') {
        $results = $list[$id];
    } else {
        $results = $list;
    }
    return $results;

}
function productInftroTheme($arraydata)
{
    $returnarray = '';
    if (count($arraydata) > 0) {
        foreach ($arraydata AS $arrayItam) {

            switch ($arrayItam->pe_position) {
                case '1':

                    $returnarray .= '<div class="py-4">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <h2 class="h3 fmed">' . $arrayItam->pe_title . '</h2>
                            <p class="py-3">' . $arrayItam->pe_description . '</p>
                        </div>
                        <div class="col-lg-12 text-center">
                            <figure class="py-5">
                                <img src="/uploads/products/' . $arrayItam->pe_image . '" class="img-fluid" alt="" title="">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>';
                    break;

                case '2':
                    $returnarray .= '<div class="graybg reviewpad">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 align-self-center">
                            <h3 class="reviewsectitle pb-2">' . $arrayItam->pe_title . '</h3>
                            <p>' . $arrayItam->pe_description . '</p>
                        </div>
                        <div class="col-lg-6">
                            <img src="/uploads/products/' . $arrayItam->pe_image . '" class="img-fluid" alt="" title="">
                        </div>
                    </div>
                </div>
            </div>';

                    break;
                case '3':

                    $returnarray .= '<div class="reviewpad">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 align-self-center order-last">
                            <p>' . $arrayItam->pe_description . '</p>
                        </div>
                        <div class="col-lg-6">
                            <img src="/uploads/products/' . $arrayItam->pe_image . '" class="img-fluid" alt="" title="">
                        </div>
                    </div>
                </div>
            </div>';

                    break;

                case '4':

                    $returnarray .= '<div class="graybg reviewpadtop">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-12 text-center">
                            <h2 class="h3 fmed pb-5">' . $arrayItam->pe_description . '</h2>
                        </div>
                        <div class="col-lg-12 text-center">
                            <figure class="pt-5">
                                <img src="/uploads/products/' . $arrayItam->pe_image . '" class="img-fluid" alt="" title="">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>';

                    break;

                default:

                    $returnarray .= '<div class="graybg reviewpadtop">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-12 text-center">
                            <h2 class="h3 fmed pb-5">' . $arrayItam->pe_description . '</h2>
                        </div>
                        <div class="col-lg-12 text-center">
                            <figure class="pt-5">
                                <img src="/uploads/products/' . $arrayItam->pe_image . '" class="img-fluid" alt="" title="">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>';

                    break;
            }
        }
    }

    return $returnarray;
}
function g_print($array)
{
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}

