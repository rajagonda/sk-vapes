<?php
function getCountries()
{
    $countries = \App\Models\Countries::where('country_status', 'active')->orderBy('country_id', 'desc')->pluck('country_name', 'country_id');
    return $countries;

}

function getCategories()
{
    $categories = \App\Models\Categories::where('category_status', 'active')->orderBy('category_order', 'asc')->pluck('category_name', 'category_alias');
    return $categories;

}

function getCategoriesByID()
{
    $categories = \App\Models\Categories::where('category_status', 'active')->orderBy('category_id', 'desc')->pluck('category_name', 'category_id');
    return $categories;

}

function getBrands($catid = null)
{
    if ($catid) {
        $brands = \App\Models\Brands::whereRaw('FIND_IN_SET('.$catid.', brand_catid)')->where('brand_status', 'active')->orderBy('brand_id', 'desc')->pluck('brand_name', 'brand_alias');
    }else{
        $brands = \App\Models\Brands::where('brand_status', 'active')->orderBy('brand_id', 'desc')->pluck('brand_name', 'brand_alias');
    }
    return $brands;

}

function getBrandsByID($catid = null)
{
    if ($catid) {
        $brands = \App\Models\Brands::where('brand_catid', $catid)->where('brand_status', 'active')->orderBy('brand_id', 'desc')->pluck('brand_name', 'brand_id');
    } else {

        $brands = \App\Models\Brands::where('brand_status', 'active')->orderBy('brand_id', 'desc')->pluck('brand_name', 'brand_id');
    }
    return $brands;

}

function getCountriesByCode($location)
{
    App::setLocale($location);
    $countries = \App\Models\Countries::where('country_status', 'active')->orderBy('country_id', 'desc')->pluck('country_name', 'country_code');
    return $countries;

}

function getCountriesByPrefixes()
{
    $countries = \App\Models\Countries::where('country_status', 'active')->orderBy('country_id', 'asc')->pluck('country_phone_prefix');
    return $countries;

}

function getWishlistProducts()
{
    $products = \App\Models\Wishlist::where('wishlist_user_id', \Illuminate\Support\Facades\Auth::id())->pluck('wishlist_product_id')->toArray();
    return $products;

}

function getCountryByCode($country)
{
    $countries = \App\Models\Countries::where('country_code', $country)->first();
    return $countries;
}

function getProductImage($productid)
{
    $image = \App\Models\ProductImages::where('pi_product_id', $productid)->first();
    return $image;
}
