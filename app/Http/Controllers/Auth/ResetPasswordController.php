<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected function redirectTo()
    {
        $user = Auth::user();

        if ($user->role == 404) {
            $url = route('dashboard');
            return $url;
        } else {
            $url = route('userprofile');
            return $url;
        }

    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetForm(Request $request, $token = null, $location = null)
    {
        App::setLocale($location);
        $data = array();
        $data['active_menu'] = 'ResetForm';
        $data['sub_active_menu'] = 'ResetForm';
        $data['title'] = 'Reset Form';
        $data['countries'] = getCountriesByCode($location);
        return view('auth.passwords.reset',$data)->with(
            ['token' => $token, 'email' => $request->email]
        );
    }
}
