<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Request;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');

    }
    protected function validationErrorMessages()
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ];
    }

    public function showLinkRequestForm($location = null)
    {
        App::setLocale($location);
        $data = array();
        $data['active_menu'] = 'forgotPassword';
        $data['sub_active_menu'] = 'forgotPassword';
        $data['title'] = 'Forgot Password';
        $data['countries'] = getCountriesByCode($location);
        return view('auth.passwords.email', $data);
    }


}
