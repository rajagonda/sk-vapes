<?php

namespace App\Http\Controllers\Auth;

use App\Mail\WelcomeMail;
use App\User;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Notifications\UserRegisteredNotification;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected function redirectTo()
    {
        $user = Auth::user();

        snedSMS( $user->mobile_prefix . $user->mobile, 'Hi '.$user->name.', Welcome To skvapes.com');
        snedSMS(['919985089794','917702350236', '919848090537','918977111142'], 'Hi Admin, new User is '.$user->name.' Rigisterd, skvapes.com');

        if ($user->role == 404) {
            $url = route('dashboard');
            return $url;
        } else {
            $url = route('userprofile');
            return $url;
        }

    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    public function showRegistrationForm($location = null)
    {
        App::setLocale($location);
        $data = array();
        $data['active_menu'] = 'signup';
        $data['sub_active_menu'] = 'signup';
        $data['title'] = 'Sign up';
        $data['countries'] = getCountriesByCode($location);
        return view('frontend.signup', $data);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|max:12|confirmed',
//            'password' => 'required|string|min:6|max:12|confirmed|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%@^&*()~]).*$/',
            'mobile' => 'required|min:10|max:15',
            'mobile_prefix' => 'required',
            'gender' => 'required',
            'DOB' => 'required',
        ], [
            'name.required' => 'The :attribute field is required.',
            'email.required' => 'The :attribute field is required.',
            'email.max' => 'Email contains maximum 255 digits',
            'name.max' => 'Name contains maximum 255 digits',
            'mobile.required' => 'The :attribute field is required.',
            'mobile.min' => 'Mobile number contains minimum 10 digits',
            'mobile.max' => 'Mobile number contains maximum 15 digits',
            'password.min' => 'Password number contains minimum 6 digits',
            'password.max' => 'Password number contains maximum 12 digits',
            'password.required' => 'The :attribute field is required.',
//            'password.regex' => 'Passwords must have at least 6 characters and contains atleast one uppercase letter, lowercase letter, number, and specl symbols ',
            'password.confirmed' => 'Password and confirm password must be same',
            'email.unique' => 'This email is taken Enter a different one or <a href="' . route('userelogin') . '">sign in</a>',
            'gender.required' => 'Select Gender',
            'DOB.required' => 'Enter DOB',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        Session::flash('flash_message', 'Welcome to skvapes !');
        $user = User::create([
            'name' => $data['name'],
            'role' => $data['role'],
            'status' => $data['status'],
            'email' => $data['email'],
            'mobile' => $data['mobile'],
            'gender' => $data['gender'],
            'mobile_prefix' => $data['mobile_prefix'],
            'password' => Hash::make($data['password']),
            'DOB' => Carbon::createFromFormat('d-m-Y', $data['DOB'])->toDateString(),
        ]);
        Mail::to($data['email'])->send(new WelcomeMail($user));

        $user->notify(new UserRegisteredNotification($user));
        return $user;
    }

    protected function registered(Request $request, $user)
    {
        $user->notify(new UserRegisteredNotification($user));
    }
}
