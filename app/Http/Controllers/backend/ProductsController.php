<?php

namespace App\Http\Controllers\backend;

use App\Models\ProductExtraInformation;
use App\Models\ProductImages;
use App\Models\Products;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        define('PAGE_LIMIT', 30);
    }

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $pname = $request->get('product_name');
        $category = $request->get('search_category');
        if (!empty($keyword)) {
            $products = Products::
            join('categories', 'categories.category_id', '=', 'products.product_category')
                ->when($pname, function ($query) use ($pname) {
                    return $query->whereRaw("LOWER(products.product_name) LIKE ?", '%' . strtolower($pname) . '%');
                })
                ->when($category, function ($query) use ($category) {
                    return $query->where('categories.category_alias', $category);
                })
                ->orderBy('product_id', 'desc')
                ->paginate(PAGE_LIMIT);
        } elseif ($request->isMethod('post')) {
            $requestData = $request->all();

            $productimages = ProductImages::where('pi_product_id', $requestData['product_id'])->get();

            foreach ($productimages as $image) {

                File::delete('uploads/products/' . $image->pi_image_name);
                File::delete('uploads/products/thumbs/' . $image->pi_image_name);
                ProductImages::destroy($image->pi_id);
            }
            $productExtraInfo = ProductExtraInformation::where('pe_product_id', $requestData['product_id'])->get();

            foreach ($productExtraInfo as $info) {
                File::delete('uploads/products/' . $info->pe_image);
                File::delete('uploads/products/thumbs/' . $info->pe_image);
                ProductExtraInformation::destroy($info->pe_id);
            }

            $file = Products::findOrFail($requestData['product_id']);

            if ($file->product_image != '') {
                File::delete('uploads/products/' . $file->product_image);
                File::delete('uploads/products/thumbs/' . $file->product_image);
            }

            Products::destroy($requestData['product_id']);

            return redirect()->route('products')->with('flash_message', 'Product deleted successfully!');
        } else {
            $products = Products::orderBy('product_id', 'desc')
                ->paginate(PAGE_LIMIT);
        }
        $data = array();
        $data['active_menu'] = 'products';
        $data['sub_active_menu'] = 'products-list';
        $data['title'] = 'Products';
        $data['products'] = $products;
        return view('backend.products.list', $data);
    }

    public function addNewProducts(Request $request, $id = null)
    {
        $tableInfo = new Products();
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            if ($requestData['product_id'] != '') {
                $products = Products::findOrFail($requestData['product_id']);
                $imageRule = empty($products->product_image) ? 'required' : '';
            } else {
                $imageRule = 'required';
            }
            $this->validate(request(), [
                'product_name' => 'required',
                'product_type' => 'required',
                'product_availability' => 'required',
//                'product_color' => 'required',
                'product_description' => 'required',
                'product_price' => 'required',
                'product_real_price' => 'required',
                'product_brand' => 'required',
                'product_country' => 'required',
                'product_category' => 'required',
                'product_image' => $imageRule,
//                'product_shipping_price' => 'required',
                'product_status' => 'required'
            ], [
                'product_name.required' => 'Please enter name',
                'product_type.required' => 'Please enter type',
                'product_availability.required' => 'Please enter availability',
//                'product_color.required' => 'Please enter color',
                'product_description.required' => 'Please enter description',
                'product_price.required' => 'Please enter price',
                'product_real_price.required' => 'Please enter price',
                'product_brand.required' => 'Please select brand',
                'product_country.required' => 'Please select country',
                'product_category.required' => 'Please select category',
                'product_image.required' => 'select image to upload',
//                'product_shipping_price.required' => 'Please enter shipping price',
                'product_status.required' => 'Select status',
            ]);
            $requestData['product_alias'] = str_slug($requestData['product_name'], '-');
            $fileName = '';
            if ($request->hasFile('product_image')) {
                $uploadPath = public_path('/uploads/products/');

                if (!file_exists($uploadPath)) {
                    mkdir($uploadPath, 0777, true);
                }

                $extension = $request['product_image']->getClientOriginalName();

                $fileName = time() . $extension;

                $thumbPath = public_path('/uploads/products/thumbs/');

                if (!file_exists($thumbPath)) {
                    mkdir($thumbPath, 0777, true);
                }

                $thumb_img = \Intervention\Image\Facades\Image::make($request['product_image']->getRealPath())->resize(400, 400, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $thumb_img->save($thumbPath . '/' . $fileName, 60);

                $request['product_image']->move($uploadPath, $fileName);
                $requestData['product_image'] = $fileName;
            }
            if (isset($requestData['product_is_best'])) {
                $requestData['product_is_best'] = 1;
            } else {
                $requestData['product_is_best'] = 0;
            }
            if ($requestData['product_id'] == '') {
                $product_id = Products::create($requestData)->product_id;
                if ($request->hasFile('productimages')) {
                    foreach ($request['productimages'] as $file) {
                        $uploadPath = public_path('/uploads/products/');

                        if (!file_exists($uploadPath)) {
                            mkdir($uploadPath, 0777, true);
                        }
                        $extension = $file->getClientOriginalName();

                        $fileName = time() . $extension;

                        $thumbPath = public_path('/uploads/products/thumbs/');

                        if (!file_exists($thumbPath)) {
                            mkdir($thumbPath, 0777, true);
                        }

                        $thumb_img = \Intervention\Image\Facades\Image::make($file->getRealPath())->resize(400, 400, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                        $thumb_img->save($thumbPath . '/' . $fileName, 60);

                        $file->move($uploadPath, $fileName);

                        $requestdata = array();
                        $requestdata['pi_image_name'] = $fileName;
                        $requestdata['pi_status'] = 'active';
                        $requestdata['pi_product_id'] = $product_id;
                        ProductImages::create($requestdata);
                    }
                }
                $mes = 'Product added successfully!';
                return redirect()->route('productExtraInfo', ['id' => $product_id])->with('flash_message', $mes);
            } else {
                if ($products->product_image != '' && $fileName != '') {

                    File::delete('uploads/products/' . $products->product_image);
                    File::delete('uploads/products/thumbs/' . $products->product_image);
                }
                $products->update($requestData);

                $product_id = $requestData['product_id'];

                if ($request->hasFile('productimages')) {
                    foreach ($request['productimages'] as $file) {
                        $uploadPath = public_path('/uploads/products/');

                        if (!file_exists($uploadPath)) {
                            mkdir($uploadPath, 0777, true);
                        }
                        $extension = $file->getClientOriginalName();

                        $fileName = time() . $extension;

                        $thumbPath = public_path('/uploads/products/thumbs/');

                        if (!file_exists($thumbPath)) {
                            mkdir($thumbPath, 0777, true);
                        }

                        $thumb_img = \Intervention\Image\Facades\Image::make($file->getRealPath())->resize(400, 400, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                        $thumb_img->save($thumbPath . '/' . $fileName, 60);

                        $file->move($uploadPath, $fileName);

                        $requestdata = array();
                        $requestdata['pi_image_name'] = $fileName;
                        $requestdata['pi_status'] = 'active';
                        $requestdata['pi_product_id'] = $product_id;
                        ProductImages::create($requestdata);
                    }
                }

                $mes = 'Product updated successfully!';
                return redirect()->route('addNewProducts', ['id' => $product_id])->with('flash_message', $mes);
            }
        } else {
            $data = array();
            $data['product_id'] = '';
            $data['product'] = '';
            if ($id) {
                $data['product_id'] = $id;
                $data['product'] = Products::findOrFail($id);
            }
            $data['active_menu'] = 'products';
            $data['sub_active_menu'] = 'manage-products';
            $data['title'] = 'Manage products';
            $data['product_images'] = ProductImages::where('pi_product_id', $id)->get();

            return view('backend.products.add', $data);
        }
    }

    public function addProductImages(Request $request, $id)
    {
        $tableInfo = new ProductImages();
        if ($request->isMethod('post')) {

            if ($request->hasFile('productimages')) {
                foreach ($request['productimages'] as $file) {
                    $uploadPath = public_path('/uploads/products/');

                    if (!file_exists($uploadPath)) {
                        mkdir($uploadPath, 0777, true);
                    }
                    $extension = $file->getClientOriginalName();

                    $fileName = time() . $extension;

                    $thumbPath = public_path('/uploads/products/thumbs/');

                    if (!file_exists($thumbPath)) {
                        mkdir($thumbPath, 0777, true);
                    }

                    $thumb_img = \Intervention\Image\Facades\Image::make($file->getRealPath())->resize(400, 400, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $thumb_img->save($thumbPath . '/' . $fileName, 60);

                    $file->move($uploadPath, $fileName);

                    $requestdata = array();
                    $requestdata['pi_image_name'] = $fileName;
                    $requestdata['pi_status'] = 'active';
                    $requestdata['pi_product_id'] = $id;
                    ProductImages::create($requestdata);
                }
            }

            return redirect()->route('addProductImages', ['id' => $id])->with('flash_message', 'Product Images added successfully');

        } else {
            $data = array();

            $data['product_id'] = $id;
            $data['product_images'] = ProductImages::where('pi_product_id', $id)->get();
            $data['active_menu'] = 'products';
            $data['sub_active_menu'] = 'products-images';
            $data['title'] = 'Products';
            return view('backend.products.images', $data);
        }
    }

    public function deleteProductimage(Request $request)
    {
        $image = ProductImages::findOrFail($request['image']);

        File::delete('uploads/products/' . $image->pi_image_name);
        File::delete('uploads/products/thumbs/' . $image->pi_image_name);

        ProductImages::destroy($request['image']);
        exit();
    }

    public function productExtraInfo(Request $request, $id)
    {
//        dd($request);

        if ($request->isMethod('post')) {
            dd($request);
            $requestData = $request->all();

            $file = ProductExtraInformation::findOrFail($requestData['pe_id']);

            if ($file->pe_image != '') {
                File::delete('uploads/products/' . $file->pe_image);
                File::delete('uploads/products/thumbs/' . $file->pe_image);
            }

            ProductExtraInformation::destroy($requestData['pe_id']);
            return redirect()->route('productExtraInfo', ['id' => $id])->with('flash_message', 'Product Information Deleted successfully!');
        }

        $data = array();
        $data['active_menu'] = 'products';
        $data['sub_active_menu'] = 'products-extra-info';
        $data['title'] = 'Products';
        $data['product_id'] = $id;
        $data['product_details'] = ProductExtraInformation::where('pe_product_id', $id)->paginate(PAGE_LIMIT);
        return view('backend.products.extraDetails', $data);
    }

    public function addProductExtraInfo(Request $request, $id)
    {
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            if ($requestData['pe_id'] != '') {
                $product = ProductExtraInformation::findOrFail($requestData['pe_id']);
                $imageRule = empty($product->pe_image) ? 'required' : '';
            } else {
                $imageRule = 'required';
            }

            $this->validate(request(), [
                'pe_title' => 'required',
                'pe_position' => 'required',
                'pe_description' => 'required',
                'pe_image' => $imageRule,
                'pe_status' => 'required'
            ], [
                'pe_title.required' => 'Enter title',
                'pe_description.required' => 'Enter description',
                'pe_position.required' => 'Enter position',
                'pe_image.required' => 'select image to upload',
                'pe_status.required' => 'Select status',
            ]);

            $fileName = '';
            if ($request->hasFile('pe_image')) {
                $uploadPath = public_path('/uploads/products/');

                if (!file_exists($uploadPath)) {
                    mkdir($uploadPath, 0777, true);
                }

                $extension = $request['pe_image']->getClientOriginalName();

                $fileName = time() . $extension;

                $thumbPath = public_path('/uploads/products/thumbs/');

                if (!file_exists($thumbPath)) {
                    mkdir($thumbPath, 0777, true);
                }

                $thumb_img = \Intervention\Image\Facades\Image::make($request['pe_image']->getRealPath())->resize(400, 400, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $thumb_img->save($thumbPath . '/' . $fileName, 60);

                $request['pe_image']->move($uploadPath, $fileName);
                $requestData['pe_image'] = $fileName;
            }

            if ($requestData['pe_id'] == '') {

                $requestData['pe_product_id'] = $id;

                ProductExtraInformation::create($requestData);

                $mes = 'Product Info added successfully!';
            } else {

                if ($product->pe_image != '' && $fileName != '') {

                    File::delete('uploads/products/' . $product->pe_image);
                    File::delete('uploads/products/thumbs/' . $product->pe_image);
                }

                $product->update($requestData);
                $mes = 'Product Info updated successfully!';
            }
            return redirect()->route('productExtraInfo', ['id' => $id])->with('flash_message', $mes);

        }
    }

    public function deleteProductExtraimage(Request $request)
    {
        $image = ProductExtraInformation::findOrFail($request['image']);

        File::delete('uploads/products/' . $image->pe_image);
        File::delete('uploads/products/thumbs/' . $image->pe_image);

        $update_data = array();
        $update_data['pe_image'] = '';
        $image->update($update_data);
        exit();
    }
}
