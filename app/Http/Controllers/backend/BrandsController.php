<?php

namespace App\Http\Controllers\backend;

use App\Models\Brands;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;

class BrandsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        define('PAGE_LIMIT', 30);
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $file = Brands::findOrFail($requestData['brand_id']);

            if ($file->brand_image != '') {
                File::delete('uploads/brands/' . $file->brand_image);
                File::delete('uploads/brands/thumbs/' . $file->brand_image);
            }
            Brands::destroy($requestData['brand_id']);
            return redirect()->route('brands')->with('flash_message', 'Brand deleted successfully!');
        } else {
            $brands = Brands::orderBy('brand_id', 'ASC')
                ->paginate(PAGE_LIMIT);
        }
        $data = array();
        $data['active_menu'] = 'brands';
        $data['sub_active_menu'] = 'brands-list';
        $data['title'] = 'Brands';
        $data['brands'] = $brands;
        return view('backend.brands.list', $data);
    }

    public function addNewBrands(Request $request, $id = null)
    {
        $tableInfo = new Brands();
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            if ($requestData['brand_id'] != '') {
                $brands = Brands::findOrFail($requestData['brand_id']);
                $imageRule = empty($brands->brand_image) ? 'required' : '';
            } else {
                $imageRule = 'required';
            }

            $this->validate(request(), [
                'brand_name' => 'required',
                'brand_country' => 'required',
                'brand_image' => $imageRule,
                'brand_status' => 'required'
            ], [
                'brand_name.required' => 'Enter brand name',
                'brand_country.required' => 'Please select country',
                'brand_image.required' => 'select image to upload',
                'brand_status.required' => 'Select status',
            ]);

            $fileName = '';
            if ($request->hasFile('brand_image')) {
                $uploadPath = public_path('/uploads/brands/');

                if (!file_exists($uploadPath)) {
                    mkdir($uploadPath, 0777, true);
                }

                $extension = $request['brand_image']->getClientOriginalName();

                $fileName = time() . $extension;

                $thumbPath = public_path('/uploads/brands/thumbs/');

                if (!file_exists($thumbPath)) {
                    mkdir($thumbPath, 0777, true);
                }

                $thumb_img = \Intervention\Image\Facades\Image::make($request['brand_image']->getRealPath())->resize(400, 400, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $thumb_img->save($thumbPath . '/' . $fileName, 60);

                $request['brand_image']->move($uploadPath, $fileName);
                $requestData['brand_image'] = $fileName;
            }

            $requestData['brand_alias'] = str_slug($requestData['brand_name'], '-');
            if ($requestData['brand_id'] == '') {

                $requestData['brand_catid'] = implode(',', $requestData['brand_catid']);

//                dd($requestData);

                Brands::create($requestData);

                $mes = 'Brand added successfully!';
            } else {

                if ($brands->brand_image != '' && $fileName != '') {

                    File::delete('uploads/brands/' . $brands->brand_image);
                    File::delete('uploads/brands/thumbs/' . $brands->brand_image);
                }

                $requestData['brand_catid'] = implode(',', $requestData['brand_catid']);

                $brands->update($requestData);
                $mes = 'Brand updated successfully!';
            }
            return redirect()->route('brands')->with('flash_message', $mes);

        } else {
            $data = array();
            $data['brand_id'] = '';
            $data['brand'] = '';
            if ($id) {
                $data['brand_id'] = $id;
                $data['brand'] = Brands::findOrFail($id);
            }
            $data['active_menu'] = 'brands';
            $data['sub_active_menu'] = 'manage-brands';
            $data['title'] = 'Manage brands';
            return view('backend.brands.add', $data);
        }
    }

    public function deleteBrandimage(Request $request)
    {
        $image = Brands::findOrFail($request['image']);

        File::delete('uploads/brands/' . $image->brand_image);
        File::delete('uploads/brands/thumbs/' . $image->brand_image);

        $update_data = array();
        $update_data['brand_image'] = '';
        $image->update($update_data);
        exit();
    }
}
