<?php

namespace App\Http\Controllers\backend;

use App\Models\ContactUs;
use App\Models\NewsLetters;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        define('PAGE_LIMIT', 30);
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'AKVAPES-Dashboard';
        $data['active_menu'] = 'dashboard';
        $data['sub_active_menu'] = 'dashboard';
        return view('backend.dashboard', $data);
    }


    public function profile(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();
            $this->validate(request(), [
                'name' => 'required',
                'email' => 'required'
            ], [
                'name.required' => 'Please enter name',
                'email.required' => 'Please enter email'
            ]);
            $id = Auth::id();
            $user = User::findOrFail($id);
            $user->update($requestData);
            $mes = 'profile updated successfully!';
            return redirect()->route('profile')->with('flash_message', $mes);

        } else {
            $data = array();
            $data['active_menu'] = 'profile';
            $data['sub_active_menu'] = 'profile';
            $data['title'] = 'Update profile';
            return view('backend.updateProfile', $data);
        }
    }

    public function changePassword(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();
            $this->validate(request(), [
                'password' => 'required',
                'new_password' => 'required|min:6|max:12',
                'confirm_password' => 'required|min:6|max:12|same:new_password'
            ], [
                'password.required' => 'Please enter old password',
                'new_password.required' => 'Please enter new password',
                'confirm_password.required' => 'Please confirm password'
            ]);
            $id = Auth::id();
            $user = User::findOrFail($id);
            if (Hash::check($requestData['password'], $user->password)) {
                $requestData['password'] = Hash::make($requestData['new_password']);
                $user->update($requestData);
                $mes = 'Password updated successfully!';
                return redirect()->route('changePassword')->with('flash_message', $mes);
            } else {
                $mes = 'Old password is wrong ...try again';
                return redirect()->route('changePassword')->with('flash_message_error', $mes);
            }

        } else {
            $data = array();
            $data['active_menu'] = 'changepassword';
            $data['sub_active_menu'] = 'changepassword';
            $data['title'] = 'Change Password';
            return view('backend.changePassword', $data);
        }
    }

    public function newsLetters(Request $request)
    {

        $newsLetters = NewsLetters::orderBy('nl_id', 'desc')
            ->paginate(PAGE_LIMIT);
        $data = array();
        $data['active_menu'] = 'newsletter';
        $data['sub_active_menu'] = 'newsletter-list';
        $data['title'] = 'News Letters';
        $data['newsLetters'] = $newsLetters;
        return view('backend.newsLetters', $data);
    }
   public function contactus(Request $request)
    {

        $contacts = ContactUs::orderBy('id', 'desc')
            ->paginate(PAGE_LIMIT);
        $data = array();
        $data['active_menu'] = 'contactus';
        $data['sub_active_menu'] = 'contactus-list';
        $data['title'] = 'Contact Us';
        $data['contacts'] = $contacts;
        return view('backend.contactus', $data);
    }
}
