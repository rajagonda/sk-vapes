<?php

namespace App\Http\Controllers\frontend;

use App\Models\OrderItems;
use App\Models\Orders;
use App\Models\UserAddress;
use App\Models\Wishlist;
use App\User;
use Carbon\Carbon;
use Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use File;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($locale = null, Request $request)
    {
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            $tableInfo = new User();
            $this->validate(request(), [
                'name' => 'required',
                'email' => ['required', Rule::unique($tableInfo->getTable())->ignore(Auth::id(), 'id')],
                'mobile_prefix' => 'required',
                'mobile' => 'required',
                'gender' => 'required',
                'DOB' => 'required',
            ], [
                'name.required' => 'Please enter name',
                'email.required' => 'Please enter email',
                'mobile_prefix.required' => 'Enter mobile prefix',
                'mobile.required' => 'Enter mobile',
                'gender.required' => 'Select Gender',
                'DOB.required' => 'Enter DOB',
            ]);
            $fileName = '';
            if ($request->hasFile('image')) {
                $uploadPath = public_path('/uploads/users/');

                if (!file_exists($uploadPath)) {
                    mkdir($uploadPath, 0777, true);
                }

                $extension = $request['image']->getClientOriginalName();

                $fileName = time() . $extension;

                $thumbPath = public_path('/uploads/users/thumbs/');

                if (!file_exists($thumbPath)) {
                    mkdir($thumbPath, 0777, true);
                }

                $thumb_img = \Intervention\Image\Facades\Image::make($request['image']->getRealPath())->resize(400, 400, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $thumb_img->save($thumbPath . '/' . $fileName, 60);

                $request['image']->move($uploadPath, $fileName);
                $requestData['image'] = $fileName;
            }

            $requestData['DOB'] = Carbon::createFromFormat('d-m-Y', $requestData['DOB'])->toDateString();
            $users = User::findOrFail(Auth::id());
            $users->update($requestData);
            $mes = 'User updated successfully!';
            return redirect()->route('userprofile')->with('flash_message', $mes);
        } else {
            $data = array();
            $data['active_menu'] = 'profile';
            $data['sub_active_menu'] = 'profile';
            $data['title'] = 'User Profile';
            $data['user'] = User::where('id', Auth::id())->first();
            $data['countries'] = getCountriesByCode($locale);
            $data['categories'] = getCategories();
            return view('frontend.user.profile', $data);
        }
    }

    public function deleteUserimage()
    {
        $image = User::findOrFail(Auth::id());

        File::delete('uploads/users/' . $image->image);
        File::delete('uploads/users/thumbs/' . $image->image);

        $update_data = array();
        $update_data['image'] = '';
        $image->update($update_data);
        exit();
    }
    public function changePassword($locale = null, Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();
            $this->validate(request(), [
                'password' => 'required',
                'new_password' => 'required|string|min:6|max:12|confirmed|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%@^&*()~]).*$/',
                'confirm_password' => 'required|same:new_password'
            ], [
                'password.required' => 'Please enter old password',
                'new_password.required' => 'Please enter new password',
                'new_password.regex' => 'Passwords must have at least 6 characters and contains atleast one uppercase letter, lowercase letter, number, and specl symbols',
                'new_password.min' => 'Password number contains minimum 6 digits',
                'new_password.max' => 'Password number contains maximum 12 digits',
                'new_password.confirmed' => 'Password and confirm password must be same',
                'confirm_password.required' => 'Please confirm password'
            ]);
            $id = Auth::id();
            $user = User::findOrFail($id);
            if (Hash::check($requestData['password'], $user->password)) {
                $requestData['password'] = Hash::make($requestData['new_password']);
                $user->update($requestData);
                $mes = 'Password updated successfully!';
                return redirect()->route('userChangePassword')->with('flash_message', $mes);
            } else {
                $mes = 'Old password is wrong ...try again';
                return redirect()->route('userChangePassword')->with('flash_message_error', $mes);
            }

        } else {
            $data = array();
            $data['active_menu'] = 'changePassword';
            $data['sub_active_menu'] = 'changePassword';
            $data['title'] = 'Change Password';
            $data['countries'] = getCountriesByCode($locale);
            $data['categories'] = getCategories();
            return view('frontend.user.changePassword', $data);
        }
    }

    public function orders($locale = null)
    {
        $data = array();
        $data['active_menu'] = 'orders';
        $data['sub_active_menu'] = 'orders';
        $data['title'] = 'Orders';
        $data['countries'] = getCountriesByCode($locale);
        $data['categories'] = getCategories();
        $data['pending_orders'] = Orders::with('orderItems.getProduct.productImages')->where('order_user_id', Auth::id())->where('order_status', 'Pending')->orderBy('order_id', 'desc')->get();
        $data['completed_orders'] = Orders::with('orderItems.getProduct.productImages')->where('order_user_id', Auth::id())->where('order_status', 'Completed')->orderBy('order_id', 'desc')->get();
        $data['cancel_orders'] = Orders::with('orderItems.getProduct.productImages')->where('order_user_id', Auth::id())->where('order_status', 'Cancel')->orderBy('order_id', 'desc')->get();
        $data['all_orders'] = Orders::with('orderItems.getProduct.productImages')->where('order_user_id', Auth::id())->orderBy('order_id', 'desc')->get();

//       dd($data['all_orders']);
        return view('frontend.user.orders', $data);
    }

    public function orderDetails($order, $locale = null)
    {
        $data = array();
        $data['active_menu'] = 'orders';
        $data['sub_active_menu'] = 'orders';
        $data['title'] = 'Order Details';
        $data['countries'] = getCountriesByCode($locale);
        $data['categories'] = getCategories();
        $data['orderDetails'] = Orders::with('orderItems.getProduct.productImages')->where('order_reference_number', $order)->first();
//        dd($data['orderDetails']);
        return view('frontend.user.orderDetails', $data);
    }

    public function orderInvoice($order, $locale = null)
    {
        $data = array();
        $data['active_menu'] = 'orders';
        $data['sub_active_menu'] = 'orders';
        $data['title'] = 'Order Details';
        $data['countries'] = getCountriesByCode($locale);
        $data['categories'] = getCategories();
        $data['orderDetails'] = Orders::with('orderItems.getProduct.productImages')->where('order_reference_number', $order)->first();
        return view('frontend.user.orderInvoice', $data);
    }

    public function unProcessedOrders($locale = null)
    {
        $data = array();
        $data['active_menu'] = 'unProcessedOrders';
        $data['sub_active_menu'] = 'unProcessedOrders';
        $data['title'] = 'Un Completeed Orders';
        $data['countries'] = getCountriesByCode($locale);
        $data['categories'] = getCategories();
        return view('frontend.user.unProcessedOrders', $data);
    }

    public function userAddressBook($locale = null, Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();
            UserAddress::destroy($requestData['ua_id']);

            $userPrimaryAddressInfo = UserAddress::where('ua_user_id', Auth::id())->get();
            $userDefaultPrimaryAddressInfo = UserAddress::where('ua_user_id', Auth::id())->where('ua_defult', 1)->get();

            if (count($userDefaultPrimaryAddressInfo) == 0 && count($userPrimaryAddressInfo) > 0) {
                if ($userPrimaryAddressInfo[0]->ua_defult == 0) {
                    $updateData = array();
                    $updateData['ua_defult'] = 1;
                    $userAddress = UserAddress::findOrFail($userPrimaryAddressInfo[0]->ua_id);
                    $userAddress->update($updateData);
                }
            }

            if ($requestData['flag']) {
                return redirect()->route('orderConfirmation')->with('flash_message', 'Address deleted successfully!');
            } else {
                return redirect()->route('userAddressBook')->with('flash_message', 'Address deleted successfully!');
            }
        }
        $data = array();
        $data['active_menu'] = 'userAddressBook';
        $data['sub_active_menu'] = 'userAddressBook';
        $data['title'] = 'User Address Book';
        $data['countries'] = getCountriesByCode($locale);
        $data['categories'] = getCategories();
        $data['userAddresses'] = UserAddress::where('ua_user_id', Auth::id())->get();
        return view('frontend.user.userAddressBook', $data);
    }

    public function userAddAddressBook($locale = null, Request $request)
    {
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            $this->validate(request(), [
                'ua_name' => 'required',
                'ua_address' => 'required',
                'ua_landmark' => 'required',
                'ua_city' => 'required',
                'ua_state' => 'required',
                'ua_email' => 'required',
                'ua_phone' => 'required',
                'ua_country' => 'required'
            ], [
                'ua_name.required' => 'Please enter name',
                'ua_address.required' => 'Please enter address',
                'ua_landmark.required' => 'Please enter landmark',
                'ua_city.required' => 'Please enter city',
                'ua_state.required' => 'Please enter state',
                'ua_email.required' => 'Please enter email',
                'ua_phone.required' => 'Please enter phone',
                'ua_country.required' => 'Please select Country'
            ]);

            $userPrimaryAddressInfo = UserAddress::where('ua_user_id', Auth::id())->first();

            if (!$requestData['flag']) {
                if (isset($requestData['ua_defult']) && $requestData['ua_defult'] == 1) {
                    UserAddress::where('ua_user_id', Auth::id())->update(['ua_defult' => 0]);
                } else {
                    $requestData['ua_defult'] = 0;
                }
            }

            if (empty($userPrimaryAddressInfo)) {
                $requestData['ua_defult'] = 1;
            }
            if ($requestData['ua_id'] == '') {
                $requestData['ua_user_id'] = Auth::id();
                $requestData['ua_status'] = 'active';

                UserAddress::create($requestData);
                $mes = 'Address added successfully!';
            } else {
                $userAddress = UserAddress::findOrFail($requestData['ua_id']);
                $userAddress->update($requestData);

                $userPrimaryAddressInfo = UserAddress::where('ua_user_id', Auth::id())->get();
                $userDefaultPrimaryAddressInfo = UserAddress::where('ua_user_id', Auth::id())->where('ua_defult', 1)->get();

                if (count($userDefaultPrimaryAddressInfo) == 0 && count($userPrimaryAddressInfo) > 0) {
                    if ($userPrimaryAddressInfo[0]->ua_defult == 0) {
                        $updateData = array();
                        $updateData['ua_defult'] = 1;
                        $userAddress = UserAddress::findOrFail($userPrimaryAddressInfo[0]->ua_id);
                        $userAddress->update($updateData);
                    }
                }
                $mes = 'Address updated successfully!';
            }
            if ($requestData['flag']) {
                return redirect()->route('orderConfirmation')->with('flash_message', $mes);
            } else {
                return redirect()->route('userAddressBook')->with('flash_message', $mes);
            }

        }
    }

    public function makeDefaultAddress($locale = null, Request $request)
    {
        $requestData = $request->all();

        UserAddress::where('ua_user_id', Auth::id())->update(['ua_defult' => 0]);

        $userAddress = UserAddress::findOrFail($requestData['ua_id']);
        $userAddress->update($requestData);
        $mes = 'Address updated successfully!';
        return redirect()->route('orderConfirmation')->with('flash_message', $mes);

    }

    public function wishList($locale = null, Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();
            Wishlist::destroy($requestData['wishlist_id']);
            return redirect()->route('wishList')->with('flash_message', 'Wishlist Product deleted successfully!');
        }
        $data = array();
        $data['active_menu'] = 'wishList';
        $data['sub_active_menu'] = 'wishList';
        $data['title'] = 'Wish List';
        $data['countries'] = getCountriesByCode($locale);
        $data['categories'] = getCategories();
        $data['wishlistProducts'] = Wishlist::with('getProduct')->where('wishlist_user_id', Auth::id())->get();
        return view('frontend.user.wishList', $data);
    }

    public function orderConfirmation($locale = null)
    {
        if (Cart::content()->count()) {
            $data = array();
            $data['active_menu'] = 'orderConfirmation';
            $data['sub_active_menu'] = 'orderConfirmation';
            $data['title'] = 'Order Confirmation';
            $data['countries'] = getCountriesByCode($locale);
            $data['categories'] = getCategories();
            $data['userAddresses'] = UserAddress::where('ua_user_id', Auth::id())->get();
            return view('frontend.user.orderConfirmation', $data);
        } else {
            return redirect()->route('home');
        }

    }

    public function saveOredrs($locale = null, Request $request)
    {
        $requestData = $request->all();

        $userAddress = UserAddress::with('getCountry')->findOrFail($requestData['address_id']);

        $userAdd = array();
        $userAdd['ua_name'] = $userAddress->ua_name;
        $userAdd['ua_address'] = $userAddress->ua_address;
        $userAdd['ua_landmark'] = $userAddress->ua_landmark;
        $userAdd['ua_city'] = $userAddress->ua_city;
        $userAdd['ua_state'] = $userAddress->ua_state;
        $userAdd['ua_email'] = $userAddress->ua_email;
        $userAdd['ua_phone'] = $userAddress->ua_phone;
        $userAdd['ua_pincode'] = $userAddress->ua_pincode;
        $userAdd['ua_country'] = $userAddress->getCountry->country_name;
        $orders = array();
        $orders['order_user_id'] = Auth::id();
        $orders['order_delivery_address'] = serialize($userAdd);
        $orders['order_status'] = 'Pending';
        $orders['order_total_price'] = str_replace(',', '', Cart::subtotal());
        $orders['order_reference_number'] = md5(time() . rand());
        $order_id = Orders::create($orders)->order_id;

        foreach (Cart::content() as $row) {
            $orderItems = array();
            $orderItems['oitem_order_id'] = $order_id;
            $orderItems['oitem_product_id'] = $row->id;
            $orderItems['oitem_product_name'] = $row->name;
            $orderItems['oitem_product_spl'] = $row->options->color;
            $orderItems['oitem_product_size'] = $row->options->size;
            $orderItems['oitem_qty'] = str_replace(',', '', $row->qty);
            $orderItems['oitem_product_price'] = str_replace(',', '', $row->price);
            $orderItems['oitem_sub_total'] = str_replace(',', '', $row->subtotal);
            OrderItems::create($orderItems);
        }

        Cart::destroy();

        $mes = 'Your Order has been successfully submitted !';
        return redirect()->route('paywithpaypal')->with('flash_message', $mes);
    }
    public function paymentConfirmation($locale = null)
    {

        $data = array();
        $data['active_menu'] = 'paymentConfirmation';
        $data['sub_active_menu'] = 'paymentConfirmation';
        $data['title'] = 'Payment Confirmation';
        $data['countries'] = getCountriesByCode($locale);
        $data['categories'] = getCategories();
        $data['latest_order'] = Orders::with('orderItems')->where('order_user_id', Auth::id())->orderBy('order_id', 'desc')->first();
        $data['latest_order_address'] = unserialize($data['latest_order']->order_delivery_address);
        return view('frontend.user.paymentConfirmation', $data);
    }

    public function addToWishList(Request $request)
    {
        $requestData = $request->all();

        $wishlist = array();
        $wishlist['wishlist_product_id'] = $requestData['id'];
        $wishlist['wishlist_user_id'] = Auth::id();

        $checkExisting = Wishlist::where('wishlist_product_id', $requestData['id'])->orderBy('wishlist_user_id', Auth::id())->first();
        if (!empty($checkExisting)) {
            Wishlist::destroy($checkExisting->wishlist_id);
            echo 2;
        } else {
            $id = Wishlist::create($wishlist)->wishlist_id;
            if ($id) {
                echo 1;
            } else {
                echo 0;
            }
        }

    }
}
