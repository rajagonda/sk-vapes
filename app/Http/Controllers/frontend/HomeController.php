<?php

namespace App\Http\Controllers\frontend;

use App\Models\Banners;
use App\Models\Brands;
use App\Models\Categories;
use App\Models\ContactUs;
use App\Models\FooterBanners;
use App\Models\NewsLetters;
use App\Models\ProductExtraInformation;
use App\Models\ProductImages;
use App\Models\Products;
use App\Models\Wishlist;
use Carbon\Carbon;
use Cart;
use http\Url;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    public function __construct()
    {

    }

    public function index($locale = null)
    {
        $data = array();
        $data['title'] = 'SK Vapes | E-Cigarate | E-Liquid | Mods & Batteries';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        $data['countries'] = getCountriesByCode($locale);
        $data['banners'] = Banners::where('banner_status', 'active')->get();
        $data['footerbanners'] = FooterBanners::where('banner_status', 'active')->get();
        $data['categories'] = getCategories();
        $data['brands'] = Brands::with('getCategory')->where('brand_status', 'active')->where('brand_future', '1')->orderBy('brand_order', 'ASC')->take(5)->get();
        $bestproducts = Products::where('product_status', 'active')->where('product_is_best', 1)->orderBy('product_id', 'desc')->take(4)->get();
        if (count($bestproducts) == 0) {
            $bestproducts = Products::where('product_status', 'active')->orderBy('product_id', 'desc')->take(4)->get();
        }
        $data['best_products'] = $bestproducts;
        $data['wishlistProducts'] = getWishlistProducts();
        return view('frontend.home', $data);
    }

    public function products($category = null, $brand = null, $locale = null)
    {
        $data = array();
        $data['title'] = 'SKVAPES- Products';
        $data['active_menu'] = 'products';
        $data['sub_active_menu'] = 'products';
        $data['products_category'] = $category;
        $data['countries'] = getCountriesByCode($locale);
        $data['categories'] = getCategories();
        $data['wishlistProducts'] = getWishlistProducts();
        $data['categoryInfo'] = Categories::where('category_alias', $category)->first();
        if ($brand == '') {
            $data['brandInfo'] = array();
            $data['products'] = Products::with('getCategory')->where('product_category', $data['categoryInfo']->category_id)->where('product_status', 'active')->get();
        } else {
            $data['brandInfo'] = Brands::where('brand_alias', $brand)->orderBy('brand_order', 'ASC')->first();
            $data['products'] = Products::with('getCategory')->where('product_category', $data['categoryInfo']->category_id)->where('product_brand', $data['brandInfo']->brand_id)->where('product_status', 'active')->get();
        }
//        echo $data['categoryInfo']->category_id;
//          dd(getBrands($data['categoryInfo']->category_id));

        return view('frontend.products', $data);
    }

    public function userBrandproducts($brand = null, $locale = null)
    {

        $data = array();
        $data['title'] = 'SKVAPES- Products';
        $data['active_menu'] = 'products';
        $data['sub_active_menu'] = 'products';
        $data['countries'] = getCountriesByCode($locale);
        $data['categories'] = getCategories();
        $data['wishlistProducts'] = getWishlistProducts();
        $data['brandInfo'] = Brands::where('brand_name', $brand)->first();
        $data['products'] = Products::where('product_brand', $data['brandInfo']->brand_id)->where('product_status', 'active')->get();
        return view('frontend.brand-products', $data);
    }

    public function productOverview($product_alias, $locale = null)
    {
        $data = array();
        $data['title'] = 'SKVAPES- Product Overview';
        $data['active_menu'] = 'productoverview';
        $data['sub_active_menu'] = 'productoverview';
        $data['countries'] = getCountriesByCode($locale);
        $data['categories'] = getCategories();
        $data['product'] = Products::where('product_alias', $product_alias)->first();
        $data['product_info'] = ProductExtraInformation::where('pe_product_id', $data['product']->product_id)->orderBy('pe_position', 'asc')->get();
        return view('frontend.productoverview', $data);
    }

    public function productDetails($product_alias, $locale = null)
    {
        $data = array();
        $data['title'] = 'SKVAPES- product Details';
        $data['active_menu'] = 'productDetails';
        $data['sub_active_menu'] = 'productDetails';
        $data['countries'] = getCountriesByCode($locale);
        $data['wishlistProducts'] = getWishlistProducts();
        $data['product'] = Products::where('product_alias', $product_alias)->first();
        $data['categories'] = getCategories();
        $data['productImages'] = ProductImages::where('pi_product_id', $data['product']->product_id)->orderBy('pi_id', 'desc')->get();
        $data['product_info'] = ProductExtraInformation::where('pe_product_id', $data['product']->product_id)->orderBy('pe_position', 'asc')->get();
        return view('frontend.productDetails', $data);
    }

    public function cartDetails($locale = null)
    {
        $data = array();
        $data['title'] = 'SKVAPES- cart Details';
        $data['active_menu'] = 'cartDetails';
        $data['sub_active_menu'] = 'cartDetails';
        $data['countries'] = getCountriesByCode($locale);
        $data['categories'] = getCategories();
        return view('frontend.cartDetails', $data);
    }

    public function support($locale = null, Request $request)
    {
        if ($request->isMethod('post')) {
            $return = array();
            $requestData = $request->all();
            $validator = Validator::make($requestData,
                [
                    'name' => 'required',
                    'phone' => 'required',
                    'email' => 'required|email'
                ],
                [
                    'name.required' => 'Please enter email',
                    'phone.required' => 'Please enter phone',
                    'email.required' => 'Please enter email',
                    'email.email' => 'Please enter valid email'
                ]
            );
            if ($validator->fails()) {
                $return['status'] = 2;
                $test = array();
                foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                    array_push($test, $messages[0]);// messages are retrieved (publicly)
                }
                $return['errors'] = implode('<br>', $test);
            } else {
                $cdetails = ContactUs::create($requestData);
                if ($cdetails->exists) {
                    $return['status'] = 1;
                } else {
                    $return['status'] = 2;
                    $return['errors'] = "Please enter valid data";
                }
            }
            return $return;
        } else {
            $data = array();
            $data['title'] = 'SKVAPES- Support';
            $data['active_menu'] = 'support';
            $data['sub_active_menu'] = 'support';
            $data['countries'] = getCountriesByCode($locale);
            $data['categories'] = getCategories();

//            dd($locale);

            return view('frontend.support', $data);
        }

    }

    public function buyNow(Request $request)
    {
        $requestData = $request->all();

        $productInfo = Products::where('product_id', $requestData['id'])->first();

        Cart::destroy();
        Cart::add(['id' => $requestData['id'], 'name' => $productInfo->product_name, 'qty' => $requestData['qty'], 'price' => $productInfo->product_price]);

        return redirect()->route('cartDetails');

    }

    public function addToCart(Request $request)
    {
        $requestData = $request->all();

//        dd($requestData);

        $productInfo = Products::where('product_id', $requestData['id'])->first();

        Cart::add([
            'id' => $requestData['id'],
            'name' => $productInfo->product_name,

            'qty' => $requestData['qty'],
            'price' => $productInfo->product_price,
            'options' => [
                'size' => $requestData['size'],
                'color' => $requestData['color'],
                'description' => $productInfo->product_description,
                'alias' => $productInfo->product_alias,
                'image' => $productInfo->product_image
            ]
        ]);

//        dd(Cart::content());

        echo Cart::content()->count();

    }

    public function buyProduct(Request $request)
    {
        $requestData = $request->all();

        $productInfo = Products::where('product_id', $requestData['id'])->first();

        Cart::add([
            'id' => $requestData['id'],
            'name' => $productInfo->product_name,

            'qty' => $requestData['qty'],
            'price' => $productInfo->product_price,
            'options' => [
                'size' => $requestData['size'],
                'color' => $requestData['color'],
                'description' => $productInfo->product_description,
                'alias' => $productInfo->product_alias,
                'image' => $productInfo->product_image
            ]
        ]);
        return redirect()->route('cartDetails');
    }

    public function updateItemFromCart(Request $request)
    {

        $requestData = $request->all();

        Cart::update($requestData['id'], $requestData['qty']);

    }

    public function removeItemFromCart(Request $request)
    {
        $requestData = $request->all();

        Cart::remove($requestData['id']);

    }

    public function saveRestrictions(Request $request)
    {
        $requestData = $request->all();
//
//        $request['DOB'] = $request['dob_day'] . '-' . $request['dob_month'] . '-' . $request['dob_year'];
//
//        $dob = $request['DOB'];
//
//
//        $user_age = date_diff(date_create($dob), date_create('now'))->y;
//
//        $rest = array();
//        if ($user_age > 21) {
//            $rest['country'] = $requestData['country'];
//            $rest['DOB'] = $request['dob_day'] . '/' . $request['dob_month'] . '/' . $request['dob_year'];
//            $rest['status'] = 1;
//            $rest['age'] = $user_age;
//
//            session(['restrictData' => $rest]);
//        } else {
//            $rest['status'] = 0;
//            $rest['age'] = $user_age;
//        }

        $rest['status'] = 1;
        $rest['country'] = $requestData['country'];
        session(['restrictData' => $rest]);

        return $rest;

    }

    public function saveSubscribers(Request $request)
    {
        $return = array();
        $requestData = $request->all();
        $validator = Validator::make($requestData,
            [
                'nl_email' => 'required|email'
            ],
            [
                'nl_email.required' => 'Please enter email',
                'nl_email.email' => 'Please enter valid email'
            ]
        );
        if ($validator->fails()) {
            $return['status'] = 2;
            $test = array();
            foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                array_push($test, $messages[0]);// messages are retrieved (publicly)
            }
            $return['errors'] = implode('<br>', $test);
        } else {
            $cdetails = NewsLetters::create($requestData);
            if ($cdetails->exists) {
                $return['status'] = 1;
            } else {
                $return['status'] = 2;
                $return['errors'] = "Please enter valid data";
            }
        }
        return $return;

    }

    public function careers($locale = null, Request $request)
    {
        $data = array();
        $data['title'] = 'SKVAPES- Careers';
        $data['active_menu'] = 'careers';
        $data['sub_active_menu'] = 'careers';
        $data['countries'] = getCountriesByCode($locale);
        $data['categories'] = getCategories();
        return view('frontend.static.careers', $data);
    }

    public function WholesaleOpportunity($locale = null, Request $request)
    {
        $data = array();
        $data['title'] = 'SKVAPES- Bulk Orders';
        $data['active_menu'] = 'bulkOrders';
        $data['sub_active_menu'] = 'bulkOrders';
        $data['countries'] = getCountriesByCode($locale);
        $data['categories'] = getCategories();
        return view('frontend.static.wholesaleOpportunity', $data);
    }

    public function warranty($locale = null, Request $request)
    {
        $data = array();
        $data['title'] = 'SKVAPES- Warranty';
        $data['active_menu'] = 'warranty';
        $data['sub_active_menu'] = 'warranty';
        $data['countries'] = getCountriesByCode($locale);
        $data['categories'] = getCategories();
        return view('frontend.static.warranty', $data);
    }

    public function shipping($locale = null, Request $request)
    {
        $data = array();
        $data['title'] = 'SKVAPES- Shipping';
        $data['active_menu'] = 'shipping';
        $data['sub_active_menu'] = 'shipping';
        $data['countries'] = getCountriesByCode($locale);
        $data['categories'] = getCategories();
        return view('frontend.static.shipping', $data);
    }

    public function about($locale = null, Request $request)
    {
        $data = array();
        $data['title'] = 'SKVAPES- About Us';
        $data['active_menu'] = 'about';
        $data['sub_active_menu'] = 'about';
        $data['countries'] = getCountriesByCode($locale);
        $data['categories'] = getCategories();
        return view('frontend.static.about', $data);
    }

    public function faq($locale = null, Request $request)
    {
        $data = array();
        $data['title'] = 'SKVAPES- faq';
        $data['active_menu'] = 'faq';
        $data['sub_active_menu'] = 'faq';
        $data['countries'] = getCountriesByCode($locale);
        $data['categories'] = getCategories();
        return view('frontend.static.faq', $data);
    }

    public function terms($locale = null, Request $request)
    {
        $data = array();
        $data['title'] = 'SKVAPES-Terms';
        $data['active_menu'] = 'terms';
        $data['sub_active_menu'] = 'terms';
        $data['countries'] = getCountriesByCode($locale);
        $data['categories'] = getCategories();
        return view('frontend.static.terms', $data);
    }

    public function privacy($locale = null, Request $request)
    {
        $data = array();
        $data['title'] = 'SKVAPES-Privacy';
        $data['active_menu'] = 'privacy';
        $data['sub_active_menu'] = 'privacy';
        $data['countries'] = getCountriesByCode($locale);
        $data['categories'] = getCategories();
        return view('frontend.static.privacy', $data);
    }

    public function search(Request $request)
    {

        $name = $request->term;

        $products = Products::orderBy('product_id', 'desc')
            ->when($name, function ($query) use ($name) {
                return $query->whereRaw("LOWER(product_name) LIKE ?", '%' . strtolower($name) . '%');
            })
            ->get();
//            ->paginate(10);


        $returnArray=array();

        if(count($products)> 0){
            foreach ($products AS $product){
                $product_build=array();
                $product_build['name']= $product->product_name;
                $product_build['image']= url('/uploads/products/thumbs/'.$product->product_image);
//                $product_build['image']=   getProductImage($product->product_id)->pi_image_name;
                $product_build['alias']= $product->product_alias;
                $product_build['real_price']= $product->product_real_price;
                $product_build['sele_price']= $product->product_price;
                $product_build['product_url']= route('productDetails',['product_alias'=>$product->product_alias]);

                $returnArray[]=$product_build;
            }
        }else{
            $returnArray=null;
        }


        print json_encode($returnArray);

//        echo "<pre>";
//        print_r(dd($products));
        exit();
    }

}
