<?php

namespace App\Http\Controllers\frontend;

use App\Models\Orders;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

class PaymentController extends Controller
{
    public function __construct()
    {
        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
                $paypal_conf['client_id'],
                $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);

    }

    public function payWithpaypal(Request $request)
    {
        $totalamount = 0;

        $order = Orders::with('orderItems')->where('order_user_id', Auth::id())->orderBy('order_id', 'desc')->first();
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item_list = new ItemList();
        for ($i = 0; $i < count($order->orderItems); $i++) {

            $item = new Item();
            $item->setName($order->orderItems[$i]->oitem_product_name)/** item name **/
            ->setCurrency('INR')
                ->setQuantity($order->orderItems[$i]->oitem_qty)
                ->setPrice($order->orderItems[$i]->oitem_product_price);

            $totalamount += $order->orderItems[$i]->oitem_product_price;

            if ($i == 0) {
                $item_list->setItems(array($item));
            } else {
                $item_list->setItems(array_merge($item_list->getItems(), array($item)));
            }

        }
        $amount = new Amount();
        $amount->setCurrency('INR')
            ->setTotal($totalamount);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Your transaction description');
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('savePaymentStatus'))/** Specify return URL **/
        ->setCancelUrl(URL::route('home'));
        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));

        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            dd($ex);
            if (\Config::get('app.debug')) {
                \Session::put('error', 'Connection timeout');
                return Redirect::route('paywithpaypal');
            } else {
                \Session::put('error', 'Some error occur, sorry for inconvenient');
                return Redirect::route('paywithpaypal');
            }
        }
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        /** add payment ID to session **/
        \Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirect_url)) {
            /** redirect to paypal **/
            return Redirect::away($redirect_url);
        }
        \Session::put('error', 'Unknown error occurred');
        return Redirect::route('paywithpaypal');
    }

    public function getPaymentStatus()
    {
        /** Get the payment ID before session clear **/
        $payment_id = \Session::get('paypal_payment_id');
        /** clear the session payment ID **/
        \Session::forget('paypal_payment_id');

        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            \Session::put('error', 'Payment failed');
            return Redirect::route('/');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();

        $execution->setPayerId(Input::get('PayerID'));

        try {
            $result = $payment->execute($execution, $this->_api_context);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            dd($ex);
        }

        /**Execute the payment **/

        if ($result->getState() == 'approved') {

            $order = Orders::with('orderItems')->where('order_user_id', Auth::id())->orderBy('order_id', 'desc')->first();

            $updateData = array();
            $updateData['order_status'] = 'Completed';
            $updateData['order_transaction_id'] = $result->getId();

            Orders::where('order_id', $order->order_id)
                ->update($updateData);
            snedSMS(Auth::user()->mobile, 'Hi '.Auth::user()->name.', \n Your Order Seccessfully placed in skvapee.com');
            \Session::put('success', 'Payment success');
            return Redirect::route('paymentConfirmation');
        }
        \Session::put('error', 'Payment failed');
        return Redirect::route('/');
    }

}
