<?php

namespace App\Http\Controllers;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function editorimageupload(Request $request){


        if ($request->hasFile('upload')) {
            $uploadPath = public_path('/uploads/ckeditor5/');

            if (!file_exists($uploadPath)) {
                mkdir($uploadPath, 0777, true);
            }

            $extension = $request['upload']->getClientOriginalExtension();

            $fileName = time() .'.'. $extension;

            $request['upload']->move($uploadPath, $fileName);
            return json_encode(array('uploaded'=>true ,'url'=>url('/uploads/ckeditor5/'.$fileName)));
        }else{
            return json_encode(array('uploaded'=>false , array('error'=>'error ')));
        }

    }

}
