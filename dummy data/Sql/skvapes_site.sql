-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 08, 2018 at 12:21 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skvapes_site`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `banner_id` int(10) UNSIGNED NOT NULL,
  `banner_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `banner_description` longtext COLLATE utf8mb4_unicode_ci,
  `banner_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_country` int(11) NOT NULL,
  `banner_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`banner_id`, `banner_title`, `banner_image`, `banner_description`, `banner_link`, `banner_country`, `banner_status`, `created_at`, `updated_at`) VALUES
(1, 'Exceed Box with Exceed D22C', '1541675927slider01.jpg', 'Hailing design cues from the initial Joyetech EX series atomizer head, the brand-new EXCEED D22C atomizer came with childproof system apart from the precise top filling solution and the exquisite adjustable airflow control system.', 'http://www.skvapes.com/product/overview/AZC03D%20Intelligent%20Battery%20Digicharger%20Kit', 1, 'active', '2018-11-08 09:18:48', '2018-11-08 09:18:48'),
(2, 'Exceed Box with Exceed D22C', '1541675963slider02.jpg', 'Hailing design cues from the initial Joyetech EX series atomizer head, the brand-new EXCEED D22C atomizer came with childproof system apart from the precise top filling solution and the exquisite adjustable airflow control system.', 'http://www.skvapes.com/product/overview/AZC03D%20Intelligent%20Battery%20Digicharger%20Kit', 1, 'active', '2018-11-08 09:19:23', '2018-11-08 09:19:23'),
(3, 'Exceed Box with Exceed D22C', '1541675991slider03.jpg', 'Hailing design cues from the initial Joyetech EX series atomizer head, the brand-new EXCEED D22C atomizer came with childproof system apart from the precise top filling solution and the exquisite adjustable airflow control system.', 'http://www.skvapes.com/product/overview/AZC03D%20Intelligent%20Battery%20Digicharger%20Kit', 1, 'active', '2018-11-08 09:19:51', '2018-11-08 09:19:51'),
(4, 'Exceed Box with Exceed D22C', '1541676237slider04.jpg', 'Hailing design cues from the initial Joyetech EX series atomizer head, the brand-new EXCEED D22C atomizer came with childproof system apart from the precise top filling solution and the exquisite adjustable airflow control system.', 'http://www.skvapes.com/product/overview/AZC03D%20Intelligent%20Battery%20Digicharger%20Kit', 1, 'active', '2018-11-08 09:23:57', '2018-11-08 09:23:57');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `brand_id` int(10) UNSIGNED NOT NULL,
  `brand_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand_alias` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand_catid` int(11) NOT NULL,
  `brand_country` int(11) NOT NULL,
  `brand_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`brand_id`, `brand_name`, `brand_alias`, `brand_catid`, `brand_country`, `brand_image`, `brand_status`, `created_at`, `updated_at`) VALUES
(1, 'Joytech', 'joytech', 9, 1, NULL, 'active', '2018-11-24 16:00:38', '2018-11-24 16:00:38');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_alias` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `category_alias`, `category_image`, `category_status`, `created_at`, `updated_at`) VALUES
(2, 'ACCESSORIES', 'accessories', '1543040864joye-logo.png', 'active', '2018-10-29 02:15:59', '2018-11-24 16:08:27'),
(6, 'COIL', 'coil', '1543040910joye-logo.png', 'active', '2018-11-24 09:58:31', '2018-11-24 16:08:24'),
(7, 'ATOMIZER', 'atomizer', '1543040929joye-logo.png', 'active', '2018-11-24 09:58:50', '2018-11-24 16:08:23'),
(8, 'E-LIQUID', 'e-liquid', '1543040942joye-logo.png', 'active', '2018-11-24 09:59:02', '2018-11-24 16:08:21'),
(9, 'E-CIGARETTE', 'e-cigarette', '1543040961joye-logo.png', 'active', '2018-11-24 09:59:22', '2018-11-24 16:08:06');

-- --------------------------------------------------------

--
-- Table structure for table `contactus`
--

CREATE TABLE `contactus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` bigint(20) DEFAULT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `country_id` int(10) UNSIGNED NOT NULL,
  `country_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_phone_prefix` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_currency` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_language` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`country_id`, `country_name`, `country_code`, `country_phone_prefix`, `country_currency`, `country_language`, `country_status`, `created_at`, `updated_at`) VALUES
(1, 'INDIA', 'in', '91', 'INR', 'in', 'active', NULL, NULL),
(2, 'New Zealand', 'nz', '64', 'NZD', 'nz', 'active', NULL, NULL),
(3, 'Australia', 'au', '61', 'AUD', 'au', 'active', NULL, NULL),
(4, 'Malaysia', 'my', '60', 'MYR', 'my', 'active', NULL, NULL),
(5, 'United Kingdom', 'uk', '44', '	\r\nGBP', 'uk', 'active', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(33, '2014_10_12_000000_create_users_table', 1),
(34, '2018_10_15_130442_create_country_table', 1),
(35, '2018_10_16_102138_create_banners_table', 1),
(36, '2018_10_16_124330_create_categories_table', 1),
(37, '2018_10_17_095957_create_products_table', 1),
(38, '2018_10_17_101337_create_product_extra_information_table', 1),
(39, '2018_10_17_101635_create_product_images_table', 1),
(40, '2018_10_25_063633_create_news_letters_table', 1),
(41, '2018_10_25_072032_create_user_address_table', 1),
(42, '2018_10_27_091939_create_brands_table', 1),
(43, '2018_11_05_123649_create_orders_table', 1),
(44, '2018_11_05_124641_create_order_items_table', 1),
(45, '2018_11_06_100908_create_wishlist_table', 1),
(46, '2018_11_09_121501_create_password_resets_table', 1),
(47, '2018_11_12_183711_create_contactus_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `news_letters`
--

CREATE TABLE `news_letters` (
  `nl_id` int(10) UNSIGNED NOT NULL,
  `nl_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(10) UNSIGNED NOT NULL,
  `order_user_id` int(11) NOT NULL,
  `order_delivery_address` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `order_shipping_tarck_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_delivery_date` date DEFAULT NULL,
  `order_total_price` double(8,2) NOT NULL,
  `order_reference_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `order_user_id`, `order_delivery_address`, `order_status`, `order_shipping_tarck_id`, `order_delivery_date`, `order_total_price`, `order_reference_number`, `order_transaction_id`, `created_at`, `updated_at`) VALUES
(1, 3, 'a:9:{s:7:\"ua_name\";s:8:\"Suhasini\";s:10:\"ua_address\";s:7:\"fdfgdfg\";s:11:\"ua_landmark\";s:6:\"dfgdfg\";s:7:\"ua_city\";s:6:\"dfgfdg\";s:8:\"ua_state\";s:9:\"Telangana\";s:8:\"ua_email\";s:20:\"raja-buyer@gmail.com\";s:8:\"ua_phone\";i:90890890;s:10:\"ua_pincode\";i:500073;s:10:\"ua_country\";s:5:\"INDIA\";}', 'Pending', NULL, NULL, 800.00, '7ec7a28337ba236217b06ca8458f776d', NULL, '2018-12-06 17:09:07', '2018-12-06 17:09:07'),
(2, 3, 'a:9:{s:7:\"ua_name\";s:8:\"Suhasini\";s:10:\"ua_address\";s:7:\"fdfgdfg\";s:11:\"ua_landmark\";s:6:\"dfgdfg\";s:7:\"ua_city\";s:6:\"dfgfdg\";s:8:\"ua_state\";s:9:\"Telangana\";s:8:\"ua_email\";s:20:\"raja-buyer@gmail.com\";s:8:\"ua_phone\";i:90890890;s:10:\"ua_pincode\";i:500073;s:10:\"ua_country\";s:5:\"INDIA\";}', 'Pending', NULL, NULL, 800.00, '82eeb1350b178eb7a32e5c9d2fea4706', NULL, '2018-12-06 17:15:55', '2018-12-06 17:15:55'),
(3, 3, 'a:9:{s:7:\"ua_name\";s:8:\"Suhasini\";s:10:\"ua_address\";s:7:\"fdfgdfg\";s:11:\"ua_landmark\";s:6:\"dfgdfg\";s:7:\"ua_city\";s:6:\"dfgfdg\";s:8:\"ua_state\";s:9:\"Telangana\";s:8:\"ua_email\";s:20:\"raja-buyer@gmail.com\";s:8:\"ua_phone\";i:90890890;s:10:\"ua_pincode\";i:500073;s:10:\"ua_country\";s:5:\"INDIA\";}', 'Completed', NULL, NULL, 800.00, '9148951f935b6b2d376ec10fbf33b41e', 'PAY-8UD41494Y46184049LQERZAY', '2018-12-06 17:22:34', '2018-12-06 17:27:52'),
(4, 3, 'a:9:{s:7:\"ua_name\";s:8:\"Suhasini\";s:10:\"ua_address\";s:7:\"fdfgdfg\";s:11:\"ua_landmark\";s:6:\"dfgdfg\";s:7:\"ua_city\";s:6:\"dfgfdg\";s:8:\"ua_state\";s:9:\"Telangana\";s:8:\"ua_email\";s:20:\"raja-buyer@gmail.com\";s:8:\"ua_phone\";i:90890890;s:10:\"ua_pincode\";i:500073;s:10:\"ua_country\";s:5:\"INDIA\";}', 'Pending', NULL, NULL, 800.00, '7d676f7d097060e6e498ab5746e31465', NULL, '2018-12-06 17:47:36', '2018-12-06 17:47:36');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `oitem_id` int(10) UNSIGNED NOT NULL,
  `oitem_order_id` int(11) NOT NULL,
  `oitem_product_id` int(11) NOT NULL,
  `oitem_product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `oitem_qty` int(11) NOT NULL,
  `oitem_product_price` double(8,2) NOT NULL,
  `oitem_sub_total` double(8,2) NOT NULL,
  `oitem_discount_price` double(8,2) NOT NULL DEFAULT '0.00',
  `oitem_shipping_price` double(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`oitem_id`, `oitem_order_id`, `oitem_product_id`, `oitem_product_name`, `oitem_qty`, `oitem_product_price`, `oitem_sub_total`, `oitem_discount_price`, `oitem_shipping_price`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'xxx', 1, 800.00, 800.00, 0.00, 0.00, '2018-12-06 17:09:07', '2018-12-06 17:09:07'),
(2, 2, 1, 'xxx', 1, 800.00, 800.00, 0.00, 0.00, '2018-12-06 17:15:55', '2018-12-06 17:15:55'),
(3, 3, 1, 'xxx', 1, 800.00, 800.00, 0.00, 0.00, '2018-12-06 17:22:34', '2018-12-06 17:22:34'),
(4, 4, 1, 'xxx', 1, 800.00, 800.00, 0.00, 0.00, '2018-12-06 17:47:36', '2018-12-06 17:47:36');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_alias` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_availability` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `product_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_description` longtext COLLATE utf8mb4_unicode_ci,
  `product_real_price` double(8,2) NOT NULL DEFAULT '0.00',
  `product_price` double(8,2) NOT NULL DEFAULT '0.00',
  `product_brand` int(11) DEFAULT NULL,
  `product_country` int(11) DEFAULT NULL,
  `product_category` int(11) DEFAULT NULL,
  `product_shipping_price` double(8,2) DEFAULT '0.00',
  `product_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_name`, `product_alias`, `product_type`, `product_availability`, `product_color`, `product_description`, `product_real_price`, `product_price`, `product_brand`, `product_country`, `product_category`, `product_shipping_price`, `product_status`, `created_at`, `updated_at`) VALUES
(1, 'xxx', 'xxx', 'joytech', 'in_stock', 'red', '<p>ssfsfsfsf</p>', 500.00, 800.00, 1, 1, 9, 700.00, 'active', '2018-11-24 16:02:26', '2018-11-24 16:02:26'),
(2, 'The standard chunk', 'the-standard-chunk', 'Type21', 'in_stock', 'black', '<p>Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p>', 100.00, 90.00, 1, 1, 9, 0.00, 'active', '2018-12-08 15:41:39', '2018-12-08 15:41:39'),
(3, 'Bonorum et Malorum', 'bonorum-et-malorum', 'Type21', 'in_stock', 'Black', '<p>Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p>', 100.00, 90.00, 1, 1, 9, 0.00, 'active', '2018-12-08 15:45:04', '2018-12-08 15:45:04'),
(4, 'The Extremes of Good and Evil', 'the-extremes-of-good-and-evil', 'Type21', 'in_stock', 'Black', '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur</p>', 100.00, 90.00, 1, 1, 9, 0.00, 'active', '2018-12-08 15:48:23', '2018-12-08 15:48:23');

-- --------------------------------------------------------

--
-- Table structure for table `product_extra_information`
--

CREATE TABLE `product_extra_information` (
  `pe_id` int(10) UNSIGNED NOT NULL,
  `pe_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pe_description` longtext COLLATE utf8mb4_unicode_ci,
  `pe_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pe_position` int(11) DEFAULT NULL,
  `pe_product_id` int(11) NOT NULL,
  `pe_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_extra_information`
--

INSERT INTO `product_extra_information` (`pe_id`, `pe_title`, `pe_description`, `pe_image`, `pe_position`, `pe_product_id`, `pe_status`, `created_at`, `updated_at`) VALUES
(1, 'Lorem Ipsum Lorem Ipsum', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,', '1544267146productreviewimg01.png', 1, 1, 'active', '2018-12-08 15:35:46', '2018-12-08 15:35:46'),
(2, 'Neque porro quisquam', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur', '1544267192productreviewimg02.png', 2, 1, 'active', '2018-12-08 15:36:32', '2018-12-08 15:36:32'),
(3, 'passages of Lorem Ipsum', 'The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.', '1544267223productreviewimg03.png', 3, 1, 'active', '2018-12-08 15:37:03', '2018-12-08 15:37:03'),
(4, 'Neque porro quisquam', 'Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.', '1544267561productreviewimg01.png', 1, 2, 'active', '2018-12-08 15:42:42', '2018-12-08 15:42:42'),
(5, 'passages of Lorem Ipsum', 'Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.', '1544267584productreviewimg02.png', 2, 2, 'active', '2018-12-08 15:43:04', '2018-12-08 15:43:04'),
(6, 'Lorem Ipsum Lorem Ipsum', 'Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.', '1544267603productreviewimg03.png', 3, 2, 'active', '2018-12-08 15:43:23', '2018-12-08 15:43:23'),
(7, 'Neque porro quisquam', 'Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.', '1544267762productreviewimg01.png', 1, 3, 'active', '2018-12-08 15:46:02', '2018-12-08 15:46:02'),
(8, 'passages of Lorem Ipsum', 'Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.', '1544267776productreviewimg02.png', 2, 3, 'active', '2018-12-08 15:46:16', '2018-12-08 15:46:16'),
(9, 'Lorem Ipsum Lorem Ipsum', 'Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.', '1544267793productreviewimg03.png', 3, 3, 'active', '2018-12-08 15:46:33', '2018-12-08 15:46:33'),
(10, 'Neque porro quisquam', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur', '1544267950productreviewimg01.png', 1, 4, 'active', '2018-12-08 15:49:10', '2018-12-08 15:49:10'),
(11, 'passages of Lorem Ipsum', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur', '1544267962productreviewimg02.png', 2, 4, 'active', '2018-12-08 15:49:22', '2018-12-08 15:49:22'),
(12, 'Lorem Ipsum Lorem Ipsum', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur', '1544267977productreviewimg03.png', 3, 4, 'active', '2018-12-08 15:49:37', '2018-12-08 15:49:37');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `pi_id` int(10) UNSIGNED NOT NULL,
  `pi_product_id` int(11) NOT NULL,
  `pi_image_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pi_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`pi_id`, `pi_product_id`, `pi_image_name`, `pi_status`, `created_at`, `updated_at`) VALUES
(1, 1, '154305987301_Sk_Login.png', 'active', '2018-11-24 16:14:33', '2018-11-24 16:14:33'),
(2, 1, '1544267100detailgal01.png', 'active', '2018-12-08 15:35:01', '2018-12-08 15:35:01'),
(3, 1, '1544267101detailgal02.png', 'active', '2018-12-08 15:35:01', '2018-12-08 15:35:01'),
(4, 1, '1544267101detailgal03.png', 'active', '2018-12-08 15:35:01', '2018-12-08 15:35:01'),
(5, 1, '1544267101detailgal04.png', 'active', '2018-12-08 15:35:01', '2018-12-08 15:35:01'),
(6, 2, '1544267536proimg01.jpg', 'active', '2018-12-08 15:42:16', '2018-12-08 15:42:16'),
(7, 2, '1544267536proimg02.jpg', 'active', '2018-12-08 15:42:16', '2018-12-08 15:42:16'),
(8, 2, '1544267536proimg03.jpg', 'active', '2018-12-08 15:42:16', '2018-12-08 15:42:16'),
(9, 2, '1544267536proimg04.jpg', 'active', '2018-12-08 15:42:16', '2018-12-08 15:42:16'),
(10, 2, '1544267536proimg05.jpg', 'active', '2018-12-08 15:42:16', '2018-12-08 15:42:16'),
(11, 2, '1544267536proimg06.jpg', 'active', '2018-12-08 15:42:16', '2018-12-08 15:42:16'),
(12, 3, '1544267737proimg06.jpg', 'active', '2018-12-08 15:45:37', '2018-12-08 15:45:37'),
(13, 3, '1544267737prolist01.jpg', 'active', '2018-12-08 15:45:37', '2018-12-08 15:45:37'),
(14, 3, '1544267737prolist02.jpg', 'active', '2018-12-08 15:45:37', '2018-12-08 15:45:37'),
(15, 3, '1544267737prolist03.jpg', 'active', '2018-12-08 15:45:37', '2018-12-08 15:45:37'),
(16, 3, '1544267737prolist04.jpg', 'active', '2018-12-08 15:45:37', '2018-12-08 15:45:37'),
(17, 4, '1544267933proimg07.jpg', 'active', '2018-12-08 15:48:53', '2018-12-08 15:48:53'),
(18, 4, '1544267933proimg08.jpg', 'active', '2018-12-08 15:48:53', '2018-12-08 15:48:53'),
(19, 4, '1544267933prolist01.jpg', 'active', '2018-12-08 15:48:53', '2018-12-08 15:48:53'),
(20, 4, '1544267933prolist02.jpg', 'active', '2018-12-08 15:48:53', '2018-12-08 15:48:53'),
(21, 4, '1544267933prolist03.jpg', 'active', '2018-12-08 15:48:53', '2018-12-08 15:48:53'),
(22, 4, '1544267933prolist04.jpg', 'active', '2018-12-08 15:48:53', '2018-12-08 15:48:53');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_prefix` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` bigint(20) DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` int(11) NOT NULL COMMENT '404=admin,1=user',
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `mobile_prefix`, `mobile`, `password`, `gender`, `DOB`, `image`, `role`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin123', 'admin@gmail.com', '91', 9876987654, '$2y$10$08W9YZIoTse./hH7.zZn5uBx2f5nXLas5EnNl2bJVIVdd2d8Cc1g.', 'Female', '2017-03-16', NULL, 404, 'active', 'uST8DExNW8tgGQzxyCwJkNsOmWYF1EBWQ1nfRbGLz5OSVgB5iTla8ybxNHjZ', NULL, '2018-10-14 22:06:20'),
(3, 'Suhaisni', 'suhasini@gmail.com', '91', 9440765678, '$2y$10$08W9YZIoTse./hH7.zZn5uBx2f5nXLas5EnNl2bJVIVdd2d8Cc1g.', 'Female', '2016-03-30', '1540978247proimg01.jpg', 1, 'active', 'igK3oYmzqunWc9B5i3uZs9TjooyhIZwh9qIuuV3KTb4EXKAvXzaYW6zKud9Q', '2018-10-25 20:43:22', '2018-10-31 04:13:16');

-- --------------------------------------------------------

--
-- Table structure for table `user_address`
--

CREATE TABLE `user_address` (
  `ua_id` int(10) UNSIGNED NOT NULL,
  `ua_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ua_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ua_landmark` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ua_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ua_state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ua_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ua_phone` bigint(20) NOT NULL,
  `ua_defult` int(11) NOT NULL DEFAULT '0' COMMENT '1=default address',
  `ua_user_id` int(11) NOT NULL,
  `ua_country` int(11) NOT NULL,
  `ua_pincode` int(11) NOT NULL,
  `ua_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_address`
--

INSERT INTO `user_address` (`ua_id`, `ua_name`, `ua_address`, `ua_landmark`, `ua_city`, `ua_state`, `ua_email`, `ua_phone`, `ua_defult`, `ua_user_id`, `ua_country`, `ua_pincode`, `ua_status`, `created_at`, `updated_at`) VALUES
(1, 'Suhasini', 'fdfgdfg', 'dfgdfg', 'dfgfdg', 'Telangana', 'raja-buyer@gmail.com', 90890890, 0, 3, 1, 500073, 'active', '2018-12-06 17:08:58', '2018-12-06 17:35:35');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `wishlist_id` int(10) UNSIGNED NOT NULL,
  `wishlist_product_id` int(11) NOT NULL,
  `wishlist_user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `contactus`
--
ALTER TABLE `contactus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_letters`
--
ALTER TABLE `news_letters`
  ADD PRIMARY KEY (`nl_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`oitem_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `product_extra_information`
--
ALTER TABLE `product_extra_information`
  ADD PRIMARY KEY (`pe_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`pi_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_address`
--
ALTER TABLE `user_address`
  ADD PRIMARY KEY (`ua_id`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`wishlist_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `banner_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `brand_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `contactus`
--
ALTER TABLE `contactus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `country_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `news_letters`
--
ALTER TABLE `news_letters`
  MODIFY `nl_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `oitem_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product_extra_information`
--
ALTER TABLE `product_extra_information`
  MODIFY `pe_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `pi_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_address`
--
ALTER TABLE `user_address`
  MODIFY `ua_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `wishlist_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
