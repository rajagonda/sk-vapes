@extends('frontend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')

    <!--main Starts-->
    <section class="main">
        <!-- product overview header -->
        <section class="productview-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <!-- brudcrumb -->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb nobg mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Support</li>
                            </ol>
                        </nav>
                        <!--/ brudcrumb -->
                    </div>
                </div>
            </div>
        </section>
        <!--/ product overview header -->
        <!-- cart checkout pages -->
        <div class="account">
            <div class="container mb-5">
                <div class="row py-5 border-bottom">
                    <div class="col-lg-4">
                        <div class="addbox  rounded addplus  addbox-support text-center h-100 border-0">
                            <figure class="text-center align-self-center "><img src="/frontend/img/headphones.svg"
                                                                                class="svg supporticon py-5" alt=""
                                                                                title="">
                                <h4 class="h4 text-uppercase">Call Us At</h4>
                                <p class="fgray py-2">
                                    {{--+91 91213333383<br/>--}}
                                    +91 9985089794 <br/>
                                    +91 7702350236

                                </p>
                            </figure>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="addbox  rounded addplus  addbox-support text-center h-100 border-0">
                            <figure class="text-center align-self-center "><img src="/frontend/img/location.svg"
                                                                                class="svg supporticon py-5" alt=""
                                                                                title="">
                                <h4 class="h4 text-uppercase">Office Address</h4>
                                <p class="fgray py-2 px-3"># 2-6-135/136,Sarkar Building,1st Floor, Sikh Village,Near
                                    Mastana Hotel,Secunderabad-500009</p>
                            </figure>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="addbox  rounded addplus  addbox-support text-center h-100 border-0">
                            <figure class="text-center align-self-center "><img src="/frontend/img/mailbox.svg"
                                                                                class="svg supporticon py-5" alt=""
                                                                                title="">
                                <h4 class="h4 text-uppercase">Email At</h4>
                                <p class="fgray py-2 px-5">
                                    <a class="fgray" href="mailto:support@skvapes.com">support@skvapes.com</a>
                                    <a class="fgray" href="saisvmhyd@gmail.com">saisvmhyd@gmail.com</a>
                                </p>
                            </figure>
                        </div>
                    </div>
                </div>
                <!--row -->
                <div class="row pt-5">
                    <div class="col-lg-12 text-center">
                        <h3 class="h3 pb-5">Drop us a Line or Just Say Hello!</h3>
                    </div>
                </div>
                <div class="contactusformStatus">
                </div>
                <form class="" action="{{ route('support') }}" id="support">
                    <div class="row supportform">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input type="text" placeholder="Full name" class="form-control" name="name"></div>
                            <div class="form-group">
                                <input type="text" placeholder="E-Mail Address" class="form-control" name="email"></div>
                            <div class="form-group">
                                <input type="text" placeholder="Phone" class="form-control" name="phone"></div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Message" name="message"></textarea>
                            </div>
                        </div>
                        <div class="col-lg-12 text-center py-4">
                            <input type="submit" class="signbtn text-uppercase btn" value="Send Message">
                        </div>

                    </div>
                </form>
                <!--/ row -->
            </div>
        </div>
        <!--/ cartr checkout page-->
    </section>
    <!--/main Ends-->

@endsection
@section('footerScripts')

    <script>
        $(function () {
            {{--$("#searchinpt").autocomplete({--}}
                {{--source: "{{route('searchProductInfo')}}",--}}
                {{--minLength: 1,--}}
                {{--select: function (event, ui) {--}}
                    {{--// $("#searchInput").val(ui.item.value);--}}
                    {{--// $("#userID").val(ui.item.id);--}}
                {{--}--}}
            {{--}).data("ui-autocomplete")._renderItem = function (ul, item) {--}}
                {{--return $("<li class='ui-autocomplete-row'></li>")--}}
                    {{--.data("item.autocomplete", item)--}}
                    {{--.append(item.label)--}}
                    {{--.appendTo(ul);--}}
            {{--};--}}

        })
    </script>

@endsection