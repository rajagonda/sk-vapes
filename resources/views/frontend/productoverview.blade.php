@extends('frontend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')
    <!--main Starts-->
    <section class="main">
        <!-- product overview header -->
        <section class="productview-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <h1 class="mb-0 pt-1">{{ $product->product_name }}</h1>
                    </div>
                    <div class="col-lg-4 text-right viewbtns">
                        {{--<form class="" method="POST" action="{{ route('buyNow') }}"--}}
                              {{--accept-charset="UTF-8" class="">--}}
                            {{--{{ csrf_field() }}--}}
                            {{--<input type="hidden" name="id" value="{{ $product->product_id }}">--}}
                            {{--<input type="hidden" name="qty" value="1">--}}
                            {{--<input type="submit" value="BUY NOW" class="btn signbtn fmed">--}}
                        {{--</form>--}}
                        <a href="{{ route('productDetails',['product_alias'=>$product->product_alias]) }}" class="btn signbtn fmed">BUY NOW</a>

                    </div>
                </div>
            </div>
        </section>
        <!--/ product overview header -->
        <!-- product overview body -->

{{--        {{dump($product_info)}}--}}

        <section class="productview-body">

            {!! productInftroTheme($product_info) !!}

        </section>
        <!--/ product overview body -->
    </section>
    <!--/main Ends-->

@endsection
@section('footerScripts')

@endsection