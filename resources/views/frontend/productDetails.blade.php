@extends('frontend.layout')
@section('title', $title)

@section('headerStyles')
    <link rel="stylesheet" href="/frontend/assets/css/main-gal.css">
    <style>
        .productview-body ol, .productview-body ol li, .productview-body ul, .productview-body ul li {
            list-style: inherit;
            margin-left: 10px;
        }

        #show-img {
            padding: 2px;
            border: solid 1px #CCC;
        }

        #small-img-roll img {
            padding: 2px;
            border: solid 1px #CCC;
        }

        #small-img-roll img.active {
            padding: 2px;
            border: solid 1px #8a8a8a;
        }



        .bgredhover, .cartdet li, .form-control {

            border: #8a8a8a 1px solid !important;
        }

        .bgredhover a {
            color: #8a8a8a !important;

        }

        .bgredhover:hover {

            border: #db302f 1px solid !important;
        }

        .bgredhover:hover a {
            color: #FFF !important;

        }

    </style>
@endsection

@section('content')

    <!--main Starts-->
    <section class="main">
        <!-- product detail header -->
        <section class="productview-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-0 nobg brcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item"><a
                                            href="{{ route('userproducts',['category'=>$product->getCategory->category_alias]) }}">{{ $product->getCategory->category_name }}</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">{{ $product->product_name }}</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <!--/ product detail header -->
        <!-- product detail body -->
        <section class="product-detailbody">
            <!-- product detail top -->
            <div class="product-top">
                <div class="container">
                    <div class="row">
                        <!-- gallery slider -->
                        <div class="col-lg-6">
                            <div class="show" href="/uploads/products/{{ $productImages[0]->pi_image_name }}">
                                @if (count($productImages)>0)
                                    <img src="/uploads/products/{{ $productImages[0]->pi_image_name }}" id="show-img">

                                @else
                                    <img src="https://via.placeholder.com/300x300.png?text=No Image"
                                         class="image_1 img-fluid" alt="" title=""/>

                                @endif
                            </div>
                            <div class="small-img">
                                <a class="icon-left" id="prev-img"><i class="fas fa-chevron-left"></i></a>
                                <div class="small-container">
                                    <div id="small-img-roll">

                                        @if (count($productImages)>0)
                                            @foreach ($productImages as $image)

                                                <img src="/uploads/products/{{ $image->pi_image_name }}"
                                                     class="image_{{ $image->pi_id }} show-small-img" alt="" title=""
                                                        {{ ($loop->iteration==1) ? '' : "" }} />

                                            @endforeach
                                        @else
                                            <img src="https://via.placeholder.com/100x100.png?text=No Image"
                                                 class="" alt="" title=""/>

                                        @endif
                                    </div>

                                </div>
                                <a class="icon-right" id="next-img"><i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                        <!--/ gallery slider -->
                        <!-- product primary details -->
                        <div class="col-lg-6 prodesc">
                            <h1 class="fmed h3">{{ $product->product_name }}</h1>
                            <h2 class="fmed fred h3 py-2"><i class="fas fa-rupee-sign"></i>
                                <span>{{ $product->product_price }}</span></h2>
                            <table class="desctable">
                                <tr>
                                    <td>Availability</td>
                                    <td>:</td>
                                    <td class="{{ ($product->product_availability=='out_of_stock')?'text-danger':'text-success'  }}">{{ availability($product->product_availability) }}</td>
                                </tr>
                                <tr>
                                    <td>Product Type</td>
                                    <td>:</td>
                                    <td>{{ $product->product_type }}</td>
                                </tr>
                                @if ($product->product_color)
                                    <tr>
                                        <td>Colour</td>
                                        <td>:</td>
                                        <td>
                                            {{--                                            {{ $product->product_color }}--}}
                                            <?php
                                            $colors = explode(',', $product->product_color);
                                            ?>

                                            <select name="color" class="selectcolor form-control float-left"
                                                    style="width: 100px">
                                                @foreach($colors AS $color)
                                                    <option value="{{$color}}">{{$color}}</option>
                                                @endforeach

                                            </select>

                                        </td>
                                    </tr>
                                @endif
                                @if ($product->product_size)
                                    <tr>
                                        <td>Size/Capacity</td>
                                        <td>:</td>
                                        <td>
                                            {{--                                            {{ $product->product_color }}--}}
                                            <?php
                                            $sizes = explode(',', $product->product_size);
                                            ?>

                                            <select name="size" class="selectsize form-control float-left"
                                                    style="width: 100px">
                                                @foreach($sizes AS $size)
                                                    <option value="{{$size}}">{{$size}}</option>
                                                @endforeach

                                            </select>

                                        </td>
                                    </tr>



                                @endif


                                <tr>
                                    <td>Quantity</td>
                                    <td>:</td>
                                    <td>
                                        <ul class="cartdet nav py-0">
                                            <li class="nav-item">
                                                <form>
                                                    <div class="value-button"
                                                         onclick="decreaseValue(1)"
                                                         value="Decrease Value">-
                                                    </div>
                                                    <input type="number" id="val_1"
                                                           value="1"
                                                           class="number itemQty1 productqty"/>
                                                    <div class="value-button"
                                                         onclick="increaseValue(1)"
                                                         value="Increase Value">+
                                                    </div>
                                                </form>
                                            </li>
                                            <li class="nav-item bgredhover">
                                                <a class="text-uppercase descanch px-5 productaddwishlist{{ $product->product_id }} addToWishList @if(in_array($product->product_id,$wishlistProducts)) @endif"
                                                   href="#" data-id="{{ $product->product_id }}"
                                                   data-userid="{{ Auth::id() }}"
                                                > <span class="icon-heart moonfont"></span>
                                                </a>
                                            </li>


                                        </ul>
                                    </td>
                                </tr>


                                @if ($product->product_availability == 'in_stock')

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>

                                            <ul class="cartdet nav py-0">


                                                <li class="nav-item bgredhover buy-botton">
                                                    <a class="text-uppercase descanch addToCartInProduct px-4" href="#"
                                                       data-id="{{ $product->product_id }}" title="Add to Cart"><span
                                                                class="icon-online-shopping-cart moonfont"></span> Add
                                                        to
                                                        Cart </a>
                                                </li>

                                                <li class="nav-item bgredhover buy-botton">
                                                    {{--<a class="text-uppercase descanch" href="#"--}}
                                                    {{--data-id="" title="Buy Now"><span--}}
                                                    {{--class="icon-online-shopping-cart moonfont"></span>Buy Now</a>--}}

                                                    <form class="buyProductForm" action="{{ route('buyProduct') }}"
                                                          id=""
                                                          method="GET">

                                                        <input type="hidden" name="id"
                                                               value="{{ $product->product_id }}"/>
                                                        <input type="hidden" name="qty" class="pqty"/>
                                                        <input type="hidden" name="size" class="psize"/>
                                                        <input type="hidden" name="color" class="pcolor"/>
                                                        <a href="#" title="Buy Now"
                                                           class="text-uppercase descanch buyproduct px-4"><span
                                                                    class="icon-online-shopping-cart moonfont"></span>
                                                            Buy
                                                            Now</a>

                                                    </form>
                                                </li>
                                            </ul>

                                        </td>
                                    </tr>


                                @endif


                            </table>


                            <!-- /add to cart columns -->
                            <!-- delivery to -->
                        {{--<div class="deliverto pb-2">--}}
                        {{--<p class="pb-2 fmed">Delivery to</p>--}}
                        {{--<div class="inputdelivery py-2">--}}
                        {{--<input class="float-left" type="text" placeholder="Please enter your Pin code"/>--}}
                        {{--<input class="float-left" type="submit" placeholder="Check"/></div>--}}
                        {{--</div>--}}
                        <!-- /delivery to -->
                            <!-- share this product -->
                        {{--<p class="py-2 fmed">Share this product</p>--}}
                        {{--<ul class="sharepro nav">--}}
                        {{--<li class="nav-item"><a class="fb" href="#" target="_blank"><i--}}
                        {{--class="fab fa-facebook-f"></i> Share <span>0</span></a></li>--}}
                        {{--<li class="nav-item"><a class="tw" href="#" target="_blank"><i--}}
                        {{--class="fab fa-twitter"></i> Tweet <span>0</span></a></li>--}}
                        {{--<li class="nav-item"><a class="pin" href="#" target="_blank"><i--}}
                        {{--class="fab fa-pinterest-p"></i> Pin it <span>0</span></a></li>--}}
                        {{--<li class="nav-item"><a class="gplus" href="#" target="_blank"><i--}}
                        {{--class="fab fa-google-plus-g"></i> <span>+1</span></a></li>--}}
                        {{--</ul>--}}
                        <!--/ share this product -->
                        </div>
                        <!--/ product primary details -->
                    </div>
                </div>
            </div>
            <!-- / product detail top -->
            <!-- product detail features -->
            <section class="detail-features graybg">
                <div class="detailfeat-header">
                    <div class="container">
                        <ul id="mainNav" class="nav justify-content-center">
                            {{--<li class="nav-item"><a href="#overview" class="nav-link"><span>Overview</span></a></li>--}}
                            <li class="nav-item"><a href="#specs" class="nav-link"><span>Specs</span></a></li>
                            <li class="nav-item"><a href="#overview" class="nav-link"><span>Overview</span></a></li>
                            <li class="nav-item"><a href="#rate" class="nav-link"><span>Rating &amp; Overview</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- product overview-->
            {{--<div id="overview" class="detscrolldiv">--}}
            {{--<div class="container">--}}
            {{--<div class="row justify-content-center">--}}
            {{--<div class="col-lg-12">--}}
            {{--<h3 class="h3 py-2">AZC03D Intelligent Battery Digicharger Kit</h3>--}}
            {{--<h6 class="h6 pb-2">Introduction</h6>--}}
            {{--<p>Introducing the innovative Joyetech NCFilmTM heater along with the CUBIS Max tank.--}}
            {{--Being a coil-less design, this NCFilmTM heater can be easily cleaned thus supports a--}}
            {{--long lifespan. The CUBIS Max atomizer allows a simple replacement of cotton for a--}}
            {{--pure and delicate vaping experience. The ULTEX T80, a powerful vape pen style system--}}
            {{--is beautifully crafted. It’s powered by single replaceable 18650 battery paired with--}}
            {{--multiple output modes. A 0.49-inch OLED screen is place on the downside. This--}}
            {{--intuitive display presents you with everything you need. Having the top-fill and--}}
            {{--top-airflow system, you’re free of any leakage. Best at 40w, you can enjoy a huge--}}
            {{--but flavored clouds.</p>--}}
            {{--</div>--}}
            {{--<div class="col-lg-6 text-center py-5"><img src="/frontend/img/data/productreviewimg01.png"--}}
            {{--alt="" title="" class="img-fluid"></div>--}}
            {{--</div>--}}
            {{--</div>--}}
            <!--/ product overview-->

                <br/>

                <div id="specs" class="detscrolldiv">
                    <div class="container py-4">
                        <div class="row">
                            <div class="col-lg-12">

                                <h5 class="h5 py-2">Description</h5>

                                {{--<h6 class="h6"><span class="fred">19687 </span>Reviews</h6>--}}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                {!! $product->product_description !!}

                            </div>
                        </div>
                    </div>
                </div>

                <div id="overview" class="detscrolldiv">
                    <div class="container py-4">

                        <!--main Starts-->
                        <section class="row">
                            <!-- product overview body -->

                            {{--        {{dump($product_info)}}--}}

                            <section class="productview-body">

                                {!! productInftroTheme($product_info) !!}

                            </section>
                            <!--/ product overview body -->
                        </section>
                        <!--/main Ends-->
                    </div>
                </div>
                <!--rate -->
                <div id="rate" style="display: none">
                    <div class="container whitebg">
                        <!-- Number of reviews-->
                        <div class="specsdetails">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h5 class="h5 py-2">Customer Reviews</h5>
                                    <h6 class="h6"><span class="fred">19687 </span>Reviews</h6>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3 ratecol align-self-center">
                                    <h2 class="fbold fred">4.9</h2>
                                    <p class="ratestars"><span><i class="fas fa-star fred"></i></span> <span><i
                                                    class="fas fa-star fred"></i></span><span><i
                                                    class="fas fa-star fred"></i></span><span><i
                                                    class="fas fa-star fred"></i></span><span><i
                                                    class="fas fa-star-half-alt fred"></i></span></p>
                                    <p>4.9 out of 5 stars</p>
                                </div>
                                <!-- rate percentage -->
                                <div class="col-lg-5">
                                    <table width="100%">
                                        <tr>
                                            <td width="50px"><span>18407</span></td>
                                            <td>
                                                <div class="progress" style="height:10px">
                                                    <div class="progress-bar" style="width:80%;height:10px"></div>
                                                </div>
                                            </td>
                                            <td width="80px"><span class="fmed starq"><input
                                                            type="checkbox"> 5 Star</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="50px"><span>1182</span></td>
                                            <td>
                                                <div class="progress" style="height:10px">
                                                    <div class="progress-bar" style="width:60%;height:10px"></div>
                                                </div>
                                            </td>
                                            <td width="80px"><span class="fmed starq"><input
                                                            type="checkbox"> 4 Star</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="50px"><span>98</span></td>
                                            <td>
                                                <div class="progress" style="height:10px">
                                                    <div class="progress-bar" style="width:20%;height:10px"></div>
                                                </div>
                                            </td>
                                            <td width="80px"><span class="fmed starq"><input
                                                            type="checkbox"> 3 Star</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="50px"><span>0</span></td>
                                            <td>
                                                <div class="progress" style="height:10px">
                                                    <div class="progress-bar" style="width:2%;height:10px"></div>
                                                </div>
                                            </td>
                                            <td width="80px"><span class="fmed starq"><input
                                                            type="checkbox"> 2 Star</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="50px"><span>0</span></td>
                                            <td>
                                                <div class="progress" style="height:10px">
                                                    <div class="progress-bar" style="width:1%;height:10px"></div>
                                                </div>
                                            </td>
                                            <td width="80px"><span class="fmed starq"><input
                                                            type="checkbox"> 1 Star</span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!--/ rate percentage -->
                                <!-- image -->
                                <div class="col-lg-4 text-center align-self-center">
                                    <figure class="revimg mx-auto"><img
                                                src="/frontend/img/data/productreviewimg01.png" alt="" title=""
                                                class="img-fluid"></figure>
                                    <a class="btn custombtn btn2" href="#">VIEW PRODUCT</a>
                                </div>
                                <!--/ image -->
                            </div>
                        </div>
                        <!--/ Number of reviews-->
                    </div>
                </div>
                <!--/rate -->
                <!-- most helpful reviews -->
                <div class="mosthelpful" style="display: none">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <h6 class="h6 fmed p-3 border-bottom">Most Helpful</h6>
                            </div>
                        </div>
                        <!--help row -->
                        <div class="row border-bottom py-3">
                            <div class="col-lg-2 text-center">
                                <figure class="helpthumb">
                                    <a href="#"><img src="/frontend/img/helpthumb.png" alt="" title=""></a>
                                </figure>
                            </div>
                            <div class="col-lg-8">
                                <h6 class="helptitle fmed">Super charger</h6>
                                <p class="fgray">India Standard Charger (2A Fast Charging</p>
                                <p class="fblack pt-2">charger is so good fast charging</p>
                                <ul class="nav helplistthumbs">
                                    <li class="nav-item">
                                        <a class="nav-link pt-0">
                                            <a href="#"><img class="helpth" src="/frontend/img/thumbhelpful.jpg"></a>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link pt-0">
                                            <a href="#"><img class="helpth"
                                                             src="/frontend/img/thumbhelpful.jpg"></a>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link pt-0">
                                            <a href="#"><img class="helpth"
                                                             src="/frontend/img/thumbhelpful.jpg"></a>
                                        </a>
                                    </li>
                                </ul>
                                <div class="replyinput">
                                    <p class="text-right py-2"><a class="fgray" href="#"><i
                                                    class="fas fa-thumbs-up"></i> Likes<span>(131)</span></a></p>
                                    <div class="deliverto pb-2">
                                        <div class="inputdelivery w-100 py-2">
                                            <input class="float-left" type="text"
                                                   placeholder="Writedown your reply"/>
                                            <input class="float-left" type="submit" value="Reply"/></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 text-center">
                                <p>February 3, 2018</p>
                            </div>
                        </div>
                        <!--/help row -->
                        <!--help row -->
                        <div class="row pt-3 pb-5">
                            <div class="col-lg-2 text-center">
                                <figure class="helpthumb">
                                    <a href="#"><img src="/frontend/img/helpthumb.png" alt=""
                                                     title=""></a>
                                </figure>
                            </div>
                            <div class="col-lg-8">
                                <h6 class="helptitle fmed">Super charger</h6>
                                <p class="fgray">India Standard Charger (2A Fast Charging</p>
                                <p class="fblack pt-2">charger is so good fast charging</p>
                                <ul class="nav helplistthumbs">
                                    <li class="nav-item">
                                        <a class="nav-link pt-0">
                                            <a href="#"><img class="helpth"
                                                             src="/frontend/img/thumbhelpful.jpg"></a>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link pt-0">
                                            <a href="#"><img class="helpth"
                                                             src="/frontend/img/thumbhelpful.jpg"></a>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link pt-0">
                                            <a href="#"><img class="helpth"
                                                             src="/frontend/img/thumbhelpful.jpg"></a>
                                        </a>
                                    </li>
                                </ul>
                                <div class="replyinput">
                                    <p class="text-right py-2"><a class="fgray" href="#"><i
                                                    class="fas fa-thumbs-up"></i> Likes<span>(131)</span></a></p>
                                    <div class="deliverto pb-2">
                                        <div class="inputdelivery w-100 py-2">
                                            <input class="float-left" type="text"
                                                   placeholder="Writedown your reply"/>
                                            <input class="float-left" type="submit" value="Reply"/></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 text-center">
                                <p>February 3, 2018</p>
                            </div>
                        </div>
                        <!--/help row -->
                    </div>
                </div>
                <!-- /most helpful reviews -->
                </div>
            </section>
            <!-- / product detail features -->
        </section>
        <!--/ product detail body -->
    </section>
    <!--/main Ends-->

@endsection
@section('footerScripts')

    <!--product ditail gallery-->
    <script src="/frontend/assets/js/zoom-image.js"></script>

    <script>
        $('.show').zoomImage();
        $('.show-small-img:first-of-type').addClass('active')
        $('.show-small-img:first-of-type').attr('alt', 'now').siblings().removeAttr('alt')
        $('.show-small-img').click(function () {
            $('#show-img').attr('src', $(this).attr('src'))
            $('#big-img').attr('src', $(this).attr('src'))
            $(this).attr('alt', 'now').siblings().removeAttr('alt')
            $(this).addClass('active').siblings().removeClass('active')
            if ($('#small-img-roll').children().length > 4) {
                if ($(this).index() >= 3 && $(this).index() < $('#small-img-roll').children().length - 1) {
                    $('#small-img-roll').css('left', -($(this).index() - 2) * 76 + 'px')
                } else if ($(this).index() == $('#small-img-roll').children().length - 1) {
                    $('#small-img-roll').css('left', -($('#small-img-roll').children().length - 4) * 76 + 'px')
                } else {
                    $('#small-img-roll').css('left', '0')
                }
            }
        })
        //
        $('#next-img').click(function () {
            $('#show-img').attr('src', $(".show-small-img[alt='now']").next().attr('src'))
            $('#big-img').attr('src', $(".show-small-img[alt='now']").next().attr('src'))
            $(".show-small-img[alt='now']").next().addClass('active').siblings().removeClass('active')
            $(".show-small-img[alt='now']").next().attr('alt', 'now').siblings().removeAttr('alt')
            if ($('#small-img-roll').children().length > 4) {
                if ($(".show-small-img[alt='now']").index() >= 3 && $(".show-small-img[alt='now']").index() < $('#small-img-roll').children().length - 1) {
                    $('#small-img-roll').css('left', -($(".show-small-img[alt='now']").index() - 2) * 76 + 'px')
                } else if ($(".show-small-img[alt='now']").index() == $('#small-img-roll').children().length - 1) {
                    $('#small-img-roll').css('left', -($('#small-img-roll').children().length - 4) * 76 + 'px')
                } else {
                    $('#small-img-roll').css('left', '0')
                }
            }
        })
        //
        $('#prev-img').click(function () {
            $('#show-img').attr('src', $(".show-small-img[alt='now']").prev().attr('src'))
            $('#big-img').attr('src', $(".show-small-img[alt='now']").prev().attr('src'))
            $(".show-small-img[alt='now']").prev().addClass('active').siblings().removeClass('active')
            $(".show-small-img[alt='now']").prev().attr('alt', 'now').siblings().removeAttr('alt')
            if ($('#small-img-roll').children().length > 4) {
                if ($(".show-small-img[alt='now']").index() >= 3 && $(".show-small-img[alt='now']").index() < $('#small-img-roll').children().length - 1) {
                    $('#small-img-roll').css('left', -($(".show-small-img[alt='now']").index() - 2) * 76 + 'px')
                } else if ($(".show-small-img[alt='now']").index() == $('#small-img-roll').children().length - 1) {
                    $('#small-img-roll').css('left', -($('#small-img-roll').children().length - 4) * 76 + 'px')
                } else {
                    $('#small-img-roll').css('left', '0')
                }
            }
        })

    </script>

    <script type="text/javascript">
        // product detail gallery script
        $(document).ready(function () {
            $('#gallery').simplegallery({
                galltime: 400,
                gallcontent: '.content',
                gallthumbnail: '.thumbnail',
                gallthumb: '.thumb'
            });
            $('a.goback').click(function () {
                window.history.back();
                return false;
            });
            $('.buyproduct').click(function () {

                if ($('.productqty').val()) {
                    var qty = $('.productqty').val();
                } else {
                    var qty = 1;
                }

                if ($('.selectsize').val()) {
                    var size = $('.selectsize').val();
                } else {
                    var size = '';
                }

                if ($('.selectcolor').val()) {
                    var color = $('.selectcolor').val();
                } else {
                    var color = '';
                }

                $('.pqty').val(qty);
                $('.psize').val(size);
                $('.pcolor').val(color);
                $('.buyProductForm').submit();
            });
        });

    </script>
    <script>
        function increaseValue(id) {
            var value = parseInt($('#val_' + id).val(), 10);
            value = isNaN(value) ? 0 : value;
            value++;
            $('#val_' + id).val(value);
        }

        function decreaseValue(id) {
            var value = parseInt($('#val_' + id).val(), 10);
            value = isNaN(value) ? 0 : value;
            value < 1 ? value = 1 : '';
            value--;
            $('#val_' + id).val(value);
        }

    </script>
    <script>
        // smooth scrolling
        $('a[href*="#"]:not([href="#"]):not([href="#show"]):not([href="#hide"])').click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });

    </script>
@endsection