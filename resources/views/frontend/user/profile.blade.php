@extends('frontend.layout')

@section('title', $title)

@section('headerStyles')

@endsection

@section('content')

    <!--main Starts-->
    <section class="main">
        <!-- product overview header -->
        <section class="productview-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <!-- brudcrumb -->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb nobg mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Profile Information</li>
                            </ol>
                        </nav>
                        <!--/ brudcrumb -->
                    </div>
                </div>
            </div>
        </section>
        <!--/ product overview header -->
        <!-- cart checkout pages -->
        <div class="account">
            <div class="container mb-5">
                <!-- payment page-->
                <div class="paymentpage">
                    <div class="tabin mt-4">
                        <div class="row">
                            <!-- left account nav-->
                            <div class="col-lg-3 col-md-3 border-right pr-0">
                                <div class="cartheadrow">
                                    <h5 class="h5 fmedf p-2">MY ACCOUNT</h5>
                                </div>
                                @include('frontend._partials.userprofile-nav')
                            </div>
                            <!--/ left account nav -->
                            <!-- right account body -->
                            <div class="col-lg-9 col-md-9">
                                @if (Session::has('flash_message'))
                                    <br/>
                                    <div class="alert alert-success alert-dismissable">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>{{ Session::get('flash_message' ) }}</strong>
                                    </div>
                                @endif
                                <div class="accountrt p-3">
                                    <h5 class="h5 fmed border-bottom pb-3">Profile Information</h5>
                                    <!-- account right body -->
                                    <div class="accountrtbody_view">

                                        <div class="row">
                                            {{ csrf_field() }}
                                            <div class="col-lg-2">
                                                <figure class="propic">
                                                    @if(($user) && ($user->image))
                                                        <div class="imagediv{{ $user->id }}">
                                                            <img src="/uploads/users/thumbs/{{$user->image}}"/>

                                                        </div>
                                                @endif

                                                <!--/ file input -->
                                                </figure>
                                            </div>
                                            <div class="col-lg-8">

                                                <div class="row">
                                                    <div class="col-lg-4 text-right pr-0">
                                                        <label>Name</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="form-group">
                                                            <p class="view">{{ ($user->name) ? $user->name : '' }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-4 text-right pr-0">
                                                        <label>Your Gender</label>
                                                    </div>
                                                    <div class="col-lg-8 align-self-center">
                                                        <div class="form-group">
                                                            <p class="view">{{ ($user->gender) ? $user->gender : '' }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-4 text-right pr-0">
                                                        <label>Date of Birth</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="form-group ">
                                                            <p class="view">{{ ((($user) && ($user->DOB)) ? \Carbon\Carbon::parse($user->DOB)->format('d-m-Y') : '') }}</p>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-4 text-right pr-0">
                                                        <label>Phone Number</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="form-group">
                                                            <p class="view">
                                                                {{ ($user->mobile_prefix) ? $user->mobile_prefix : ''}} {{ ($user->mobile) ? $user->mobile : '' }}
                                                            </p>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-4 text-right pr-0">
                                                        <label>Email Address</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="form-group">

                                                            <p class="view">
                                                                {{ ($user->email) ? $user->email : ''}}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-4 text-right">
                                                        <label>&nbsp;</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="form-group mt-2">
                                                            <a class="signbtn btn editprofile">Edit</a>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                    <!--/ account right body -->
                                    <!-- account right body -->
                                    <div class="accountrtbody" style="display: none">
                                        <form method="POST" id="profileInfo" action="{{ route('userprofile') }}"
                                              accept-charset="UTF-8" class="py-4" enctype="multipart/form-data">
                                            <div class="row">
                                                {{ csrf_field() }}
                                                <div class="col-lg-2">
                                                    <figure class="propic">
                                                        @if(($user) && ($user->image))
                                                            <div class="imagediv{{ $user->id }}">
                                                                <img src="/uploads/users/thumbs/{{$user->image}}"/>
                                                                <a href="#" id="{{$user->id}}"

                                                                   class="delete_user_image btn btn-danger "
                                                                   onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                                    Delete
                                                                </a>

                                                            </div>
                                                    @endif
                                                    <!-- file  input-->
                                                        <div class="input-file-container">
                                                            <table class="inputfile">
                                                                <tr>
                                                                    <td>
                                                                        <input class="input-file" id="my-file"
                                                                               type="file" name="image">
                                                                        <label tabindex="0" for="my-file"
                                                                               class="input-file-trigger"><i
                                                                                    class="fas fa-camera"></i> Upload
                                                                            Image</label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <p class="file-return"></p>
                                                        <!--/ file input -->
                                                    </figure>
                                                </div>
                                                <div class="col-lg-8">

                                                    <div class="row">
                                                        <div class="col-lg-4 text-right pr-0">
                                                            <label>Name</label>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="form-group">
                                                                <input type="text" placeholder="Name"
                                                                       class="form-control" name="name"
                                                                       value="{{ !empty(old('name')) ? old('name') : ((($user) && ($user->name)) ? $user->name : '') }}">
                                                                @if ($errors->has('name'))
                                                                    <span class="text-danger help-block">{{ $errors->first('name') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-4 text-right pr-0">
                                                            <label>Your Gender</label>
                                                        </div>
                                                        <div class="col-lg-8 align-self-center">
                                                            <div class="form-group">
                                                                <label class="pr-3 pt-0">
                                                                    <input type="radio" name="gender"
                                                                           value="Male" {{ (!empty(old('gender')) && old('gender')=="Male")  ? 'selected' : ((($user) && ($user->gender == "Male")) ? 'checked' : '') }}><span
                                                                            class="px-2">Male</span></label>
                                                                <label class="pr-3 pt-0">
                                                                    <input type="radio" name="gender"
                                                                           value="Female" {{ (!empty(old('gender')) && old('gender')=="Female")  ? 'selected' : ((($user) && ($user->gender == "Female")) ? 'checked' : '') }}><span
                                                                            class="px-2">Female</span></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-4 text-right pr-0">
                                                            <label>Date of Birth</label>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="form-group ">
                                                                <input type="text" placeholder="DD"
                                                                       class="form-control datepicker" name="DOB"
                                                                       value="{{ !empty(old('DOB')) ? old('DOB') : ((($user) && ($user->DOB)) ? \Carbon\Carbon::parse($user->DOB)->format('d-m-Y') : '') }}">
                                                                @if ($errors->has('DOB'))
                                                                    <span class="text-danger help-block">{{ $errors->first('DOB') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-4 text-right pr-0">
                                                            <label>Phone Number</label>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <div class="form-group">
                                                                <select class="form-control" name="mobile_prefix">
                                                                    @if(count(getCountriesByPrefixes())>0)
                                                                        @foreach(getCountriesByPrefixes() as $prefix)
                                                                            <option value="{{ $prefix }}" {{ (!empty(old('mobile_prefix')) && old('mobile_prefix')==$prefix)  ? 'selected' : ((($user) && ($user->mobile_prefix == $prefix)) ? 'selected' : '') }}>{{ $prefix }}</option>
                                                                        @endforeach
                                                                    @else
                                                                        <option>No Prefixes Found</option>
                                                                    @endif

                                                                </select>
                                                                @if ($errors->has('mobile_prefix'))
                                                                    <span class="text-danger help-block">{{ $errors->first('mobile_prefix') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-5">
                                                            <div class="form-group">
                                                                <input type="text" placeholder="Mobile No"
                                                                       class="form-control" name="mobile"
                                                                       value="{{ !empty(old('mobile')) ? old('mobile') : ((($user) && ($user->mobile)) ? $user->mobile : '') }}">
                                                                @if ($errors->has('mobile'))
                                                                    <span class="text-danger help-block">{{ $errors->first('mobile') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-4 text-right pr-0">
                                                            <label>Email Address</label>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="form-group">
                                                                <input type="text" placeholder="Email"
                                                                       class="form-control" name="email" readonly
                                                                       value="{{ !empty(old('email')) ? old('email') : ((($user) && ($user->email)) ? $user->email : '') }}">
                                                                @if ($errors->has('email'))
                                                                    <span class="text-danger help-block">{{ $errors->first('email') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-4 text-right">
                                                            <label>&nbsp;</label>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="form-group mt-2">
                                                                <input type="button" value="Back"
                                                                       class="signbtn btn  editprofile"/>
                                                                <input type="submit" value="Update"
                                                                       class="signbtn btn "/>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                    <!--/ account right body -->
                                </div>
                            </div>
                            <!--/ right account body -->
                        </div>
                    </div>
                </div>
                <!-- order information -->
            </div>
        </div>
        <!--/ cartr checkout page-->
    </section>
    <!--/main Ends-->

@endsection
@section('footerScripts')
    <script type="text/javascript">
        // $('.datepicker').datepicker({dateFormat: 'dd-mm-yy'});
    </script>

    <script src="{{url('frontend/modules/users/js/users.js')}}"></script>
    <script>
        $('.editprofile').on('click', function () {
            $(".accountrtbody_view").toggle(500);
            $(".accountrtbody").toggle(500);
        })

    </script>

@endsection