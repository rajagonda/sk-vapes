@extends('frontend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')

    <!--main Starts-->
    <section class="main">
        <!-- product overview header -->
        <section class="productview-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <!-- brudcrumb -->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb nobg mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Address Book</li>
                            </ol>
                        </nav>
                        <!--/ brudcrumb -->
                    </div>
                </div>
            </div>
        </section>
        <!--/ product overview header -->
        <!-- cart checkout pages -->
        <div class="account">
            <div class="container mb-5">
                <!-- payment page-->
                <div class="paymentpage">
                    <!--
                   <div class="form-group mt-2"> <a href="#" class="signbtn btn ">CHANGE PASSWORD</a> </div>
                   -->
                    <div class="tabin mt-4">
                        <div class="row">
                            <!-- left account nav-->
                            <div class="col-lg-3 border-right pr-0">
                                <div class="cartheadrow">
                                    <h5 class="h5 fmedf p-2">MY ACCOUNT</h5>
                                </div>
                                @include('frontend._partials.userprofile-nav')
                            </div>
                            <!--/ left account nav -->
                            <!-- right account body -->
                            <div class="col-lg-9">
                                @if (Session::has('flash_message'))
                                    <br/>
                                    <div class="alert alert-success alert-dismissable">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>{{ Session::get('flash_message' ) }}</strong>
                                    </div>
                                @endif
                                <div class="accountrt p-3">
                                    <h5 class="h5 fmed border-bottom pb-3">Address Book</h5>
                                    <!-- account right body -->
                                    <div class="accountrtbody">
                                        <div class="row pt-3">
                                            <div class="col-lg-6 col-sm-6 text-center ">
                                                <div class="addbox dashed rounded  addplus d-flex  align-items-center">
                                                    <a class="icadd mx-auto" href="#"
                                                       data-toggle="modal" data-target="#addaddress"><span
                                                                class="acaddin"><i class="fas fa-plus"></i></span>
                                                        <span class="d-block">Add Address</span>
                                                    </a></div>
                                            </div>

                                            @if(count($userAddresses)>0)
                                                @foreach($userAddresses as $address)

                                                    <div class="col-lg-6 col-sm-6">
                                                        <div class="addbox position-relative border rounded  align-items-center">
                                                            <h4 class="h4">{{ $address->ua_name }}</h4>
                                                            <p class="fgray">Address : {{ $address->ua_address }}
                                                                ,{{ $address->ua_city }},{{ $address->ua_state }}
                                                                ,{{ $address->getCountry->country_name }}</p>
                                                            <p class="fgray">Landmark : {{ $address->ua_landmark }}</p>
                                                            <p class="fgray">Email : {{ $address->ua_email }}</p>
                                                            <p class="fgray">Phone : {{ $address->ua_phone }}</p>
                                                            <p class="fgray">Pincode : {{ $address->ua_pincode }}</p>
                                                            <p class="pt-3 editdel">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <a href="#" data-toggle="modal"
                                                                           data-target="#updateAddress{{ $address->ua_id }}"
                                                                           class="text-uppercase pr-3 btn btn-default btn-sm">
                                                                            <i class="far fa-edit"></i> Edit
                                                                        </a>
                                                                    </td>
                                                                    <td>
                                                                        <form method="POST" id="useraddress"
                                                                              action="{{ route('userAddressBook') }}"
                                                                              accept-charset="UTF-8"
                                                                              class="form-horizontal"
                                                                              style="display:inline">
                                                                            {{ csrf_field() }}
                                                                            <input type="hidden" name="flag"/>
                                                                            <input type="hidden" name="ua_id"
                                                                                   value="{{ $address->ua_id }}"/>
                                                                            <button type="submit"
                                                                                    class="text-center  btn btn-danger btn-sm"
                                                                                    title="Delete Address"
                                                                                    onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                                                <i class="far fa-trash-alt"></i>
                                                                            </button>
                                                                        </form>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            </p>
                                                            @if($address->ua_defult==1)
                                                                <span class="fred position-absolute Defaultadd text-uppercase">Default</span>
                                                            @endif

                                                        </div>
                                                        <!-- The Modal -->
                                                        <div class="modal" id="updateAddress{{ $address->ua_id }}">
                                                            <div class="modal-dialog">
                                                                <!-- address form -->
                                                                <form method="POST"
                                                                      action="{{ route('userAddAddressBook') }}"
                                                                      accept-charset="UTF-8"
                                                                      class="form-horizontal userAddress"
                                                                      enctype="multipart/form-data">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name="ua_id"
                                                                           value="{{ $address->ua_id }}">
                                                                    <input type="hidden" name="flag">
                                                                    <div class="modal-content">
                                                                        <!-- Modal Header -->
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title">Update Delivery
                                                                                Address</h4>
                                                                            <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                                                                        </div>
                                                                        <!-- Modal body -->
                                                                        <div class="modal-body">

                                                                            <div class="form-group">
                                                                                <input type="text" placeholder="Name"
                                                                                       class="form-control"
                                                                                       name="ua_name"
                                                                                       value="{{ !empty(old('ua_name')) ? old('ua_name') : ((($address) && ($address->ua_name)) ? $address->ua_name : '') }}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <input type="text" placeholder="Phone"
                                                                                       class="form-control"
                                                                                       name="ua_phone"
                                                                                       value="{{ !empty(old('ua_phone')) ? old('ua_phone') : ((($address) && ($address->ua_phone)) ? $address->ua_phone : '') }}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <input type="text" placeholder="Address"
                                                                                       class="form-control"
                                                                                       name="ua_address"
                                                                                       value="{{ !empty(old('ua_address')) ? old('ua_address') : ((($address) && ($address->ua_address)) ? $address->ua_address : '') }}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <input type="text"
                                                                                       placeholder="Landmark"
                                                                                       class="form-control"
                                                                                       name="ua_landmark"
                                                                                       value="{{ !empty(old('ua_landmark')) ? old('ua_landmark') : ((($address) && ($address->ua_landmark)) ? $address->ua_landmark : '') }}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <input type="text" placeholder="City"
                                                                                       class="form-control"
                                                                                       name="ua_city"
                                                                                       value="{{ !empty(old('ua_city')) ? old('ua_city') : ((($address) && ($address->ua_city)) ? $address->ua_city : '') }}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <input type="text" placeholder="State"
                                                                                       class="form-control"
                                                                                       name="ua_state"
                                                                                       value="{{ !empty(old('ua_state')) ? old('ua_state') : ((($address) && ($address->ua_state)) ? $address->ua_state : '') }}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <select name="ua_country"
                                                                                        id="ua_country"
                                                                                        class="form-control">
                                                                                    <option value="">Select Country
                                                                                    </option>
                                                                                    <?php
                                                                                    $countries = getCountries(); ?>
                                                                                    @foreach($countries as $ks=>$s)
                                                                                        <option value="{{ $ks }}" {{ (!empty(old('ua_country')) && old('ua_country')==$ks)  ? 'selected' : ((($address) && ($address->ua_country == $ks)) ? 'selected' : '') }}>
                                                                                            {{ $s }}
                                                                                        </option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <input type="text" placeholder="Email"
                                                                                       class="form-control"
                                                                                       name="ua_email"
                                                                                       value="{{ !empty(old('ua_email')) ? old('ua_email') : ((($address) && ($address->ua_email)) ? $address->ua_email : '') }}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <input type="text" placeholder="Pincode"
                                                                                       class="form-control"
                                                                                       name="ua_pincode"
                                                                                       value="{{ !empty(old('ua_pincode')) ? old('ua_pincode') : ((($address) && ($address->ua_pincode)) ? $address->ua_pincode : '') }}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <input type="checkbox" name="ua_defult"
                                                                                       value="1" {{ ((($address) && ($address->ua_defult==1)) ? 'checked' : '') }}>
                                                                                <label>Default Address</label>
                                                                            </div>

                                                                            <!--/ address form -->
                                                                        </div>
                                                                        <!-- Modal footer -->
                                                                        <div class="modal-footer justify-content-center pt-0 border-0">
                                                                            <button type="submit"
                                                                                    class="btn text-uppercase cbtn">
                                                                                Confirm
                                                                            </button>
                                                                            <button type="button"
                                                                                    class="btn text-uppercase cbtn"
                                                                                    data-dismiss="modal">Cancel
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <!--/ modal for add address -->
                                                    </div>

                                                @endforeach

                                            @endif

                                        </div>
                                    </div>
                                    <!--/ account right body -->
                                </div>
                            </div>
                            <!--/ right account body -->
                        </div>
                    </div>
                </div>
                <!-- order information -->
            </div>
        </div>
        <!--/ cartr checkout page-->
    </section>
    <!--/main Ends-->
    <!-- The Modal -->
    <div class="modal" id="addaddress">
        <div class="modal-dialog">
            <!-- address form -->
            <form method="POST" action="{{ route('userAddAddressBook') }}"
                  accept-charset="UTF-8" class="form-horizontal userAddress" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="ua_id">
                <input type="hidden" name="flag">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Add a New Delivery Address</h4>
                        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">

                        <div class="form-group">
                            <input type="text" placeholder="Name" class="form-control" name="ua_name">
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="Phone" class="form-control" name="ua_phone">
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="Address" class="form-control" name="ua_address">
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="Landmark" class="form-control" name="ua_landmark">
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="City" class="form-control" name="ua_city">
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="State" class="form-control" name="ua_state">
                        </div>
                        <div class="form-group">
                            <select name="ua_country" id="ua_country" class="form-control">
                                <option value="">Select Country</option>
                                <?php
                                $countries = getCountries(); ?>
                                @foreach($countries as $ks=>$s)
                                    <option value="{{ $ks }}">
                                        {{ $s }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="Email" class="form-control" name="ua_email">
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="Pincode" class="form-control" name="ua_pincode">
                        </div>
                        <div class="form-group">
                            <input type="checkbox" name="ua_defult" value="1">
                            <label>Default Address</label>
                        </div>

                        <!--/ address form -->
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer justify-content-center pt-0 border-0">
                        <button type="submit" class="btn text-uppercase cbtn">Confirm</button>
                        <button type="button" class="btn text-uppercase cbtn" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--/ modal for add address -->
@endsection
@section('footerScripts')
    <script src="{{url('frontend/modules/users/js/users.js')}}"></script>
@endsection