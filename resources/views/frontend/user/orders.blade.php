@extends('frontend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')

    <!--main Starts-->
    <section class="main">
        <!-- product overview header -->
        <section class="productview-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <!-- brudcrumb -->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb nobg mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">My Orders</li>
                            </ol>
                        </nav>
                        <!--/ brudcrumb -->
                    </div>
                </div>
            </div>
        </section>
        <!--/ product overview header -->
        <!-- cart checkout pages -->
        <div class="account">
            <div class="container mb-5">
                <!-- payment page-->
                <div class="paymentpage">
                    <!--
                   <div class="form-group mt-2"> <a href="#" class="signbtn btn ">CHANGE PASSWORD</a> </div>
                   -->
                    <div class="tabin mt-4">
                        <div class="row">
                            <!-- left account nav-->
                            <div class="col-lg-3 col-md-3 border-right pr-0">
                                <div class="cartheadrow">
                                    <h5 class="h5 fmedf p-2">MY ACCOUNT</h5>
                                </div>
                                @include('frontend._partials.userprofile-nav')
                            </div>
                            <!--/ left account nav -->
                            <!-- right account body -->
                            <div class="col-lg-9 col-md-9">
                                <div class="accountrt p-3">
                                    <h5 class="h5 fmed border-bottom pb-3">My Orders</h5>
                                    <!-- account right body -->
                                    <div class="accountrtbody">
                                        <!--Horizontal Tab-->
                                        <div class="ordertab">
                                            <div id="parentHorizontalTab">
                                                <ul class="resp-tabs-list hor_1">
                                                    <li>All <span>({{ count($all_orders) }})</span></li>
                                                    <li>Failed <span>({{ count($pending_orders) }}) </span></li>
                                                    <li>Completed <span>({{ count($completed_orders) }})</span></li>
                                                    <li>Cancel <span>({{ count($cancel_orders) }})</span></li>
                                                </ul>
                                                <div class="resp-tabs-container hor_1">
                                                    <!-- all products -->
                                                    <div>
                                                        @if(count($all_orders)>0)
                                                            @foreach($all_orders as $aorder)

                                                                @include('frontend._partials.ordersdiv')

                                                            @endforeach
                                                        @else
                                                            <div class="s-product border-bottom py-3">
                                                                No orders Found
                                                            </div>
                                                        @endif

                                                    </div>
                                                    <!--/ all products -->
                                                    <!-- awaiting payments -->
                                                    <div>
                                                        @if(count($pending_orders)>0)
                                                            @foreach($pending_orders as $aorder)

                                                                @include('frontend._partials.ordersdiv')

                                                            @endforeach
                                                        @else
                                                            <div class="s-product border-bottom py-3">
                                                                No orders Found
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <!--/ awaiting payments -->
                                                    <!-- shipping -->
                                                    <div>
                                                        @if(count($completed_orders)>0)
                                                            @foreach($completed_orders as $aorder)

                                                                @include('frontend._partials.ordersdiv')

                                                            @endforeach
                                                        @else
                                                            <div class="s-product border-bottom py-3">
                                                                No orders Found
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <!--/ shipping -->
                                                    <!-- completed -->
                                                    <div>
                                                        @if(count($cancel_orders)>0)
                                                            @foreach($cancel_orders as $aorder)

                                                                @include('frontend._partials.ordersdiv')

                                                            @endforeach
                                                        @else
                                                            <div class="s-product border-bottom py-3">
                                                                No orders Found
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <!--/ completed -->
                                                </div>
                                            </div>
                                        </div>
                                        <!--/Horizontal Tab-->
                                    </div>
                                    <!--/ account right body -->
                                </div>
                            </div>
                            <!--/ right account body -->
                        </div>
                    </div>
                </div>
                <!-- order information -->
            </div>
        </div>
        <!--/ cartr checkout page-->
    </section>
    <!--/main Ends-->

@endsection
@section('footerScripts')

@endsection