@extends('frontend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')

    <!--main Starts-->
    <section class="main">
        <!-- product overview header -->
        <section class="productview-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <!-- brudcrumb -->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb nobg mb-0">
                                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Cancel / Refund</li>
                            </ol>
                        </nav>
                        <!--/ brudcrumb -->
                    </div>
                </div>
            </div>
        </section>
        <!--/ product overview header -->
        <!-- cart checkout pages -->
        <div class="account">
            <div class="container mb-5">
                <!-- payment page-->
                <div class="paymentpage">
                    <!--
                   <div class="form-group mt-2"> <a href="#" class="signbtn btn ">CHANGE PASSWORD</a> </div>
                   -->
                    <div class="tabin mt-4">
                        <div class="row">
                            <!-- left account nav-->
                            <div class="col-lg-3 border-right pr-0">
                                <div class="cartheadrow">
                                    <h5 class="h5 fmedf p-2">MY ACCOUNT</h5>
                                </div>
                                @include('frontend._partials.userprofile-nav')
                            </div>
                            <!--/ left account nav -->
                            <!-- right account body -->
                            <div class="col-lg-9">
                                <div class="accountrt p-3">
                                    <h5 class="h5 fmed border-bottom pb-3">Cancel/Refund</h5>
                                    <!-- account right body -->
                                    <div class="accountrtbody">
                                        <!--Horizontal Tab-->
                                        <div class="ordertab">
                                            <div id="parentHorizontalTab">
                                                <ul class="resp-tabs-list hor_1">
                                                    <li>Cancel (1)</li>
                                                    <li>Refund <span>(0) </span></li>
                                                </ul>
                                                <div class="resp-tabs-container hor_1">
                                                    <!-- Cancel products -->
                                                    <div>
                                                        <div class="s-product border-bottom py-3">
                                                            <h4 class="ordtitle fmed">Order Cancel</h4>
                                                            <ul class="orddetlist py-4">
                                                                <li> <span>Name:</span> <span class="fgray pl-2">Santosh Chary</span> </li>
                                                                <li> <span>Order number:</span> <span class="fgray pl-2">18100614451880850561</span> </li>
                                                                <li> <span>Order Date &amp; Time:</span> <span class="fgray pl-2">06 Oct 2018 14:45:19</span> </li>
                                                            </ul>
                                                            <div class="row py-2 ordrow">
                                                                <div class="col-lg-2">
                                                                    <figure class="cartimg">
                                                                        <a href="#"><img src="img/data/productreviewimg01.png"> </a>
                                                                    </figure>
                                                                </div>
                                                                <div class="col-lg-6 align-self-center">
                                                                    <h5 class="fmed h6">AZC03D Intelligent Battery Digicharger Kit </h5>
                                                                    <p class="fgray">Innovative Joyetech NCFilmTM heater along with the CUBIS Max tank. Being a coil-less</p>
                                                                    <div class="paybtns pt-3"> <a href="#" class="cbtn btn text-uppercase fgray">PAYNOW</a> <a href="account-myorder-detail.php" class="cbtn btn text-uppercase fgray">Order Details</a></div>
                                                                </div>
                                                                <div class="col-lg-4 align-self-right text-center">
                                                                    <h1 class="price h1"><i class="fas fa-rupee-sign"></i> 498</h1>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/ Cancel products -->
                                                    <!-- Refund Product -->
                                                    <div>
                                                        <div class="s-product border-bottom py-3">
                                                            <p class="text-center">No Refund Products Available</p>
                                                        </div>
                                                    </div>
                                                    <!--/ Refund Products -->
                                                </div>
                                            </div>
                                        </div>
                                        <!--/Horizontal Tab-->
                                    </div>
                                    <!--/ account right body -->
                                </div>
                            </div>
                            <!--/ right account body -->
                        </div>
                    </div>
                </div>
                <!-- order information -->
            </div>
        </div>
        <!--/ cartr checkout page-->
    </section>
    <!--/main Ends-->

@endsection
@section('footerScripts')

@endsection