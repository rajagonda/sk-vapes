@extends('frontend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')

    <!--main Starts-->
    <section class="main">
        <!-- product overview header -->
        <section class="productview-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <h1 class="mb-0 pt-1 text-uppercase">Payment</h1>
                    </div>
                </div>
            </div>
        </section>
        <!--/ product overview header -->
        <!-- cart checkout pages -->
        <div class="cartprocess">
            <div class="container mb-5">
                <!-- payment page-->
                <div class="paymentpage">
                    <div class="tabin mt-4">
                        <div class="row border-bottom py-5 mx-0">
                            {{--                            @if (Session::has('flash_message'))--}}
                            <div class="col-lg-12 text-center"><img src="/frontend/img/checkicon.png" title="" alt="">
                                {{--                                <h5 class="fmed h5 pt-2">{{ Session::get('flash_message' ) }}</h5>--}}
                                <h5 class="fmed h5 pt-2">Your Order has been successfully submitted !</h5>
                            </div>
                            {{--@endif--}}
                        </div>
                        <div class="row py-3 mx-0">
                            <div class="col-lg-12">
                                <h5 class="h5 fmedf pl-3">Order Details</h5>
                            </div>
                            <div class="col-lg-12">
                                <table class="table nobrd mb-0 w-50">
                                    <tr>
                                        <td>Order number</td>
                                        <td>:</td>
                                        <td><span class="price">{{ $latest_order->order_reference_number }}</span></td>
                                    </tr>
                                </table>
                                <table class="table nobrd mb-0 w-100">
                                    <tr>
                                        <td class="w-75">
                                            <p class="f16">{{ $latest_order_address['ua_name'] . $latest_order_address['ua_phone'] }}
                                                ,<br/>
                                                {{ $latest_order_address['ua_address'] .$latest_order_address['ua_city'] . $latest_order_address['ua_state'] . $latest_order_address['ua_country'] }}
                                            </p>
                                        </td>
                                        <td align="center">
                                            <table>
                                                <tr>
                                                    <td><span class="fmed">Total</span></td>
                                                    <td>:</td>
                                                    <td><span class="price">
                                                            <h3 class="h3 d-inline fbold"><i
                                                                        class="fas fa-rupee-sign"></i>{{ $latest_order->order_total_price }}</h3>
                                                        </span></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- order information -->
            </div>
        </div>
        <!--/ cartr checkout page-->
    </section>
    <!--/main Ends-->

@endsection
@section('footerScripts')
@endsection