@extends('frontend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')

    <!--main Starts-->
    <section class="main">
        <!-- product overview header -->
        <section class="productview-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <!-- brudcrumb -->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb nobg mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Change Password</li>
                            </ol>
                        </nav>
                        <!--/ brudcrumb -->
                    </div>
                </div>
            </div>
        </section>
        <!--/ product overview header -->
        <!-- cart checkout pages -->
        <div class="account">
            <div class="container mb-5">
                <!-- payment page-->
                <div class="paymentpage">
                    <div class="tabin mt-4">
                        <div class="row">
                            <!-- left account nav-->
                            <div class="col-lg-3 col-md-3 border-right pr-0">
                                <div class="cartheadrow">
                                    <h5 class="h5 fmedf p-2">MY ACCOUNT</h5>
                                </div>
                                @include('frontend._partials.userprofile-nav')
                            </div>
                            <!--/ left account nav -->
                            <!-- right account body -->
                            <div class="col-lg-9 col-md-9">
                                @if (Session::has('flash_message'))
                                    <br/>
                                    <div class="alert alert-success alert-dismissable">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>{{ Session::get('flash_message' ) }}</strong>
                                    </div>
                                @endif
                                @if (Session::has('flash_message_error'))
                                    <br/>
                                    <div class="alert alert-danger alert-dismissable">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>{{ Session::get('flash_message_error' ) }}</strong>
                                    </div>
                                @endif
                                <div class="accountrt p-3">
                                    <h5 class="h5 fmed border-bottom pb-3">Change Password</h5>
                                    <!-- account right body -->
                                    <div class="accountrtbody">
                                        <form method="POST" id="changePassword"
                                              action="{{ route('userChangePassword') }}"
                                              accept-charset="UTF-8" class="form-horizontal">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="col-lg-6">

                                                    <div class="row pt-5">
                                                        <div class="col-lg-12">
                                                            <div class="form-group position-relative">
                                                                <label class="py-2">Current Password</label>
                                                                <input type="password" id="password" name="password"
                                                                       placeholder="Enter Password"
                                                                       value="{{ !empty(old('password')) ? old('password') : '' }}"
                                                                       class="form-control">
                                                                @if ($errors->has('password'))
                                                                    <span class="text-danger help-block">{{ $errors->first('password') }}</span>
                                                                @endif
                                                                <span class="showpw">
                                                                    <i
                                                                            class="fas fa-eye-slash"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <div class="form-group position-relative">
                                                                <label class="py-2">New Password</label>
                                                                <input type="password" id="new_password"
                                                                       name="new_password"
                                                                       value="{{ !empty(old('new_password')) ? old('new_password') : '' }}"
                                                                       placeholder="Enter New password"
                                                                       class="form-control">
                                                                @if ($errors->has('new_password'))
                                                                    <span class="text-danger help-block">{{ $errors->first('new_password') }}</span>
                                                                @endif
                                                                <span class="showpw"><i  class="fas fa-eye-slash"></i></span></div>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <div class="form-group position-relative">
                                                                <label class="py-2">Confirm New Password</label>
                                                                <input type="password" class="form-control"
                                                                       id="confirm_password"
                                                                       name="confirm_password"
                                                                       placeholder="Confirm Password"
                                                                       value="{{ !empty(old('confirm_password')) ? old('confirm_password') : '' }}">
                                                                @if ($errors->has('confirm_password'))
                                                                    <span class="text-danger help-block">{{ $errors->first('confirm_password') }}</span>
                                                                @endif
                                                                <span class="showpw"><i
                                                                            class="fas fa-eye-slash"></i></span></div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="py-2 pl-5 passwordmust">
                                                        <h6 class="h6 fmed">Your new password must</h6>
                                                        <ul class="list-group custom-popover pt-3">
                                                            <li class="list-group-item">
                                                                <span>At least 6 Characters</span>
                                                            </li>
                                                            <li class="list-group-item"><span>At least 1 Upper case letter (A - Z)</span>
                                                            </li>
                                                            <li class="list-group-item"><span>At least 1 Lower case Letter (a - z)</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <span>At least 1 Number (0 - 9)</span></li>
                                                            <li class="list-group-item">
                                                                <span>One specl character (For example: !, $, #, or %)</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row py-3">
                                                <div class="col-lg-4 mx-auto">
                                                    <div class="form-group mt-2">
                                                        <button type="submit" class="signbtn btn ">CHANGE PASSWORD
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!--/ account right body -->
                                </div>
                            </div>
                            <!--/ right account body -->
                        </div>
                    </div>
                </div>
                <!-- order information -->
            </div>
        </div>
        <!--/ cartr checkout page-->
    </section>
    <!--/main Ends-->

@endsection
@section('footerScripts')
    <script src="{{url('frontend/modules/users/js/changePassword.js')}}"></script>
@endsection