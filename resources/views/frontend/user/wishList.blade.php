@extends('frontend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')

    <!--main Starts-->
    <section class="main">
        <!-- product overview header -->
        <section class="productview-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <!-- brudcrumb -->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb nobg mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">My Wish List</li>
                            </ol>
                        </nav>
                        <!--/ brudcrumb -->
                    </div>
                </div>
            </div>
        </section>
        <!--/ product overview header -->
        <!-- cart checkout pages -->
        <div class="account">
            <div class="container mb-5">
                <!-- payment page-->
                <div class="paymentpage">
                    <!--
                   <div class="form-group mt-2"> <a href="#" class="signbtn btn ">CHANGE PASSWORD</a> </div>
                   -->
                    <div class="tabin mt-4">
                        <div class="row">
                            <!-- left account nav-->
                            <div class="col-lg-3 border-right pr-0">
                                <div class="cartheadrow">
                                    <h5 class="h5 fmedf p-2">MY ACCOUNT</h5>
                                </div>
                                @include('frontend._partials.userprofile-nav')
                            </div>
                            <!--/ left account nav -->
                            <!-- right account body -->
                            <div class="col-lg-9">
                                @if (Session::has('flash_message'))
                                    <br/>
                                    <div class="alert alert-success alert-dismissable">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>{{ Session::get('flash_message' ) }}</strong>
                                    </div>
                                @endif
                                <div class="accountrt p-3">
                                    <h5 class="h5 fmed border-bottom pb-3">My Wish List</h5>
                                    <!-- account right body -->
                                    <div class="accountrtbody">

                                    @if(count($wishlistProducts)>0)
                                        @foreach($wishlistProducts as $list)
                                            <!--row -->
                                                <div class="row py-3 ordrow border-bottom dashedbrd">
                                                    <div class="col-lg-2">
                                                        <figure class="cartimg">
                                                            <a href="{{ route('productDetails',['product_alias'=>$list->getProduct->product_alias]) }}"><img
                                                                        src="/frontend/img/data/productreviewimg01.png">
                                                            </a>
                                                        </figure>
                                                    </div>
                                                    <div class="col-lg-6 align-self-center">
                                                        <h5 class="fmed h6">{{ $list->getProduct->product_name }}</h5>
                                                        <p class="fgray">{!! str_limit($list->getProduct->product_description, 150)  !!}</p>
                                                        <div class="paybtns pt-3">

                                                            <form method="POST" action="{{ route('buyNow') }}"
                                                                  accept-charset="UTF-8" class="">
                                                                {{ csrf_field() }}
                                                                <input type="hidden" name="id" value="{{ $list->getProduct->product_id }}">
                                                                <input type="hidden" name="qty" value="1">
                                                                <input type="submit" value="BUY NOW" class="cbtn btn text-uppercase fgray">
                                                            </form>

                                                            <form method="POST"
                                                                  action="{{ route('wishList') }}"
                                                                  accept-charset="UTF-8" class="form-horizontal"
                                                                  style="display:inline">
                                                                {{ csrf_field() }}
                                                                <input type="hidden" name="wishlist_id"
                                                                       value="{{ $list->wishlist_id }}"/>
                                                                <button type="submit"
                                                                        class="cbtn btn text-uppercase fgray"
                                                                        title="Delete Product"
                                                                        onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                                    Remove
                                                                </button>
                                                            </form>

                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 align-self-right text-center">
                                                        <h1 class="price h1"><i class="fas fa-rupee-sign"></i> {{ $list->getProduct->product_price }}</h1>
                                                    </div>
                                                </div>
                                                <!--/ row -->
                                            @endforeach
                                        @else
                                            <div class="row py-3 ordrow border-bottom dashedbrd">
                                                No Products Found
                                            </div>
                                        @endif

                                    </div>
                                    <!--/ account right body -->
                                </div>
                            </div>
                            <!--/ right account body -->
                        </div>
                    </div>
                </div>
                <!-- order information -->
            </div>
        </div>
        <!--/ cartr checkout page-->
    </section>
    <!--/main Ends-->
@endsection
@section('footerScripts')

@endsection