@extends('frontend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')

    <!--main Starts-->
    <section class="main">
        <!-- product overview header -->
        <section class="productview-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <!-- brudcrumb -->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb nobg mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('orders') }}">Orders</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Order Details</li>
                            </ol>
                        </nav>
                        <!--/ brudcrumb -->
                    </div>
                </div>
            </div>
        </section>
        <!--/ product overview header -->
        <!-- cart checkout pages -->
        <div class="account">
            <div class="container mb-5">
                <!-- payment page-->
                <div class="paymentpage">
                    <!--
                   <div class="form-group mt-2"> <a href="#" class="signbtn btn ">CHANGE PASSWORD</a> </div>
                   -->
                    <div class="tabin mt-4">
                        <div class="row">
                            <!-- left account nav-->
                            <div class="col-lg-3 border-right pr-0">
                                <div class="cartheadrow">
                                    <h5 class="h5 fmedf p-2">MY ACCOUNT</h5>
                                </div>
                                @include('frontend._partials.userprofile-nav')
                            </div>
                            <!--/ left account nav -->
                            <!-- right account body -->
                            <div class="col-lg-9">
                                <div class="accountrt p-3">
                                    <h5 class="h5 fmed border-bottom pb-3">My Orders</h5>

                                @php
                                    $address=unserialize($orderDetails->order_delivery_address);
                                @endphp

                                    <!-- account right body -->
                                    <div class="accountrtbody">
                                        <h4 class="ordtitle fmed pb-2">Order Number
                                            : {{ $orderDetails->order_reference_number }}</h4>
                                        <h4 class="ordtitle fmed fgreen py-3">{{ $orderDetails->order_status }}</h4>

                                        @foreach($orderDetails->orderItems as $items)
                                            <div class="row py-2 ordrow">
                                                <div class="col-lg-2">
                                                    <figure class="cartimg">
                                                        <a href="#">
                                                            <img src="/uploads/products/thumbs/{{ $items->getProduct->productImages[0]->pi_image_name }}">
                                                        </a>
                                                    </figure>
                                                </div>
                                                <div class="col-lg-6 align-self-center">
                                                    <h5 class="fmed h6">{{ $items->getProduct->product_name }}</h5>
                                                    <p class="fgray">{!! str_limit($items->getProduct->product_description, 150) !!}</p>
                                                    <p class="fgrey">
                                                        <?php echo $items->oitem_product_spl ? '<b>Color:</b> '.strtoupper($items->oitem_product_spl):''; ?>
                                                        <?php echo $items->oitem_product_size ? '<b>Color:</b> '.strtoupper($items->oitem_product_size):''; ?>
                                                    </p>
                                                </div>
                                                <div class="col-lg-4 align-self-right text-center">
                                                    <h6 class="price h6"><i class="fas fa-rupee-sign"></i> {{ $items->oitem_qty }} X {{ $items->oitem_product_price }}</h6>
                                                </div>
                                            </div>

                                    @endforeach
                                    {{--<!-- order status -->--}}
                                        {{--<div class="ord-status d-flex border-bottom dashedbrd">--}}
                                            {{--<div class="barcol"><span class="fgreen sttext">Ordered</span>--}}
                                                {{--<div class="barstrip"><a href="#" data-target="toggle"--}}
                                                                         {{--data-toggle="tooltip"--}}
                                                                         {{--title="Wed, 7th Mar 12:25 pm"--}}
                                                                         {{--data-placement="bottom"><i--}}
                                                                {{--class="fas fa-circle"></i></a></div>--}}
                                            {{--</div>--}}
                                            {{--<div class="barcol"><span class="fgreen sttext">Packed</span>--}}
                                                {{--<div class="barstrip"><a href="#" data-target="toggle"--}}
                                                                         {{--data-toggle="tooltip"--}}
                                                                         {{--title="Wed, 7th Mar 12:25 pm"--}}
                                                                         {{--data-placement="bottom"><i--}}
                                                                {{--class="fas fa-circle"></i></a></div>--}}
                                            {{--</div>--}}
                                            {{--<div class="barcol"><span class="fgreen sttext">Shipped</span> <span--}}
                                                        {{--class="fgreen sttext deliveredst">Delivered</span>--}}
                                                {{--<div class="barstrip"><a href="#" data-target="toggle"--}}
                                                                         {{--data-toggle="tooltip"--}}
                                                                         {{--title="Wed, 7th Mar 12:25 pm"--}}
                                                                         {{--data-placement="bottom"><i--}}
                                                                {{--class="fas fa-circle"></i></a> <a--}}
                                                            {{--href="#" class="delivered"--}}
                                                            {{--data-target="toggle" data-toggle="tooltip"--}}
                                                            {{--title="Wed, 7th Mar 12:25 pm" data-placement="bottom"><i--}}
                                                                {{--class="fas fa-circle"></i></a></div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        <!--/ order status -->

                                        <div class="deliverydet py-2 border-bottom dashedbrd">
                                            <h4 class="ordtitle fmed py-3">Order Information</h4>
                                            <table width="100%" class="table-orderdetails">
                                                <tr>
                                                    <td width="30%">Transaction Number</td>
                                                    <td><span class="fgray">{{ isset($orderDetails->order_transaction_id) ? $orderDetails->order_transaction_id : '--' }}</span></td>
                                                </tr>

                                                <tr>
                                                    <td width="30%">Status</td>
                                                    <td><span class="fgray">{{ $orderDetails->order_status }}</span></td>
                                                </tr>
                                            </table>
                                        </div>

                                       <div class="deliverydet py-2 border-bottom dashedbrd">
                                            <h4 class="ordtitle fmed py-3">Delivery Address</h4>
                                            <table width="100%" class="table-orderdetails">
                                                <tr>
                                                    <td width="30%">Name</td>
                                                    <td><span class="fgray">{{ $address['ua_name'] }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td width="30%">Address</td>
                                                    <td><span class="fgray">{{ $address['ua_address'] }}
                                                            ,{{ $address['ua_city'] }},{{ $address['ua_state'] }}
                                                            ,{{ $address['ua_country'] }}</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="30%">Phone</td>
                                                    <td><span class="fgray">{{ $address['ua_phone'] }}</span></td>
                                                </tr>
                                            </table>
                                        </div>

                                        <div class="row py-3">
                                            <div class="col-lg-3 text-right ml-auto text-right">
                                                <table class="finalprice" width="100%;">
                                                    <tr>
                                                        <td>Total amount</td>
                                                        <td>:</td>
                                                        <td><span><i class="fas fa-rupee-sign"></i>{{ $orderDetails->order_total_price }}</span></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <!--/ delivelry details -->
                                    </div>
                                    <!--/ account right body -->
                                </div>
                            </div>
                            <!--/ right account body -->
                        </div>
                    </div>
                </div>
                <!-- order information -->
            </div>
        </div>
        <!--/ cartr checkout page-->
    </section>
    <!--/main Ends-->

@endsection
@section('footerScripts')

@endsection