@extends('frontend.layout')
@section('title', $title)

@section('headerStyles')
    <style>
        label.addbox {
            cursor: pointer;

        }

        .userAddressid {
            opacity: 0;
        }

        .selectedAddress {
            /*border: 2px solid #000 !important;*/

            border: solid 2px black !important;
            color: black;
            padding: 24px;
            position: relative;
        }

        .selectedAddress::before,
        .selectedAddress::after {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            width: 54px;
            height: 54px;
            background-image: url(http://www.clker.com/cliparts/p/e/X/9/d/4/check-mark-white-md.png),
            linear-gradient(135deg, green 50%, transparent 50%);
            background-size: 70% auto, cover;
            background-position: auto, left top;
            background-repeat: no-repeat;
        }
    </style>
@endsection

@section('content')
    <!--main Starts-->
    <section class="main">
        <!-- product detail header -->
        <section class="productview-header"></section>
        <!--/ product detail header -->
        <!-- cart checkout pages -->
        <div class="cartprocess">
            <div id="demo">
                <div class="step-app">
                    <ul class="step-steps container">
                        <li>
                            <a href="{{ route('cartDetails') }}" class="text-uppercase text-center">1 Shopping Cart
                                <i class="fas fa-chevron-right float-right"></i></a>
                        </li>
                        <li class="active">
                            <a href="#" class="text-uppercase text-center">2. Order
                                Confirmation <i class="fas fa-chevron-right float-right"></i></a>
                        </li>
                        <li><a href="#" class="text-uppercase text-center stepanchor">3.
                                Complete Payment </a></li>
                    </ul>
                    <div class="step-content container mb-5">
                        @if (Session::has('flash_message'))
                            <br/>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>{{ Session::get('flash_message' ) }}</strong>
                            </div>
                        @endif
                        <div class="tabin p-3">
                            <h5 class="h5 fmedf">Recipient Information</h5>
                            <div class="row pt-3">

                                <div class="col-lg-4 text-center ">
                                    <div class="addbox dashed rounded  addplus d-flex  align-items-center">
                                        <a class="icadd mx-auto" href="#" data-toggle="modal"
                                           data-target="#addaddress"><span class="acaddin"><i
                                                        class="fas fa-plus"></i></span>
                                            <span class="d-block">Add Address</span>
                                        </a>
                                    </div>
                                </div>

                                @if(count($userAddresses)>0)
                                    @foreach($userAddresses as $address)

                                        <div class="col-lg-4">
                                            <label class="addbox position-relative border rounded  align-items-center">
                                                <input type="radio" name="userAddressid" class="userAddressid"
                                                       value="{{ $address->ua_id }}"
                                                       @if($address->ua_defult==1) checked="checked" @endif/>
                                                <h4 class="h4">{{ $address->ua_name }}</h4>
                                                <h5 class="pb-2">{{ $address->ua_phone }}</h5>
                                                <p class="fgray">{{ $address->ua_address }}
                                                    ,{{ $address->ua_city }},{{ $address->ua_state }}
                                                    ,{{ $address->getCountry->country_name }}</p>
                                                <div class="pt-3 editdel">
                                                    <a href="#" data-toggle="modal"
                                                       data-target="#updateAddress{{ $address->ua_id }}"
                                                       class="text-uppercase pr-3">
                                                        <i class="far fa-edit"></i> Edit
                                                    </a>

                                                    <form method="POST" action="{{ route('userAddressBook') }}"
                                                          accept-charset="UTF-8" class="form-horizontal"
                                                          style="display:inline">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="ua_id"
                                                               value="{{ $address->ua_id }}"/>
                                                        <input type="hidden" name="flag" value="1">
                                                        <button type="submit"
                                                                class="text-uppercase btn btn-default btn-sm text-center"
                                                                title="Delete Address"
                                                                onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                            <i class="far fa-trash-alt"></i>
                                                        </button>
                                                    </form>

                                                    @if($address->ua_defult==1)
                                                        {{--<span class="fred position-absolute Defaultadd text-uppercase">Default</span>--}}
                                                    @else
                                                        <a href="#"
                                                           class="text-uppercase pl-2 setUserDefaultAddress"
                                                           id="{{ $address->ua_id }}"><i
                                                                    class="fas fa-map-marker-alt"></i> Set as
                                                            Default</a>
                                                    @endif
                                                </div>

                                            </label>
                                            <!-- The Modal -->
                                            <div class="modal" id="updateAddress{{ $address->ua_id }}">
                                                <div class="modal-dialog">
                                                    <!-- address form -->
                                                    <form method="POST"
                                                          action="{{ route('userAddAddressBook') }}"
                                                          accept-charset="UTF-8" class="form-horizontal userAddress"
                                                          enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="ua_id"
                                                               value="{{ $address->ua_id }}">
                                                        <input type="hidden" name="flag" value="1">
                                                        <div class="modal-content">
                                                            <!-- Modal Header -->
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">Update Delivery
                                                                    Address</h4>
                                                                <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                                                            </div>
                                                            <!-- Modal body -->
                                                            <div class="modal-body">
                                                                <div class="form-group">
                                                                    <input type="text" placeholder="Name"
                                                                           class="form-control"
                                                                           name="ua_name"
                                                                           value="{{ !empty(old('ua_name')) ? old('ua_name') : ((($address) && ($address->ua_name)) ? $address->ua_name : '') }}">
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="text" placeholder="Phone"
                                                                           class="form-control"
                                                                           name="ua_phone"
                                                                           value="{{ !empty(old('ua_phone')) ? old('ua_phone') : ((($address) && ($address->ua_phone)) ? $address->ua_phone : '') }}">
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="text" placeholder="Address"
                                                                           class="form-control"
                                                                           name="ua_address"
                                                                           value="{{ !empty(old('ua_address')) ? old('ua_address') : ((($address) && ($address->ua_address)) ? $address->ua_address : '') }}">
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="text"
                                                                           placeholder="Landmark"
                                                                           class="form-control"
                                                                           name="ua_landmark"
                                                                           value="{{ !empty(old('ua_landmark')) ? old('ua_landmark') : ((($address) && ($address->ua_landmark)) ? $address->ua_landmark : '') }}">
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="text" placeholder="City"
                                                                           class="form-control"
                                                                           name="ua_city"
                                                                           value="{{ !empty(old('ua_city')) ? old('ua_city') : ((($address) && ($address->ua_city)) ? $address->ua_city : '') }}">
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="text" placeholder="State"
                                                                           class="form-control"
                                                                           name="ua_state"
                                                                           value="{{ !empty(old('ua_state')) ? old('ua_state') : ((($address) && ($address->ua_state)) ? $address->ua_state : '') }}">
                                                                </div>
                                                                <div class="form-group">
                                                                    <select name="ua_country"
                                                                            id="ua_country"
                                                                            class="form-control">
                                                                        <option value="">Select Country
                                                                        </option>
                                                                        <?php
                                                                        $countries = getCountries(); ?>
                                                                        @foreach($countries as $ks=>$s)
                                                                            <option value="{{ $ks }}" {{ (!empty(old('ua_country')) && old('ua_country')==$ks)  ? 'selected' : ((($address) && ($address->ua_country == $ks)) ? 'selected' : '') }}>
                                                                                {{ $s }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="text" placeholder="Email"
                                                                           class="form-control"
                                                                           name="ua_email"
                                                                           value="{{ !empty(old('ua_email')) ? old('ua_email') : ((($address) && ($address->ua_email)) ? $address->ua_email : '') }}">
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="text" placeholder="Pincode"
                                                                           class="form-control"
                                                                           name="ua_pincode"
                                                                           value="{{ !empty(old('ua_pincode')) ? old('ua_pincode') : ((($address) && ($address->ua_pincode)) ? $address->ua_pincode : '') }}">
                                                                </div>
                                                            {{--<div class="form-group">--}}
                                                            {{--<input type="checkbox" name="ua_defult"--}}
                                                            {{--value="1" {{ ((($address) && ($address->ua_defult==1)) ? 'checked' : '') }}>--}}
                                                            {{--<label>Default Address</label>--}}
                                                            {{--</div>--}}

                                                            <!--/ address form -->
                                                            </div>
                                                            <!-- Modal footer -->
                                                            <div class="modal-footer justify-content-center pt-0 border-0">
                                                                <button type="submit"
                                                                        class="btn text-uppercase cbtn">
                                                                    Confirm
                                                                </button>
                                                                <button type="button"
                                                                        class="btn text-uppercase cbtn"
                                                                        data-dismiss="modal">Cancel
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!--/ modal for add address -->
                                        </div>

                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="tabin mt-4 p-3">
                            <h5 class="h5 fmedf">Products Details</h5>

                            <!--table -->
                            <table id="table" class="resptable table shoppingcarttable ordconfirm">
                                <thead style="display: none;">
                                <th class="image">Images</th>
                                <th class="proname">Product Name</th>
                                <th>Price</th>
                                <th align="center">Quantity</th>
                                <th>Total</th>
                                </thead>
                                <tbody>
                                <?php
                                if (Cart::content()->count() > 0){
                                foreach(Cart::content() as $row) :

                                ?>
                                @php

                                    if($row->options->image){
                                    $ptname=$row->options->image;
                                    }else{
                                     if (!empty(getProductImage($row->id))){
                                      $ptname=getProductImage($row->id)->pi_image_name;
                                     }else{
                                     $ptname='';
                                     }
                                    }

                                @endphp

                                <tr>
                                    <td class="ordconfirmimg">
                                        <figure class="cartimg">
                                            <a href="#"><img
                                                        src="/uploads/products/thumbs/{{ $ptname }}"></a>
                                        </figure>
                                    </td>
                                    <td class="pronametd">
                                        <h6 class="fmed h6"><a
                                                    href="{{ route('productDetails',['product_alias'=>$row->options->alias]) }}"><?php echo $row->name; ?></a>
                                        </h6>
                                        <p class="fgrey"><?php echo($row->options->has('description') ? str_limit($row->options->description, 150) : ''); ?></p>
                                        <p class="fgrey">
                                            <?php echo $row->options->color ? '<b>Color:</b> '.strtoupper($row->options->color):''; ?>

                                            <?php echo ($row->options->size) ? '<b>Size/Capacity:</b> '.strtoupper($row->options->size):''; ?>
                                        </p>
                                    </td>
                                    <td>
                                        <span class="price"><i class="fas fa-rupee-sign"></i> <?php echo $row->price; ?></span>
                                    </td>
                                    <td>
                                        <span class="price"><?php echo $row->qty; ?></span>
                                    </td>
                                    <td>
                                        <span class="price fred"><i
                                                    class="fas fa-rupee-sign"></i> <?php echo $row->subtotal; ?></span>
                                    </td>
                                </tr>
                                <?php
                                endforeach;
                                }else { ?>
                                <tr>
                                    <td colspan="5">
                                        No data found
                                    </td>
                                </tr>
                                <?php  }
                                ?>
                                </tbody>
                            </table>
                            <!--/ table -->
                            <div class="row mx-0 py-3 border-top border-bottom">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4"><span class="fmed">Delivery Service</span></div>
                                        <div class="col-lg-8"><span class="fmed">Home Delivery</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mx-0 py-3 border-bottom">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4 align-self-center"><span
                                                    class="fmed">Discount Coupon</span></div>
                                        <div class="col-lg-8">
                                            <div class="deliverto">
                                                <div class="inputdelivery py-2">
                                                    <input class="float-left" type="text"
                                                           placeholder="Please enter your Coupon"/>
                                                    <input class="float-left" type="submit" value="Apply"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mx-0 py-3 border-bottom">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4"><span class="fmed">Total Price</span></div>
                                        <div class="col-lg-6">
                                            <table class="table nobrd mb-0">
                                                <tr>
                                                    <td>Sub Total</td>
                                                    <td>:</td>
                                                    <td><span class="price"><i
                                                                    class="fas fa-rupee-sign"></i> <?php echo Cart::subtotal(); ?></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Shipping
                                                        <br></td>
                                                    <td>:</td>
                                                    <td><span class="price"><i class="fas fa-rupee-sign"></i> -0 </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Discount</td>
                                                    <td>:</td>
                                                    <td><span class="price"><i class="fas fa-rupee-sign"></i> -0</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><span class="price">
                                                                <h4 class="h4"> Total</h4>
                                                            </span></td>
                                                    <td>:</td>
                                                    <td><span class="price">
                                                                <h4 class="h4"><i class="fas fa-rupee-sign"
                                                                                  style="font-size:20px;"></i> <?php echo Cart::subtotal(); ?></h4>
                                                            </span></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row mx-0 py-3">--}}
                            {{--<div class="col-lg-6">--}}
                            {{--<div class="row">--}}
                            {{--<div class="col-lg-4"><span class="fmed">Delivery Address</span></div>--}}
                            {{--<div class="col-lg-8">--}}
                            {{--<p>P Santosh Chary 9849367037</p>--}}
                            {{--<p> 2-3-64/10/A/23, Jaiswal Garden, Amberpet, Little Flower High School 500013, HYDERABAD TELANGANA.</p>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            <div class="row mx-0 mt-3">
                                <form method="POST" action="{{ route('saveOredrs') }}" accept-charset="UTF-8"
                                      class="form-horizontal selectionOfAddress" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <input type="hidden" name="address_id" class="assignUserAddress">
                                    <input type="submit" class="signbtn btn" value="PROCEED"/>
                                </form>
                                <form method="POST" action="{{ route('makeDefaultAddress') }}" accept-charset="UTF-8"
                                      class="form-horizontal" id="makeDefaultAddress" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <input type="hidden" name="ua_id" class="makeDefaultAddress">
                                    <input type="hidden" name="ua_defult" value="1">
                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!--/ cartr checkout page-->
    </section>
    <!--/main Ends-->
    <!-- The Modal -->
    <div class="modal" id="addaddress">
        <div class="modal-dialog">
            <!-- address form -->
            <form method="POST" action="{{ route('userAddAddressBook') }}"
                  accept-charset="UTF-8" class="form-horizontal userAddress" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="flag" value="1">
                <input type="hidden" name="ua_id">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Add a New Delivery Address</h4>
                        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">

                        <div class="form-group">
                            <input type="text" placeholder="Name" class="form-control" name="ua_name">
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="Phone" class="form-control" name="ua_phone">
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="Address" class="form-control" name="ua_address">
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="Landmark" class="form-control" name="ua_landmark">
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="City" class="form-control" name="ua_city">
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="State" class="form-control" name="ua_state">
                        </div>
                        <div class="form-group">
                            <select name="ua_country" id="ua_country" class="form-control">
                                <option value="">Select Country</option>
                                <?php
                                $countries = getCountries(); ?>
                                @foreach($countries as $ks=>$s)
                                    <option value="{{ $ks }}">
                                        {{ $s }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="Email" class="form-control" name="ua_email">
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="Pincode" class="form-control" name="ua_pincode">
                        </div>
                        <div class="form-group">
                            <input type="checkbox" name="ua_defult" value="1">
                            <label>Default Address</label>
                        </div>

                        <!--/ address form -->
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer justify-content-center pt-0 border-0">
                        <button type="submit" class="btn text-uppercase cbtn">Confirm</button>
                        <button type="button" class="btn text-uppercase cbtn" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--/ modal for add address -->

@endsection
@section('footerScripts')

    <script src="{{url('frontend/modules/users/js/users.js')}}"></script>
    <script type="text/javascript">
        $(function () {

            @if(count($userAddresses) == 0)
            $('#addaddress').modal('show');
            @endif
            $('.setUserDefaultAddress').on('click', function () {
                $('.makeDefaultAddress').val($(this).attr('id'));
                $('#makeDefaultAddress').submit();
            });

            $('.selectionOfAddress').validate({
                ignore: [],
                errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
                // errorElement: 'div',
                // errorPlacement: function (error, e) {
                //     e.parents('.form-group').append(error);
                // },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.text-danger').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.text-danger').remove();
                },
                rules: {
                    address_id: {
                        required: true
                    }
                },
                messages: {
                    address_id: {
                        required: 'Select address to process the order'
                    }
                },
            });
            $("input[type=\"radio\"]").parents('.addbox').removeClass("selectedAddress");
            $("input[type=\"radio\"]:checked").parents('.addbox').addClass('selectedAddress');
            $(".addbox input[type=radio]").click(function () {
                if ($(this).prop("checked")) {

                    $("input[type=\"radio\"]").parents('.addbox').removeClass("selectedAddress");
                    $("input[type=\"radio\"]:checked").parents('.addbox').addClass('selectedAddress');
                    // $("input[type=\"radio\"]").parents('.addbox').attr("style", "border:none");
                    // $("input[type=\"radio\"]:checked").parents('.addbox').attr("style", "border:2px solid #000 !important");
                }
            });
            getAddress();
            $(".userAddressid").click(function () {
                getAddress();
            });
        });

        // increment and descrement of numbers
        function increaseValue() {
            var value = parseInt(document.getElementById('number').value, 10);
            value = isNaN(value) ? 0 : value;
            value++;
            document.getElementById('number').value = value;
        }

        function decreaseValue() {
            var value = parseInt(document.getElementById('number').value, 10);
            value = isNaN(value) ? 0 : value;
            value < 1 ? value = 1 : '';
            value--;
            document.getElementById('number').value = value;
        }

        function getAddress() {
            $('.assignUserAddress').val($('input[name=userAddressid]:checked').val());
        }

    </script>

@endsection