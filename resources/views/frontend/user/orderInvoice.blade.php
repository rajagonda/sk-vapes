<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Invoice SK Vapes</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>
<!--
    font-family: 'Roboto', sans-serif;
-->
<body>

<div style="width:832px; margin:0 auto; font-family: 'Roboto', sans-serif; font-size:14px;">
    <table cellpadding="0" ; cellspacing="0" ; width="100%">
        <tr>
            <td width="50%" valign="top" colspan="2">
                <span style="font-size:25px; font-weight:bold; text-transform:uppercase;">Tax Invoice</span>
            </td>
            <td width="50%" align="right">
                <p style="margin-bottom:0;"><img src="/frontend/img/logo.png" alt="" title="skvapes"></p>
                <p style="font-size:16px; margin:0;"><strong>SK Vapes</strong></p>
                <p style="margin:0; line-height: 20px;">Pro1600 Amphitheatre Mountain View,</p>
                <p style="margin:0; line-height: 20px;">CA 10486 Phone: +1 888-555-0000</p>
            </td>

        </tr>
        <tr>
            <td height="20px" colspan="3" style="border-bottom:1px solid #bebebe"></td>
        </tr>
        <tr>
            <td colspan="3">
                <table width="260px">
                    <tr>
                        <td colspan="3" height="15px" ;>&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3"><p style="font-size:16px; margin:0;"><strong>ORIGINAL</strong></p></td>
                    </tr>
                    <tr>
                        <td width="90">Invoice No</td>
                        <td>:</td>
                        <td>{{ isset($orderDetails->order_transaction_id) ? $orderDetails->order_transaction_id : '---------------' }}</td>
                    </tr>
                    {{--<tr>--}}
                    {{--<td>Invoice Date</td>--}}
                    {{--<td>:</td>--}}
                    {{--<td>28 Mar 2018</td>--}}
                    {{--</tr>--}}
                    <tr>
                        <td>Order No.</td>
                        <td>:</td>
                        <td>{{ isset($orderDetails->order_reference_number) ? $orderDetails->order_reference_number : '---------------' }}</td>
                    </tr>
                    <tr>
                        <td>Order Date</td>
                        <td>:</td>
                        <td>{{ \Carbon\Carbon::parse($orderDetails->created_at)->format('d/m/Y H:i:s')}}</td>
                    </tr>
                    <tr>
                        <td colspan="3" height="15px" ;>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table width="100%">

                    @php
                        $address=unserialize($orderDetails->order_delivery_address);
                    @endphp

                    <tr>
                        <td width="48%" style="padding:0 15px 15px 15px; border:1px solid #d7e0f1; border-radius:10px;">
                            <p style="font-size:16px;"><strong>Shipping Address</strong></p>
                            <p style="margin:0;">{{ $address['ua_name'] }}</p>
                            <p style="margin:0;">{{ $address['ua_address'] }}
                                ,{{ $address['ua_city'] }},{{ $address['ua_state'] }}
                                ,{{ $address['ua_country'] }}</p>
                        </td>
                        <td>&nbsp;</td>
                        <td width="48%" style="padding:0 15px 15px 15px; border:1px solid #d7e0f1; border-radius:10px;">
                            <p style="font-size:16px;"><strong>Billing Address</strong></p>
                            <p style="margin:0;">{{ $address['ua_name'] }}</p>
                            <p style="margin:0;">{{ $address['ua_address'] }}
                                ,{{ $address['ua_city'] }},{{ $address['ua_state'] }}
                                ,{{ $address['ua_country'] }}</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td height="20px" colspan="3"></td>
        </tr>
        <tr>
            <td colspan="3">
                <table border="0" ; cellpadding="0" ; cellspacing="0" ; width="100%"
                       style="border:1px solid #d7e0f1; border-radius:10px;">
                    <tbody>
                    <tr height="47" style="background:#f5f7f9;">
                        <td align="center"
                            style="padding:0 15px; border-bottom:1px solid #d7e0f1; border-right:1px solid #d7e0f1; border-top-left-radius: 10px;">
                            <strong>#Iterm</strong></td>
                        <td style="padding:0 15px; border-bottom:1px solid #d7e0f1; border-right:1px solid #d7e0f1;">
                            <strong>Description</strong></td>
                        <td style="padding:0 15px; border-bottom:1px solid #d7e0f1; border-right:1px solid #d7e0f1;"
                            align="center"><strong>Qty</strong></td>
                        <td style="padding:0 15px; border-bottom:1px solid #d7e0f1; border-right:1px solid #d7e0f1;"
                            align="center"><strong>Unit Price</strong></td>
                        <td style="padding:0 15px; border-bottom:1px solid #d7e0f1; border-right:1px solid #d7e0f1;"
                            align="center"><strong>Sub Total</strong></td>
                        {{--<td style="padding:0 15px; border-bottom:1px solid #d7e0f1; border-right:1px solid #d7e0f1;" align="center" colspan="2"><strong>IGST</strong></td>--}}
                        <td style="padding:0 15px; border-top-left-radius: 10px;" align="center" rowspan="2"><p><strong>Total
                                    Amount</strong></p>
                            <p><strong>(Incl. tax)</strong></p></td>
                    </tr>
                    <tr height="47" style="background:#f5f7f9;">
                        <td style="padding:0 15px; border-right:1px solid #d7e0f1;">&nbsp;</td>
                        <td style="padding:0 15px; border-right:1px solid #d7e0f1;">&nbsp;</td>
                        <td style="padding:0 15px; border-right:1px solid #d7e0f1;">&nbsp;</td>
                        <td style="padding:0 15px; border-right:1px solid #d7e0f1;">&nbsp;</td>
                        <td style="padding:0 15px; border-right:1px solid #d7e0f1;">&nbsp;</td>
                        {{--<td style="padding:0 15px; border-right:1px solid #d7e0f1;" align="center"><strong>Rate</strong></td>--}}
                        {{--<td style="padding:0 15px; border-right:1px solid #d7e0f1;" align="center"><strong>Amount</strong></td>--}}
                    </tr>
                    @foreach($orderDetails->orderItems as $items)
                        <tr height="47">
                            <td style="padding:0 15px; border-right:1px solid #d7e0f1;"
                                align="center">{{ $items->getProduct->product_name }}</td>
                            <td style="padding:0 15px; border-right:1px solid #d7e0f1;">{!! str_limit($items->getProduct->product_description, 150) !!}</td>
                            <td style="padding:0 15px; border-right:1px solid #d7e0f1;"
                                align="center">{{ $items->oitem_qty }}</td>
                            <td style="padding:0 15px; border-right:1px solid #d7e0f1;"
                                align="center">Rs: {{ $items->oitem_product_price }}</td>
                            <td style="padding:0 15px; border-right:1px solid #d7e0f1;"
                                align="center">Rs: {{ $items->oitem_sub_total }}</td>
                            {{--<td style="padding:0 15px; border-right:1px solid #d7e0f1;" align="center">Rs: 12.00</td>--}}
                            {{--<td style="padding:0 15px; border-right:1px solid #d7e0f1;" align="center">Rs: 13,99</td>--}}
                            <td style="padding:0 15px;" align="center">Rs: {{ $orderDetails->order_total_price }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" height="40px" ;>&nbsp;</td>
        </tr>
        <tr>
            <td width="28%"><p style="padding-bottom:0; margin-bottom:0;">Registered office Details:</p>
                <p style="padding-top:0; margin-top:0;">
                    Jayalakshmi Estate,
                    New no 29(Old No.8),
                    Haddows Road, Chennai – 600 006
                </p></td>
            <td width="40%">&nbsp;</td>
            <td width="20%" valign="bottom" align="right"><p>(Authorized Signatory)</p></td>
        </tr>

    </table>

</div>

</body>
</html>