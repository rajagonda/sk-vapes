<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SK Vapes</title>
    @include('frontend._partials.stylesheets')
</head>

<body>
<!-- sign in -->
<section class="signin">
    <div class="signbgcol text-center">
        <article class="mx-auto">
            <h3 class="fwhite text-uppercase h4 flight">{{ __('message.login.welcome') }}</h3>
            <h1 class="fbold fwhite h1">SK VAPES</h1>
            <p class="fwhite">{{ __('message.login.subtext') }}</p>
        </article>
    </div>
    <!-- sign right section -->
    <div class="signright">
        <div class="signrtin mx-auto">
            <div class="signheader text-center">
                <a class="d-none d-sm-block" href="{{ route('home') }}">
                    <img class="signbrand" src="/frontend/img/logo.png" alt="" title="">
                </a>
                <h3 class="h3 py-1">{{ __('message.login.signup') }}</h3>
                <p>{{ __('message.login.signinInfo') }}</p>
            </div>
            <form method="POST" action="{{ route('register') }}" class="formsign pt-4" id="register" autocomplete="off">
                {{ csrf_field() }}
                <input type="hidden" name="role" value="1">
                <input type="hidden" name="status" value="active">
                <div class="form-group">
                    <label class="label text-uppercase">Name</label>
                    <input id="name" type="text" placeholder="Name"
                           class="form-control mt-2 flight {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"
                           value="{{ old('name') }}">
                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="label text-uppercase">Email Address</label>
                    <input type="text" placeholder="Email Address"
                           class="form-control mt-2 flight {{ $errors->has('email') ? ' is-invalid' : '' }}"
                           name="email" value="{{ old('email') }}">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{!! $errors->first('email') !!}</strong>
                       </span>
                    @endif
                </div>
                {{--<div class="form-group signsel">--}}

                    {{--<label class="label text-uppercase">Gender</label>--}}
                    {{--<div class=" mt-2 flight">--}}
                    {{--<label class="pr-3 pt-0">--}}
                        {{--<input type="radio" name="gender" class="" value="Male"/>--}}
                        {{--<span class="px-2">Male</span>--}}
                    {{--</label>--}}
                    {{--<label class="pr-3 pt-0">--}}
                        {{--<input type="radio" name="gender" value="Female">--}}
                        {{--<span class="px-2">Female</span>--}}
                    {{--</label>--}}
                    {{--</div>--}}

                {{--</div>--}}
                {{--<div class="form-group ">--}}
                    {{--<label class="label text-uppercase">Date of Birth</label>--}}

                    {{--<input type="text" placeholder="DD" class="form-control mt-2 flight datepicker" name="DOB"--}}
                           {{--value=""/>--}}
                    {{--@if ($errors->has('DOB'))--}}
                        {{--<span class="text-danger help-block">{{ $errors->first('DOB') }}</span>--}}
                    {{--@endif--}}

                {{--</div>--}}



                <div class="form-group signsel">
                    <div class="row">
                        <div class="col-md-3 pr-0">
                            <label class="label text-uppercase">Gender</label>
                            <select class="form-control w-100" name="gender">
                                @if(count(gender())>0)
                                    @foreach(gender() as $value)
                                        <option value="{{ $value }}" {{ (!empty(old('gender')) && old('gender')==$value)  ? 'selected' : '' }}>{{ $value }}</option>
                                    @endforeach
                                @else
                                    <option>No Prefixes Found</option>
                                @endif

                            </select>

                        </div>
                        <div class="col-md-9 m-0 p-0">
                            <label class="label text-uppercase">Date of Birth</label>

                            <input type="text" placeholder="DOB(dd-mm-yyyy) " class="form-control  datepicker" name="DOB"
                                   value=""/>
                            @if ($errors->has('DOB'))
                                <span class="text-danger help-block">{{ $errors->first('DOB') }}</span>
                            @endif
                        </div>
                    </div>

                </div>


                <div class="form-group signsel">
                    <label class="label text-uppercase">Mobile No</label>
                    <select class="form-control float-left" name="mobile_prefix">
                        @if(count(getCountriesByPrefixes())>0)
                            @foreach(getCountriesByPrefixes() as $prefix)
                                <option value="{{ $prefix }}" {{ (!empty(old('mobile_prefix')) && old('mobile_prefix')==$prefix)  ? 'selected' : '' }}>{{ $prefix }}</option>
                            @endforeach
                        @else
                            <option>No Prefixes Found</option>
                        @endif

                    </select>
                    <input type="text" name="mobile" placeholder="Enter Mobile Number"
                           class="form-control mt-2 flight {{ $errors->has('mobile') ? ' is-invalid' : '' }}">
                    @if ($errors->has('mobile'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('mobile') }}</strong>
                       </span>
                    @endif
                </div>
                <div class="form-group position-relative">
                    <label class="label text-uppercase">Password</label>
                    <input id="password" type="password" name="password" placeholder="Enter Your Password"
                           class="form-control mt-2 flight{{ $errors->has('password') ? ' is-invalid' : '' }} "
                    >
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                @endif
                <!-- popover-->
                    <a href="#" class="infodiv" data-toggle="popover" title="Password must contain"><i
                                class="fas fa-info-circle"></i></a>
                    <!--/ popover -->
                    <!-- show password icon -->
                    <span class="showpw"><i class="far fa-eye"></i></span>
                    <!--/ show password  icon -->

                    <!-- loaded popover content -->
                    <div id="popover-content" style="display: none">
                        <ul class="list-group custom-popover">
                            <li class="list-group-item"><span>At least 6 Characters</span></li>
                            <li class="list-group-item"><span>At least 1 Upper case letter (A - Z)</span></li>
                            <li class="list-group-item"><span>At least 1 Lower case Letter (a - z)</span></li>
                            <li class="list-group-item"><span>At least 1 Number (0 - 9)</span></li>
                            <li class="list-group-item"><span>One specl character like !, $, # ....</span></li>
                        </ul>
                    </div>
                    <!--/ loaded popover content -->
                </div>
                <div class="form-group position-relative">
                    <label class="label text-uppercase">Confirm Password</label>
                    <input id="password-confirm" type="password" placeholder="Enter Your Confirm Password"
                           class="form-control mt-2 flight" name="password_confirmation">
                    <!-- show password icon --><span class="showpw"><i class="far fa-eye"></i></span>
                    <!--/ show password  icon -->
                </div>
                <div class="form-group text-center my-1">
                    <input class="btn signbtn fmed" type="submit" value="SIGN UP"></div>
                <p class="text-center py-4">Already have an account? <a href="{{ route('userelogin') }}" class="fred">
                        Sign in</a></p>
            </form>

        </div>
        <!-- sign social -->
        <div class="signsocial mx-auto py-3">
            <ul class="listsignsocial text-cernter">
                <li class="d-inline"><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                <li class="d-inline"><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                <li class="d-inline"><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>

            </ul>
        </div>
        <!--/ sign social -->
    </div>
    <!--/ sign right section -->
</section>
<!--/ sign in -->

@auth
@else
    @if (empty(session('restrictData')))
        <!-- on page loading windows -->
        @include('frontend._partials.ageRuleModel')
    @endif
@endauth
@include('frontend._partials.scripts')
<script type="text/javascript" src="/frontend/modules/users/js/register.js"></script>
</body>

</html>
