<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('frontend._partials.stylesheets')
    @yield('headerStyles')
</head>

<body>

@include('frontend._partials.header')

@yield('content')

@auth
@else
    @if (empty(session('restrictData')))
        <!-- on page loading windows -->

        @include('frontend._partials.ageRuleModel')

    @endif
@endauth

@include('frontend._partials.footer')

@include('frontend._partials.scripts')

@yield('footerScripts')


<script>

    $(function () {


        {{--$("#search").autocomplete({--}}
        {{--source: function(request, response) {--}}
        {{--$.ajax({--}}
        {{--type: "POST",--}}
        {{--contentType: "application/json; charset=utf-8",--}}
        {{--url: "{{route('searchProductInfo')}}",--}}
        {{--data: "{'username':'" + document.getElementById('txtSearch').value + "'}",--}}
        {{--dataType: "json",--}}
        {{--success: function (data) {--}}
        {{--if (data != null) {--}}

        {{--response(data.d);--}}
        {{--}--}}
        {{--},--}}
        {{--error: function(result) {--}}
        {{--alert("Error");--}}
        {{--}--}}
        {{--});--}}
        {{--}--}}
        {{--});--}}

        $("#searchinpt").autocomplete({
            source: "{{route('searchProductInfo')}}",

            minLength: 1,
            select: function (event, ui) {
                /*
                var url = ui.item.id;
                if(url != '') {
                  location.href = '...' + url;
                }
                */
            },
            html: true,
            open: function (event, ui) {
                $(".ui-autocomplete").css("z-index", 1000);

            }
        })
            .autocomplete("instance")._renderItem = function (ul, item) {
            return $("<li>" +

                "<div>" +
                "<a href='" + item.product_url + "'>" +
                "<img width='50' height='50' style='float: left; margin: 10px' src='" + item.image + "'>" +

                "<span style='float: left; margin: 10px'>" +
                "<span style='padding: 0px 0px 10px 0px !important;display: block;'>" + item.name + "</span>" +
                "<div style='font-size: 11px;'>" +
                "Price <span class='linethrough'><i class=\"fas fa-rupee-sign\"></i>" + item.real_price + "</span> &nbsp;" +
                "<span><i class=\"fas fa-rupee-sign\"></i>" + item.sele_price + "</span>" +
                "</div>" +
                "</span>" +
                "</a>" +
                "<div style='clear: both'></div>" +
                "</div>" +

                "</li>").appendTo(ul);
        };

    });

</script>

</body>

</html>

