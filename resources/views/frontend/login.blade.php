<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SK Vapes</title>
    @include('frontend._partials.stylesheets')
</head>
<body>
<!-- sign in -->
<section class="signin">
    <div class="signbgcol text-center">
        <article class="mx-auto">
            <h3 class="fwhite text-uppercase h4 flight">{{ __('message.login.welcome') }}</h3>
            <h1 class="fbold fwhite h1">SK VAPES</h1>
            <p class="fwhite">{{ __('message.login.subtext') }}</p>
        </article>
    </div>
    <!-- sign right section -->
    <div class="signright">
        <div class="signrtin mx-auto">
            <div class="signheader text-center">
                <a href="{{ route('home') }}" class="d-none d-sm-block">
                    <img class="signbrand" src="/frontend/img/logo.png" alt="" title="" />
                </a>
                <h3 class="h3 py-1">{{ __('message.header.signin') }}</h3>
                <p>{{ __('message.login.signinInfo') }}</p>
            </div>
            <form method="POST" action="{{ route('userelogin') }}" aria-label="{{ __('Login') }}" class="formsign pt-4"
                  id="login">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="label text-uppercase">{{ __('message.login.email') }}</label>
                    <input id="email" type="text" placeholder="Email Address" class="form-control mt-2 flight{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"  value="{{ old('email') }}">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert"> <strong>{{ $errors->first('email') }}</strong>   </span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="label text-uppercase">{{ __('message.login.password') }}</label>
                    <input id="password" type="password" placeholder="Enter Your Password"
                           class="form-control mt-2 flight{{ $errors->has('password') ? ' is-invalid' : '' }}"
                           name="password">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group text-right"><a  class="fred" href="{{ route('password.reset') }}">{{ __('message.login.forgotpassword') }}?</a></div>
                <div class="form-group text-center my-4">
                    <input class="btn signbtn fmed" type="submit" value="{{ __('message.login.login') }}">
                </div>
                <p class="text-center py-4">{{ __('message.login.account') }}
                    <a href="{{ route('register') }}" class="fred">{{ __('message.login.signup') }}</a>
                </p>
            </form>
        </div>
        <!-- sign social -->
        <div class="signsocial mx-auto py-3">
            <ul class="listsignsocial text-center">
                <li class="d-inline"><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                <li class="d-inline"><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                <li class="d-inline"><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
            </ul>
        </div>
        <!--/ sign social -->
    </div>
    <!--/ sign right section -->
</section>
<!--/ sign in -->
@auth
@else
    @if (empty(session('restrictData')))
        <!-- on page loading windows -->
        @include('frontend._partials.ageRuleModel')
    @endif
@endauth
@include('frontend._partials.scripts')
<script type="text/javascript" src="/frontend/modules/users/js/login.js"></script>
</body>
</html>
