@extends('frontend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')

    <!--main Starts-->
    <section class="main">
        <!-- product detail header -->
        <section class="productview-header"></section>
        <!--/ product detail header -->
        <!-- cart checkout pages -->
        <div class="cartprocess">
            <div id="demo">
                <div class="step-app">
                    <ul class="step-steps container">
                        <li class="active"><a href="#" class="text-uppercase text-center">1 Shopping
                                Cart <i class="fas fa-chevron-right float-right"></i></a></li>
                        <li><a href="#" class="text-uppercase text-center">2 Order Confirmation <i
                                        class="fas fa-chevron-right float-right"></i></a></li>
                        <li><a href="#" class="text-uppercase text-center stepanchor "> 3 Complete
                                Payment </a></li>
                    </ul>
                    <div class="step-content container mb-5">
                        <tr class="tabin">
                            <!-- this is shopping cart -->
                            <!-- single product -->
                            <!--table -->
                            <table id="table" class="resptable table shoppingcarttable">
                                <thead>
                                <th class="image">Images</th>
                                <th class="proname">Product Name</th>
                                <th>Price</th>
                                <th style="text-align: center">Quantity</th>
                                <th>Total</th>
                                <th style="text-align: center">Remove</th>
                                </thead>

                                <tbody>
                                <?php
                                if (Cart::content()->count() > 0){ foreach(Cart::content() as $row) :?>
                                <tr>
                                    <td>
                                        @php

                                            if($row->options->image){
                                            $ptname=$row->options->image;
                                            }else{
                                             if (!empty(getProductImage($row->id))){
                                              $ptname=getProductImage($row->id)->pi_image_name;
                                             }else{
                                             $ptname='';
                                             }
                                            }

                                        @endphp
                                        <figure class="cartimg">
                                            <a href="#">
                                                @if (!empty($ptname))
                                                    <img src="/uploads/products/thumbs/{{$ptname }}">
                                                @else
                                                    <img src="https://via.placeholder.com/150x150.png?text=No Image"
                                                         alt="" class="img-fluid object-fit-cover" title="">
                                                @endif
                                            </a>
                                        </figure>
                                    </td>
                                    <td class="pronametd">
{{--                                        {{dd($row)}}--}}

                                        <h6 class="fmed h6"><a
                                                    href="{{ route('productDetails',['product_alias'=>$row->options->alias]) }}"><?php echo $row->name; ?></a>
                                        </h6>
                                        <p class="fgrey"><?php echo($row->options->has('description') ? str_limit($row->options->description, 150) : ''); ?></p>
                                        <p class="fgrey">

{{--                                            {{  dump($row->options->size)  }}--}}

                                            <?php echo $row->options->color ? '<b>Color:</b> '.strtoupper($row->options->color):''; ?>

                                            <?php echo ($row->options->size) ? '<b>Size/Capacity:</b> '.strtoupper($row->options->size):''; ?>
                                        </p>
                                    </td>
                                    <td>
                                        <span class="price"><i class="fas fa-rupee-sign"></i> <?php echo $row->price; ?></span>
                                    </td>
                                    <td style="text-align: center">
                                        <form style="width:82px;" class="brd mx-auto">
                                            <div class="value-button" onclick="decreaseValue({{$row->id}})"
                                                 value="Decrease Value"> -
                                            </div>
                                            <input type="hidden" class="row_{{$row->id}}" value="{{$row->rowId}}" />
                                            <input type="number" id="val_{{$row->id}}" value="<?php echo $row->qty; ?>"
                                                   class="number itemQty<?php echo $row->rowId; ?>"/>
                                            <div class="value-button" onclick="increaseValue({{$row->id}})"
                                                 value="Increase Value">+
                                            </div>
                                        </form>
                                        {{--<a style="padding-top:5px;" href="#"--}}
                                           {{--class="updateCartItem d-block"--}}
                                           {{--data-id="{{$row->rowId}}">Update</a>--}}
                                    </td>
                                    <td>
                                        <span class="price fred"><i
                                                    class="fas fa-rupee-sign"></i> <?php echo $row->subtotal; ?></span>
                                    </td>
                                    <td style="text-align: center"><a href="#" class="removeCartItem"
                                                                      data-id="<?php echo $row->rowId; ?>"><i
                                                    class="far fa-times-circle" style="font-size:20px;"></i></a>
                                    </td>
                                </tr>

                                <?php
                                endforeach;
                                }else { ?>
                                <tr>
                                    <td colspan="6">
                                        No data found
                                    </td>
                                </tr>

                                <?php  }
                                ?>

                                </tbody>
                            </table>
                            <!--/ table -->
                            <?php
                            if (Cart::content()->count() > 0){ ?>
                            <div class="row mx-0 py-3 justify-content-end border-top border-bottom border-left border-right">
                                <div class="col-lg-10">
                                    <div class="row ">
                                        <div class="col-lg-6 border-right text-right pr-4"><span class="actfont">
                                                <a href="{{ route('home') }}">CONTINUE SHOPPING</a> </span> <a
                                                    class="signbtn btn " href="{{ route('orderConfirmation') }}">CHECKOUT</a>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="col-lg-5 text-right">
                                                    <p style="padding-top:15px;">{{ Cart::content()->count() }} items selected, total</p>
                                                    {{--<p>Free shipping on orders above 500</p>--}}
                                                </div>
                                                <div class="col-lg-7 text-right">
                                                    <h1 class="fbold fred h2" style="padding-top:5px;"><i class="fas fa-rupee-sign"  style="font-size:28px;"></i> <?php echo Cart::subtotal(); ?>
                                                    </h1>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/ this is shopping cart -->
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <!--/ cartr checkout page-->
    </section>
    <!--/main Ends-->

@endsection
@section('footerScripts')
    <script>
        function updatecarts(rowid) {
            var id = rowid;
            var qty = $('.itemQty' + id).val();

            if (id != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '{{ route('updateItemFromCart') }}',
                    type: 'POST',
                    data: 'id=' + id + '&qty=' + qty,
                    success: function (response) {
                        location.reload();
                    }
                });
            }
        }
        function increaseValue(id) {

           var rowid=  $('.row_'+id).val();

            var value = parseInt($('#val_' + id).val(), 10);
            value = isNaN(value) ? 0 : value;
            value++;
            $('#val_' + id).val(value);
            updatecarts(rowid)
        }

        function decreaseValue(id) {
            var rowid=  $('.row_'+id).val();

            var value = parseInt($('#val_' + id).val(), 10);
            value = isNaN(value) ? 0 : value;
            value < 1 ? value = 1 : '';
            value--;
            $('#val_' + id).val(value);

            updatecarts(rowid)
        }
    </script>
@endsection