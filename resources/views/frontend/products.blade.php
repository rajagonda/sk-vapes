@extends('frontend.layout')
@section('title', $title)

@section('headerStyles')
    <style>
        html {
            height: 100%;
        }

        body {
            padding: 0;
            margin: 0;
            height: 100%;
        }

        h2 {
            color: white;
            font-family: sans-serif;
            background-color: teal;
            padding: 10px;
            font-weight: lighter;
        }

        h2 a {
            float: right;
            color: white;
            text-decoration: none;
            vertical-align: bottom;
        }

        #wrap {
            width: 550px;
            margin: 0 auto;
        }

        pre, pre.noclick {
            text-align: left;
            background-color: #EEE;
            border-left: 5px solid teal;
            cursor: pointer;
            border-top: 1px solid transparent;
            border-bottom: 1px solid transparent;
            border-right: 1px solid transparent;
        }

        pre:hover {
            background-color: #f4f4f4;
            border-color: teal;
        }

        pre:active {
            background-color: #DDD;
        }

        pre.noclick {
            cursor: inherit;
        }

        pre.noclick:hover {
            background-color: #EEE;
            border-top-color: transparent;
            border-right-color: transparent;
            border-bottom-color: transparent;
        }

        footer .footernav {
            font-family: sans-serif;
            font-size: 12px;
        }

        footer .footernav {
            color: #aaa;
        }

        .title {
            font-size: 57px;
            font-weight: bold;
            color: #555;
            margin-bottom: 0;
        }

        .subtitle {
            font-size: 14px;
            color: #999;
            margin-top: -10px;
        }

        .version {
            font-size: 10px;
            font-weight: lighter;
            font-family: sans-serif;
            color: #555;
        }

        .s {
            color: teal;
        }

        .b {
            color: purple;
        }

        .f {
            font-weight: bold;
        }

        .n {
            font-weight: bold;
        }

        pre {
            padding: 10px;
            background-color: #EEE;
        }

        hr {
            height: 5px;
            border: 0;
            margin: 0;
        }

        .comment {
            color: #AAA;
        }

        .string {
            color: teal;
        }

        .tag {
            color: blue;
        }

        .attr {
            color: green;
        }

        .button_download {
            display: block;
            font-family: sans-serif;
            cursor: pointer;
            width: 60px;
            padding: 10px 30px 10px 30px;
            font-weight: bold;
            font-size: 20px;
            text-decoration: none;
            text-align: center;
            margin: 0 auto;
            background-color: #444;
            color: #EEE;
            transition: all 0.3s;
            -moz-transition: all 0.3s;
            -webkit-transition: all 0.3s;
            -o-transition: all 0.3s;
            -ms-transition: all 0.3s;
        }

        .button_download:hover {
            width: 480px;
            background-color: yellowgreen;
            color: #444;
        }

        .step {
            font-weight: bold;
        }
    </style>
    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>--}}
    {{--<script href="http://localhost/Simple-Easy-jQuery-Notification-Plugin-NotifIt/notifIt/js/notifIt.js" type="text/javascript"></script>--}}
    {{--<link href="http://localhost/Simple-Easy-jQuery-Notification-Plugin-NotifIt/notifIt/css/notifIt.css" type="text/css" rel="stylesheet">--}}

@endsection

@section('content')

    <!--main Starts-->
    <section class="main">
        <!-- products list header-->
        <section class="headerprolist ">
            <div class="container ">
                <div class="row justify-content-center">
                    <div class="col-lg-6 align-self-center text-center">
                        <h1 class="h3 fmed fwhite text-uppercase">{{ $categoryInfo->category_name }} Products</h1>
                    </div>
                </div>
            </div>
        </section>
        <!--/ products list header -->
        <!-- product list body -->
        <section class="productbody">
            <!--nav-->
            <div class="productlist-nav">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-12">
                            <ul class="nav float-left">
                                <li class="nav-item {{ empty(Request::segment(3)) ? 'selectnav' : ''  }}"><a
                                            href="{{ route('userproducts',['category'=>Request::segment(2)]) }}"
                                            class="nav-link"><span>All</span></a>
                                </li>

                                <?php
                                $brandslimit = false;
                                ?>

                                @foreach(getBrands($categoryInfo->category_id) as $key=>$brand)

                                    @if($loop->iteration < 8 )

                                        <li class="nav-item {{ (!empty(Request::segment(3)) && (Request::segment(3)==$key)) ? 'selectnav' : ''  }}">

                                            <a href="{{ route('userproducts',['category'=>Request::segment(2),'brand'=>$key]) }}"
                                               class="nav-link"><span>{{ $brand }}</span>
                                            </a>
                                        </li>

                                    @endif

                                    <?php
                                    if ($loop->iteration > 8) {
                                        $brandslimit = true;
                                    }
                                    ?>


                                @endforeach



                            </ul>

{{--                            @if($brandslimit ==true )--}}
                                <div class="dropdown float-right">
                                    <a href="#" class=" dropdown-toggle  "
                                            data-toggle="dropdown" style="margin-top: 13px; display: block;">
                                        More+
                                    </a>
                                    <div class="dropdown-menu">
                                        @foreach(getBrands($categoryInfo->category_id) as $key=>$brand)
                                            <a class="dropdown-item {{ (!empty(Request::segment(3)) && (Request::segment(3)==$key)) ? 'selectnav' : ''  }}" href="{{ route('userproducts',['category'=>Request::segment(2),'brand'=>$key]) }}">
                                                {{ $brand }}
                                            </a>
                                        @endforeach

                                    </div>
                                </div>
                            {{--@endif--}}

                            {{--<ul class="nav float-right">--}}
                            {{--<li class="nav-item"><a href="#" class="nav-link"><span>New</span> </a>--}}
                            {{--</li>--}}
                            {{--<li class="nav-item"><a href="#" class="nav-link"><span>Price <i--}}
                            {{--class="fas fa-long-arrow-alt-up"></i> </span> </a></li>--}}
                            {{--<li class="nav-item"><a href="#" class="nav-link"><span>Price <i--}}
                            {{--class="fas fa-long-arrow-alt-down"></i></span> </a></li>--}}
                            {{--</ul>--}}
                        </div>



                    </div>
                </div>
            </div>
            <!--/ nav -->
            <!-- list of products -->
            <div class="productslist">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-12">
                            <ul class="nav float-left">
                                <li>
                                   <h4 style="  text-transform: uppercase; font-size: 18px;"> {{  (!empty(Request::segment(3)))? ucfirst(Request::segment(3)) :'' }}</h4>
                                </li>
                            </ul>
                            <ul class="nav float-right">
                                <li class="nav-item"><a href="#" class="nav-link"><span>New</span> </a>
                                </li>
                                <li class="nav-item"><a href="#" class="nav-link"><span>Price <i
                                                    class="fas fa-long-arrow-alt-up"></i> </span> </a></li>
                                <li class="nav-item"><a href="#" class="nav-link"><span>Price <i
                                                    class="fas fa-long-arrow-alt-down"></i></span> </a></li>
                            </ul>
                        </div>


                        @if (count($products))
                            @foreach($products as $product)
                                <div class="col-lg-3 col-sm-6 list-product text-center">
                                    <div class="productcol">
                                        <figure>
                                            @php

                                                if($product->product_image){
                                                $ptname=$product->product_image;
                                                }else{
                                                 if (count($product->productImages)>0){
                                                  $ptname=$product->productImages[0]->pi_image_name;
                                                 }else{
                                                 $ptname='';
                                                 }
                                                }

                                            @endphp
                                            <a href="{{ route('productDetails',['product_alias'=>$product->product_alias]) }}" class="{{ ($product->product_availability != 'in_stock')?'outofstack':''  }}_box">
                                                @if (!empty($ptname))
                                                    <img
                                                            src="/uploads/products/thumbs/{{ $ptname }}"
                                                            alt=""
                                                            class="img-fluid object-fit-cover productimg" title="">
                                                @else

                                                    <img src="https://via.placeholder.com/400x300.png?text=No Image"
                                                         alt=""
                                                         class="img-fluid object-fit-cover productimg"
                                                         title="">

                                                @endif

                                                    @if ($product->product_availability != 'in_stock')
                                                        <div class="{{ ($product->product_availability != 'in_stock')?'outofstack':''  }}">
                                                            <img src="https://i.ibb.co/DCvbxN2/outofstack.png" class="img-fluid object-fit-cover" />
                                                        </div>

                                                    @endif

                                            </a>
                                        </figure>
                                        <article><a class="fmed linkpro"
                                                    href="{{ route('productDetails',['product_alias'=>$product->product_alias]) }}">{{ $product->product_name }}</a>
                                            <p class="py-4 pricep"><i class="fas fa-rupee-sign"></i><span
                                                        class="linethrough price">{{ $product->product_real_price }}</span>
                                                <span class="price fred"><i
                                                            class="fas fa-rupee-sign"></i>{{ $product->product_price }}</span>
                                            </p>
                                            <p class="likekart">

                                                @if ($product->product_availability == 'in_stock')
                                                    <a href="{{ route('productDetails',['product_alias'=>$product->product_alias]) }}"
                                                       title="Add to Cart">
                                                        <span class="icon-online-shopping-cart moonfont"></span>
                                                    </a>
                                                @else

                                                    <span href="#" class="text-danger">
                                                        {{ availability($product->product_availability)  }}
                                                    </span>


                                                @endif




                                                {{--<a href="#"--}}
                                                {{--data-toggle="tooltip"--}}
                                                {{--data-placement="bottom" data-id="{{ $product->product_id }}"--}}
                                                {{--title="Add to Cart" class="addToCart">--}}
                                                {{--<span class="icon-online-shopping-cart moonfont"></span>--}}
                                                {{--</a>--}}
                                                <a
                                                        href="#"
                                                        data-toggle="tooltip"
                                                        data-placement="bottom"
                                                        data-id="{{ $product->product_id }}"
                                                        data-userid="{{ Auth::id() }}"
                                                        class="productaddwishlist{{ $product->product_id }} addToWishList @if(in_array($product->product_id,$wishlistProducts))likeactive @endif"
                                                        title="Add to Wishlist">
                                                    <span class="icon-heart moonfont"></span>
                                                </a>
                                            </p>
                                        </article>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="col-lg-3 list-product text-center">

                                No Products Found

                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <!--/ list of products -->
        </section>
        <!--/ product list body-->
    </section>
    <!--/main Ends-->

@endsection
@section('footerScripts')

@endsection