@extends('frontend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')

    <!--main Starts-->
    <section class="main">
        <!-- product overview header -->
        <section class="productview-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <!-- brudcrumb -->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb nobg mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">About</li>
                            </ol>
                        </nav>
                        <!--/ brudcrumb -->
                    </div>
                </div>
            </div>
        </section>
        <!--/ product overview header -->
        <!-- cart checkout pages -->
        <div class="account">
            <div class="container mb-5 stpage">
                <div class="row py-5">
                    <div class="col-lg-12">
                        <h4 class="h4">About SK Vapes</h4>
                        <p>Our Stance on Electronic Cigarettes Many people, who have successfully quit, have done so with the help of electronic cigarettes. Many of us have tried other methods to quit smoking, such as the patch, nicotine gum, or even cold turkey. For many people, those attempts ended in relapse. Finally, product was introduced that allowed you to get your nicotine fix without smoking tobacco or chewing gum: e-cigarettes. Non-smokers don’t realize that the allure of smoking isn’t just the nicotine, but also the ritual of smoking. Smokers enjoy the action of holding up a cigarette to their lips and taking a drag of smoke. Although you don’t have to fumble with a lighter, e-cigs give the vaper an experience very similar to smoking tobacco cigarettes. E-Cigarettes vs. Tobacco Cigarettes We believe that electronic cigarettes can help to reduce harm. According to the United Kingdom's Department of Health, electronic cigarettes are "around 95% less harmful than tobacco." The smoke from burning tobacco contains many chemicals which are not present when you use an e-cigarette or vaping device. The Truth about E-Cigs We feel that there are a lot of misconceptions about e-cigarettes. In fact, a lot of the stigma on smoking has been unfairly passed on to vaping. It is time to think of vaping as something completely different from smoking.</p>
                    </div>
                </div>

            </div>
        </div>
        <!--/ cartr checkout page-->
    </section>
    <!--/main Ends-->

@endsection
@section('footerScripts')
@endsection