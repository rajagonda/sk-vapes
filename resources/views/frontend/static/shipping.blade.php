@extends('frontend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')

    <!--main Starts-->
    <section class="main">
        <!-- product overview header -->
        <section class="productview-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <!-- brudcrumb -->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb nobg mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Shipping</li>
                            </ol>
                        </nav>
                        <!--/ brudcrumb -->
                    </div>
                </div>
            </div>
        </section>
        <!--/ product overview header -->
        <!-- cart checkout pages -->
        <div class="account">
            <div class="container mb-5">
                <div class="row py-5">
                    <div class="col-lg-12">
                    Is Nicotine harmful ?
                    No, it's not. The effects of nicotine itself are similar to that other popular drug, caffeine. See our (nicotine reading list.) There is no evidence that nicotine causes any substantial risk for cancer, and the research shows that the risk for cardiovascular disease is minimal.
                    The research suggests nine out of 10 people falsely believe nicotine is very harmful to their heath, when in fact it is no more dangerous than the caffeine in a cup of coffee. ... Tobacco also contains some nicotine, but according to the new report,nicotine itself isn't harmful.
                    It isn't the stuff that can cause serious illness and death from cancer, lung, and heart disease. Those culprits are the tar and toxic gases that are released from burning tobacco when you smoke. Nicotine is a chemical that is dangerous not because it causes cancer but because it can addict you to cigarettes.
                    IS NICOTINE BAD IN ITSELF ?
                    Nicotine, in and of itself, is not a harmful substance unless taken in large quantities. Used in moderate levels, nicotine actually boasts some significant health benefits. ... Yes, nicotine can be addictive. However, nicotine is no more addictive than caffeine and is no more dangerous in suggested doses.
                    IS  NICOTINE HARMFUL FOR THE LUNGS ?
                    A: No, tar in cigarette smoke causes cancer. But, Nicotine is an addictive drug. It is not the nicotine in cigarette smoke that causes cancer. Nicotine may keep you smoking, but it is the other bad chemicals in cigarettes that make smoking sodangerous
                    IS THERE NICOTINE IN VEGETABLES ?
                    There is considerable evidence that nicotine is present in certain human foods, especially plants from the family Solanaceae (such as potatoes, tomatoes, and eggplant).
                    •	Tomato. ...
                    •	Potato. ...
                    •	Eggplant. ...
                    •	Teas. ...
                    •	Peppers and Capsicums. ...
                    •	Cauliflower.
                    HOOKAH ?
                    Hookah is a pipe used to smoke Shisha, a combination of tobacco and fruit or vegetable that is heated and the smoke is filtrated through water. The Hookah consists of a head, body water bowl and hose. The tobacco or Shisha is heated in the hookah usually using charcoal. According to a World Health Organization advisory, a typical one-hour session of hookah smoking exposes the user to 100 to 200 times the volume of smoke inhaled from a single cigarette. Even after passing through water, tobacco smoke still contains high levels of toxic compounds, including carbon monoxide, heavy metals and cancer-causing chemicals (carcinogens). Hookah smoking also delivers significant levels of nicotine — the addictive substance in tobacco. Hookah smoking has been associated with lung, mouth and other cancers, heart disease and respiratory infections. The substances used to heat the tobacco also produce carbon monoxide, heavy metals and cancer causing chemicals, creating it own health hazards. Sharing the mouthpiece of the Hookah has been associated with mouth and other infections including herpes, tuberculosis and hepatitis.
                    PG vs VG – All Things You Should Know About E-liquid
                    The eliquid – also named as ejuice is the liquid used in electronic cigarette for producing vapor and simulating smoking. It’s normally composed of up to 4 ingredients:
                    •	PG (Propylene glycol)
                    •	VG (Vegetable Glycerin)
                    •	Flavor (Hundreds kind of food flavors)
                    •	Nicotine (You can choose 0 nicotine)
                    Also you can add some distilled water to dilute the E-Liquid if it’s too thick, such as 100% VG based eliquid.
                    2: PG vs VG
                    The PG (Propylene glycol) and VG (Vegetable Glycerin) are the two primary base ingredients of eliquid. They are all FDA approved and are equally safe.
                    •	The PG is a clear, colorless liquid which usually been used as a flavoring carrier in food products, The main applications of Vegetable Glycerin are cosmetics products, food production, and e-juice.
                    •	The VG is a natural organic liquid made from vegetable oil, it’s sweet and thick, more viscous than PG. The main applications of Vegetable Glycerin are cosmetics products, food production, and e-juice.
                    •	The PG is thinner and the VG is thicker.
                    E-liquid can be made up of 100% PG base, 100% VG base or a combination of both. But which one is better? We will talking about this in different categories:
                    Throat Hit: PG based eliquid provides a stronger throat hit and more pure flavor taste than the VG based ones.
                    Vapor Production: VG based leiquid creates more vapor than the PG based ones, and it has a slightly sweet aftertaste.
                    Storage Time: The PG based eliquid has longer storage time than the VG based ones cos its powerful moisturizing properties.
                    Temperature Resistance: VG based eliquid has higher temperature resistance than the PG based one.
                    So: Higher Power needed to produce vapor with VG based eliquid. But it may cause burnt tastes using high power with PG based eliquid.
                    Atomizer Friendliness:
                    •	PG is thinner and has low density, it’s absorbed much faster by wick materials, and less likely to gunk up your wicks and heating elements of the atomizers, meaning less cleaning required for the atomizers/ That will meet many popular atomizers/ clearomizers in the market, such as CE4, vivi nova and protank.
                    •	VG is thicker and has high density, it can produce thicker clouds, but it’s also more likely to build up on your wicks, heating elements and other vaping components, meaning more cleaning and maintenance will be required. So it’s best to use with the rebuildable atomizers, such as Trident, Omega, etc.
                    P.S. A tiny percentage of people have an allergy to PG, such as rash or upset stomach.
                    Now most eliquid companies mix both PG and VG together to retain the advantages of the 2 ingredients. PG deliver you better flavor taste, stronger throat hit, and VG produce more vapors. The typical ratio of the PG and VG is 50/50, 60/40 and 70/30.
                    3: About the Nicotine
                    We don’t suggest those non-smokers / new vapers use nicotine in the eliquid cos it’s addictive.
                    The nicotine level is measured by the number of milligrams of nicotine per milliliter of liquid. So 24mg/ml means there is 24 milligrams of nicotine in 1 milliliter of liquid. For those smokers who wanna switch to vaping, you can choose the different nicotine levels depending on how many traditional cigarettes you consume daily. Just test and choose the best level for you.
                    Here is a list of Approximate Nicotine Levels used by many eliquid companies:
                    •	0mg = None
                    •	1mg to 11mg = Low
                    •	12mg to 16mg = Medium
                    •	17mg – 24mg = High
                    •	25mg or more = Extra High
                    Don’t forget that you switch to ecig cos you want to quit, so don’t choose too high level, and also downgrade the nicotine level step by step to help you quit.
                    4: About the Flavor
                    The flavor concentrate is another important ingredient of eliquid which will provide you the taste you want. You can buy them online or you can develop your own flavors.
                    There are about 5% to 20% flavors added in most eliquid. EXCEPTION some Tobacco Flavors you only need 1% to 3%. Let me explain this:
                    if 10ml of eliquid = 100%, then there are:
                    95%-80% of PG or VG or PG/VG Mixture
                    5% – 20% flavor concentrate
                    and some nicotine.
                    So if you want to mix your own eliquid, such as: 10ml, 18mg, 50%PG / 50%VG, 10% Apple Flavor, you need to prepare:
                    1): 1ml of apple flavor
                    2): 4.5ml PG, 4.5ml VG
                    3): 180mg nicotine
                    Put them together in a bottle and shake for 15 seconds, and your eliquid is ready for testing.
                    5: Which Eliquid is Best for you?
                    There is no one can tell you which eliquid is exactly the best for you. the only way is to test by yourself. The 50/50, 60/40 or 70/30 mixtures is a good choice to start If you don’t know how to choose.
                    If you want more pure flavor tasting, just add some PG; if you want more vapor, just add some VG. Give it some time and you will find out the best proportions for you.
                    In my opinion, the 70%PG / 30%VG working great with most of the clearomizers in the market, and the 20%PG/80%VG is great for those RBAs.
                    PLUS: 6: PH Level of Eliquids
                    Just like other liquid, you should choose a good balance of PH level to get the best tastes when you buy or mix your own eliquid. Use a pH test strip to test it before you vape it.
                    It is advised that e-liquid pH levels should be between 6-8 for the ultimate taste.
                    In most cases, the PH level of eliquid will be on the side of alkaline. Because the ingredients of the eliquid, such as PG / VG are all alkalescent, even the flavor concentrate. So you can use some acidic juice or flavor concentrate, such as Lemon juice or Orange juice to counteract it.
                    Don’t forget to test the PH level after you added those acidic juice into your eliquid. Enjoy vaping and vaping healthily.
                    </div>
                </div>
            </div>
        </div>
        <!--/ cartr checkout page-->
    </section>
    <!--/main Ends-->

@endsection
@section('footerScripts')
@endsection