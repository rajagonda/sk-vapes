@extends('frontend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')

    <!--main Starts-->
    <section class="main">
        <!-- product overview header -->
        <section class="productview-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <!-- brudcrumb -->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb nobg mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Warranty</li>
                            </ol>
                        </nav>
                        <!--/ brudcrumb -->
                    </div>
                </div>
            </div>
        </section>
        <!--/ product overview header -->
        <!-- cart checkout pages -->
        <div class="account">
            <div class="container mb-5 stpage">
                <div class="row py-5">
                   <div class="col-lg-12">
                       <p>Sk Vapes is committed to deliver you authentic top quality products with free and fast delivery, 60-day warranty, best prices, and more, to ensure you shop with utmost confidence!</p>
                       <h5>100% AUTHENTIC</h5>
                       <p>We buy directly from manufacturers, from certified and authorized international distributors, or select products and brands are imported parallelly and sold on Sk Vapes. All products are 100% authentic. You are welcome to check the product code for each of the devices on the manufacturer’s website whenever applicable. We take great pride in only selling original and 100% authentic products.</p>
                       <h5>100% SECURE PAYMENT</h5>
                       <p>Pay via cash or debit/credit card from options available. All your banking information is processed via secure and established payment gateways or wallets. All your personal information is processed in secured servers with advance technology security protocols. We take security of information very seriously.</p>

                       <h5>100% DISCRETE PACKAGING</h5>
                       <p>Be assured that there is no product category name written on the outside of the package. This feature is made available in case you are looking for discretion, even though switching from tobacco cigarettes to e-cigarettes is moment of pride!   </p>

                       <h5>60-DAY WARRANTY</h5>
                       <p>All products have a 60 day manufacturing warranty, with the exception of  tanks and e liquids. Refer to Terms & Conditions for further information. </p>

                       <h5>BEST PRICE GUARANTEE</h5>
                       <p>Not only do we offer the best selection of products, but also the best prices to our customers. If you happen to find a product being sold at a lower price, we will match the price to that offering. Kindly note that we will price match with only authorized and registered Indian retailers. This feature is not applicable with out-of-stock, counterfeit and clearance products. Price to be compared includes taxes and excluding shipping, COD charge, discounts on MRP, and is applicable within 10 days from date of purchase. </p>
                   </div>
                </div>
            </div>
        </div>
        <!--/ cartr checkout page-->
    </section>
    <!--/main Ends-->

@endsection
@section('footerScripts')
@endsection