@extends('frontend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')

    <!--main Starts-->
    <section class="main">
        <!-- product overview header -->
        <section class="productview-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <!-- brudcrumb -->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb nobg mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Terms &amp; Conditions</li>
                            </ol>
                        </nav>
                        <!--/ brudcrumb -->
                    </div>
                </div>
            </div>
        </section>
        <!--/ product overview header -->
        <!-- cart checkout pages -->
        <div class="account">
            <div class="container mb-5 stpage">
                <div class="row py-5">
                    <div class="col-lg-12">
                        <h5>Terms & Conditions</h5>
                        <p>The following is an informative Sk Vapes Terms & Conditions. By placing an order via vapestop.in, you are acknowledging that you are in full agreement of these Terms & Conditions:</p>
                        <p>The website is meant for adult smokers of the age of 18 or above. Your presence on this site is an agreement that you are of legal smoking age, and ordering from a province/state where electronic cigarettes, vaping, e-liquids are permitted. We ask you with all seriousness to leave the website immediately if you are under 18 years old.</p>
                        <p>The products are not for medical use and not for people with unstable heart condition, pregnant or breast-feeding women. This is not a proven cessation device, though many claim to have left tobacco cigarettes because of electronic cigarettes. Several studies demonstrate that e-cigarettes/vaping is significantly less harmful than smoking tobacco. Products are meant to be used at your own discretion.</p>
                        <h5>WARRANTY:</h5>
                        <p>All products have a 60 day manufacturing warranty, with the exception of tanks and e liquids. Manufacturing defects only are covered in the warranty, and issues related to abuse, misuse or normal wear & tear including exposure to humidity or heat are not covered in the warranty. Upon inspection, if the product is found to be defective, we will ship you a replacement item. Please contact our customer service team for next steps if your product has a manufacturing defect.</p>

                        <h5>INCORRECT DELIVERY ADDRESS:</h5>
                        <p>It is the responsibility of the customer to enter the correct delivery address for products. If you have entered an incorrect mailing address, then please get back to us as soon as possible, and if the package has not left for delivery, we will edit the mailing address.</p>

                        <h5>LIMITATION OF LIABILITY:</h5>
                        <p>Sk Vapes is not liable to any loss or damage caused wholly or mainly by your breach of our Terms & Conditions. We hereby disclaim any and all liability for personal injury, product defect and/ or failure, claims that arise that are due to normal usage, misuse of products, abuse of products, product modifications, improper product selection. Our liability shall not in any event include losses related to any business of a customer including but not limited to lost profits or business interruption.</p>
                    </div>
                </div>
            </div>
        </div>
        <!--/ cartr checkout page-->
    </section>
    <!--/main Ends-->

@endsection
@section('footerScripts')
@endsection