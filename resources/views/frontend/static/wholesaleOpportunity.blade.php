@extends('frontend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')

    <!--main Starts-->
    <section class="main">
        <!-- product overview header -->
        <section class="productview-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <!-- brudcrumb -->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb nobg mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Wholesale Opportunities</li>
                            </ol>
                        </nav>
                        <!--/ brudcrumb -->
                    </div>
                </div>
            </div>
        </section>
        <!--/ product overview header -->
        <!-- cart checkout pages -->
        <div class="account">
            <div class="container mb-5 stpage">
                <div class="row py-5">
                    <div class="col-lg-12">
                        <h5>WHOLESALE OPPORTUNITY</h5>
                        <p>SK VAPES PRODUCTS NOW AVAILABLE FOR WHOLESALE!</p>
                        <p>Sk Vapes being a pioneer in the industry is positioned to become the leading supplier of vaping products in both the online and offline distribution channel. We have entered the market at a nascent stage, and have invested in the required infrastructure and expertise to remain the market leader in the long term. Wholesale purchase available in South Asian countries including India, Nepal, Bangladesh, and Sri Lanka across brands like JUUL, Cafe Racer, SMOK, Vaporesso, Joyetech, Aspire, Charlie's Chalk Dust, Cosmic Fog, Cuttwood, and more. </p>

                        <p>Key features for the offline market:</p>
                        <ul class="listcontent">
                            <li><span><i class="fas fa-angle-right"></i></span>Fast inventory replenishment to avoid stock-outs</li>
                            <li><span><i class="fas fa-angle-right"></i></span>Appealing margins and incentives to channel partners</li>
                            <li><span><i class="fas fa-angle-right"></i></span>Sales staff training for customer education</li>
                            <li><span><i class="fas fa-angle-right"></i></span>Dedicated account manager support</li>
                            <li><span><i class="fas fa-angle-right"></i></span>Focus on metro cities, which contributes to 85% of total e cigarette sales</li>
                            <li><span><i class="fas fa-angle-right"></i></span>100% authentic products</li>
                            <li><span><i class="fas fa-angle-right"></i></span>Low minimum order quantity on orders</li>
                            <li><span><i class="fas fa-angle-right"></i></span>Franchise brick & mortar store opportunity with growth potential</li>
                            <li><span><i class="fas fa-angle-right"></i></span>Sole India distributor/agent of selected brands</li>
                            <li><span><i class="fas fa-angle-right"></i></span>Availability in India, Nepal, Bangladesh, and Sri Lanka</li>
                        </ul>
                        <p>Want to be part of the booming vaping industry? Contact us at care@vapestop.in for wholesale opportunities.</p>
                    </div>
                </div>
            </div>
        </div>
        <!--/ cartr checkout page-->
    </section>
    <!--/main Ends-->

@endsection
@section('footerScripts')
@endsection