@extends('frontend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')

    <!--main Starts-->
    <section class="main">
        <!-- product overview header -->
        <section class="productview-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <!-- brudcrumb -->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb nobg mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Careers</li>
                            </ol>
                        </nav>
                        <!--/ brudcrumb -->
                    </div>
                </div>
            </div>
        </section>
        <!--/ product overview header -->
        <!-- cart checkout pages -->
        <div class="account">
            <div class="container mb-5">
                <div class="row py-5">
                    <div class="col-lg-12">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aspernatur consequuntur cumque et eum iste laboriosam officia similique ullam. Dolorem laborum minima nihil nobis, quaerat quas quasi totam vero vitae.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aspernatur consequuntur cumque et eum iste laboriosam officia similique ullam. Dolorem laborum minima nihil nobis, quaerat quas quasi totam vero vitae.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aspernatur consequuntur cumque et eum iste laboriosam officia similique ullam. Dolorem laborum minima nihil nobis, quaerat quas quasi totam vero vitae.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aspernatur consequuntur cumque et eum iste laboriosam officia similique ullam. Dolorem laborum minima nihil nobis, quaerat quas quasi totam vero vitae.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aspernatur consequuntur cumque et eum iste laboriosam officia similique ullam. Dolorem laborum minima nihil nobis, quaerat quas quasi totam vero vitae.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aspernatur consequuntur cumque et eum iste laboriosam officia similique ullam. Dolorem laborum minima nihil nobis, quaerat quas quasi totam vero vitae.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aspernatur consequuntur cumque et eum iste laboriosam officia similique ullam. Dolorem laborum minima nihil nobis, quaerat quas quasi totam vero vitae.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aspernatur consequuntur cumque et eum iste laboriosam officia similique ullam. Dolorem laborum minima nihil nobis, quaerat quas quasi totam vero vitae.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aspernatur consequuntur cumque et eum iste laboriosam officia similique ullam. Dolorem laborum minima nihil nobis, quaerat quas quasi totam vero vitae.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aspernatur consequuntur cumque et eum iste laboriosam officia similique ullam. Dolorem laborum minima nihil nobis, quaerat quas quasi totam vero vitae.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aspernatur consequuntur cumque et eum iste laboriosam officia similique ullam. Dolorem laborum minima nihil nobis, quaerat quas quasi totam vero vitae.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aspernatur consequuntur cumque et eum iste laboriosam officia similique ullam. Dolorem laborum minima nihil nobis, quaerat quas quasi totam vero vitae.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aspernatur consequuntur cumque et eum iste laboriosam officia similique ullam. Dolorem laborum minima nihil nobis, quaerat quas quasi totam vero vitae.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aspernatur consequuntur cumque et eum iste laboriosam officia similique ullam. Dolorem laborum minima nihil nobis, quaerat quas quasi totam vero vitae.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!--/ cartr checkout page-->
    </section>
    <!--/main Ends-->

@endsection
@section('footerScripts')
@endsection