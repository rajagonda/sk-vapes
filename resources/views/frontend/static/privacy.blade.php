@extends('frontend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')

    <!--main Starts-->
    <section class="main">
        <!-- product overview header -->
        <section class="productview-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <!-- brudcrumb -->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb nobg mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Privacy</li>
                            </ol>
                        </nav>
                        <!--/ brudcrumb -->
                    </div>
                </div>
            </div>
        </section>
        <!--/ product overview header -->
        <!-- cart checkout pages -->
        <div class="account">
            <div class="container mb-5 stpage">
                <div class="row py-5">
                    <div class="col-lg-12">
                        <h5>Privacy Policy</h5>
                        <p>We are committed to protecting and respecting the privacy of our customers. Please read below about how we may collect and use customer information when visiting www.vapestop.in website. We will keep information confidential in accordance with all legal requirements. All personal information is stored on our secure servers.</p>
                        <p>Information we collect about you (customer) includes information you submit online while registering an account including your billing and mailing address, name, email id, any content you send to us, details of website navigation, details of transactions of purchase.</p>
                        <p>We will use your information to improve the content and navigation of the website, inform any changes to the website, quality control and analysis to provide new features and services, share marketing collateral and other marketing material, contact via telephone, email, or post confirming purchase or otherwise unless you specify to use that you do not want to be contacted, share news of products and services which may be of your interest. Verify your age and location as products are only for 18+ years old in permitted states and the information is used only for billing and delivery purpose.</p>

                        <p>You can contact us with any queries or questions with regards to our privacy policy.</p>

                        <h5>INCORRECT DELIVERY ADDRESS:</h5>
                        <p>It is the responsibility of the customer to enter the correct delivery address for products. If you have entered an incorrect mailing address, then please get back to us as soon as possible, and if the package has not left for delivery, we will edit the mailing address.</p>

                        <h5>LIMITATION OF LIABILITY:</h5>
                        <p>Sk Vapes is not liable to any loss or damage caused wholly or mainly by your breach of our Terms & Conditions. We hereby disclaim any and all liability for personal injury, product defect and/ or failure, claims that arise that are due to normal usage, misuse of products, abuse of products, product modifications, improper product selection. Our liability shall not in any event include losses related to any business of a customer including but not limited to lost profits or business interruption.</p>

                    </div>
                </div>

            </div>
        </div>
        <!--/ cartr checkout page-->
    </section>
    <!--/main Ends-->

@endsection
@section('footerScripts')
@endsection