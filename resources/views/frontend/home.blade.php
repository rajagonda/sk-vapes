@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
    <style>

        .outofstack {
            position: absolute;
            z-index: 99;
            top: 12px;
            left: 0;
            width: 100%;
            text-align: center;
        }

    </style>
@endsection
@section('content')
    <!--main Starts-->
    <section class="main">
        @if(count($banners)>0)
            <div id="homebanner" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    @foreach($banners as $banner)
                        <li data-target="#homebanner" data-slide-to="{{($loop->iteration-1)}}"
                            class="{{ $loop->iteration==1?'active':'' }}"></li>
                    @endforeach
                </ol>
                <div class="carousel-inner">
                    @foreach($banners as $banner)
                        <div class="carousel-item {{ $loop->iteration==1?'active':'' }}">
                            <img class="d-block w-100" src="/uploads/banners/{{ $banner->banner_image }}"
                                 alt="{{ $banner->banner_title }}">
                            <div class="carousel-caption d-none d-md-block w-100"
                                 style="bottom: 0%; left: 0;margin: auto;background-color: rgba(0,0,0,0.5); height: 100vh; padding: 30% 20% 0% 20%">
                                <h3 style="font-size: xx-large; margin-bottom: 20px">{{ $banner->banner_title }}</h3>
                                <p>{{ $banner->banner_description }}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
                <a class="carousel-control-prev" href="#homebanner" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#homebanner" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        @endif
        @if($brands)
        <!-- categories-->
            <section class="homecategories sectionpadding">
                <div class="container">
                    {{--                    {{ g_print($brands) }}--}}
                    <div class="row">
                        @foreach($brands as $brand)
                            {{--                            {!! dd($brand) !!}--}}
                            <div class="
                            @if($loop->iteration==1)
                                    col-lg-5 @elseif ($loop->iteration==2)
                                    col-lg-7
@else
                                    col-lg-4
@endif
                                    text-center">
                                <div class="catcol">
                                    <figure class="position-relative"><img
                                                src="/uploads/brands/{{ $brand->brand_image }}"
                                                class="img-fluid w-100 object-fit-cover" title="" alt=""></figure>
                                    <article class="catarticle position-absolute w-100">
                                        <h3 class="fwhite">{{  $brand->brand_homepage_title !='' ? $brand->brand_homepage_title : $brand->brand_name }}</h3>
                                        {{--<a--}}
                                        {{--class="btn fwhite custombtn mt-2"--}}
                                        {{--href="{{ route('userproducts',['category'=>$brand->getCategory->category_alias,'brand'=>$brand->brand_alias]) }}">View</a>--}}
                                    </article>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </section>
            <!--/ categories -->
        @endif
        @if($best_products)
            <section class="graybg sectionpadding bestproducts">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 text-center ">
                            <h2 class="h2 flight pb-4">Best Products</h2>
                        </div>
                    </div>
                @if(count($best_products)>0)
                    <!-- ELIQUIDS-->
                        <div class="row">
                            @foreach($best_products as $product)
                                <div class="col-lg-3 col-sm-6 filtr-item text-center">
                                    <div class="productcol">
                                        <figure>
                                            <a href="{{ route('productDetails',['product_alias'=>$product->product_alias]) }}" class="{{ ($product->product_availability != 'in_stock')?'outofstack':''  }}_box">
                                                @php
                                                    if($product->product_image){
                                                    $ptname=$product->product_image;
                                                    }else{
                                                     if (!empty(getProductImage($product->product_id))){
                                                      $ptname=getProductImage($product->product_id)->pi_image_name;
                                                     }else{
                                                     $ptname='';
                                                     }
                                                    }
                                                @endphp
                                                @if (!empty($ptname))
                                                    <img src="/uploads/products/thumbs/{{ $ptname }}" alt=""
                                                         class="img-fluid object-fit-cover productimg" title="">
                                                @else
                                                    <img src="https://via.placeholder.com/300x300.png?text=No Image"
                                                         alt="" class="img-fluid object-fit-cover productimg" title="">
                                                @endif

                                                @if ($product->product_availability != 'in_stock')
                                                    <div class="{{ ($product->product_availability != 'in_stock')?'outofstack':''  }}">
                                                        <img src="https://i.ibb.co/DCvbxN2/outofstack.png" class="img-fluid object-fit-cover"/>
                                                    </div>

                                                @endif


                                            </a>
                                        </figure>
                                        <article>
                                            <a class="fmed linkpro" href="">
                                                {{ $product->product_name }}
                                                {{--                                                {{ route('productDetails',['product_alias'=>$product->product_alias]) }}--}}
                                            </a>
                                            <p class="py-4 pricep"><i
                                                        class="fas fa-rupee-sign"></i><span
                                                        class="linethrough price">{{ $product->product_real_price }}</span>
                                                <span
                                                        class="price"><i
                                                            class="fas fa-rupee-sign"></i> {{ $product->product_price }}</span>
                                            </p>
                                            <p class="likekart">


                                                @if ($product->product_availability == 'in_stock')
                                                    <a href="{{ route('productDetails',['product_alias'=>$product->product_alias]) }}"
                                                       title="Add to Cart">
                                                        <span class="icon-online-shopping-cart moonfont"></span>
                                                    </a>
                                                @else

                                                    <span href="#" class="text-danger">
                                                        {{ availability($product->product_availability)  }}
                                                    </span>


                                                @endif




                                                {{--<a href="#" data-toggle="tooltip"  data-placement="bottom" data-id="{{ $product->product_id }}"--}}
                                                {{--title="Add to Cart" class="addToCart">--}}
                                                {{--<span class="icon-online-shopping-cart moonfont"></span>--}}
                                                {{--</a>--}}
                                                <a href="#" data-toggle="tooltip"
                                                   data-placement="bottom" data-id="{{ $product->product_id }}"
                                                   title="Add to Wishlist" data-userid="{{ Auth::id() }}"
                                                   class="productaddwishlist{{ $product->product_id }} addToWishList @if(in_array($product->product_id,$wishlistProducts))likeactive @endif">
                                                    <span class="icon-heart moonfont"></span>
                                                </a>
                                            </p>
                                        </article>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <div class="py-3 text-center"><p>Coming Soon</p></div>
                @endif
                <!--/ best products -->
                </div>
            </section>
        @endif
        @if(count($footerbanners)>0)
            <section class="sliderpro">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        @foreach($footerbanners as $bannerkey=>$footerbanner)
                            <div class="carousel-item @if($loop->iteration==1) active  @endif"
                                 style="background-image: url('/uploads/banners/{{ $footerbanner->banner_image }}')">
                                <div class="transbg"></div>
                                <div class="carousel-caption">
                                    <h3 class="fmed h3 pb-4">{{ $footerbanner->banner_title }}</h3>
                                    <a class="text-uppercase fwhite flight"
                                       href="{{ $footerbanner->banner_link }}">&nbsp; &nbsp; View
                                        &nbsp; &nbsp;</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <div class="transbg"></div>
                        <span class="carousel-control-prev-icon" aria-hidden="true"> <i class="fas fa-chevron-left"></i></span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"><i class="fas fa-chevron-right"></i></span>
                    </a>
                </div>
            </section>
        @endif
    </section>
@endsection
@section('footerScripts')
@endsection