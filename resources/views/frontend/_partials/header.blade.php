<header class="fixed-top">
    <!--topheader-->
    <div class="topheader">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 hidexs col-sm-6">
                    <p class="text-uppercase freg">{{ __('message.header.welcome') }}</p>
                </div>
                <div class="col-lg-6 col-sm-6">
                    <ul class="nav nav-pills topheadernav justify-content-end">
                        @auth
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="false">{!! Auth::user()->name !!} <i class="fas fa-angle-down"></i> </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="{{ route('userprofile') }}">My Account</a>
                                    <a class="dropdown-item" href="{{ route('orders') }}">My Orders</a>
                                    <a href="{{ route('userlogout') }}" class="dropdown-item"
                                       onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                                    <form id="logout-form" action="{{ route('userlogout') }}" method="POST"
                                          style="display: none;"> {{ csrf_field() }} </form>
                                </div>
                            </li>
                        @else
                            <li class="nav-item"><a class="nav-link" href="{{ route('userelogin') }}">{{ __('message.header.signin') }}</a>
                            </li>
                        @endauth

                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('cartDetails') }}"><span class="icon-online-shopping-cart moonfont"></span>
                                {{ __('message.header.cart') }}
                                (<span id="cartcontents">
                                    {{ Cart::content()->count() }}
                                </span>)
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
{{--                                {!! dump(App::getLocale()) !!}--}}
{{--                                {{  dd(getCountryByCode(App::getLocale())) }}--}}
                                {{!empty(App::getLocale()) ? getCountryByCode(App::getLocale())->country_name : getCountryByCode('in')->country_name }}
                                <i class="fas fa-angle-down"></i>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                @if(count($countries)>0)
                                    @foreach($countries as $code=>$country)
                                        <a class="dropdown-item" href="{{ url('/'.$code.'') }}">{{ $country }}</a>
                                    @endforeach
                                @endif
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--/ topheader-->
    <!--navigation-->
    <div class="headnav">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <nav class="mainnav navbar navbar-expand-md p-0">
                        <a class="navbar-brand" href="{{ route('home') }}"><img class="" src="/frontend/img/logo.png" alt="" title=""></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
                                aria-label="Toggle navigation"><i class="fas fa-bars"></i></button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            <ul class="navbar-nav ml-auto">

                                {{--<li class="nav-item">--}}
                                    {{--<a class="nav-link @if (Request::segment(1) == '') active @endif"--}}
                                       {{--href="{{ route('home') }}"><span>Home</span></a>--}}
                                {{--</li>--}}

{{--                                {!! dd(getCategories()) !!}--}}

                                @if(count(getCategories())>0)
                                    @foreach(getCategories() as $catkey=>$category)
                                        <li class="nav-item">
                                            <a class="nav-link @if (Request::segment(2) == $catkey) active @endif"
                                               href="{{ route('userproducts',['category'=>$catkey]) }}"><span>{{ $category }}</span></a>
                                        </li>
                                    @endforeach
                                @endif
                                <li class="nav-item">
                                    <a class="nav-link @if (Request::segment(2) == 'contact-us') active @endif" href="{{ route('support') }}"><span>Support</span></a>
                                </li>
                                <li class="nav-item dropdown ">
                                    <a class="nav-link dropdown-toggle navsearch" data-toggle="dropdown" href="#"
                                       aria-expanded="false"><i class="fas fa-search"></i></a>
                                    <div class="dropdown-menu searchdrop">
                                        <input type="text" id="searchinpt" placeholder="Search Keyword">
                                        {{--<input type="submit" value="Search"></div>--}}
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!--/navigation-->
</header>
