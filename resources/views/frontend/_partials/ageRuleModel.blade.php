<div class="modal" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog loadpop" role="document">
        <div class="modal-content">
            <form method="POST" id="saveRestrictions" action="{{ route('saveRestrictions') }}"
                  accept-charset="UTF-8" class="py-4" enctype="multipart/form-data">

                {{ csrf_field() }}

                <div class="modal-body text-center">
                    <img class="popbrand" src="/frontend/img/logo.png" alt="skvapes" title="skvapes" />
                    <h5 class="h3 py-3">Are You of Legal Smoking Age?</h5>
                    <p class="flight">The Product on this website are Intended for Adults only. By entering this
                        website, you certify that your are of legal smoking age in the state in which you
                        reside</p>

                    <div class="row pt-3 rowform">
                        <input type="hidden" name="country" value="in"/>
                        {{--<div class="col-lg-6">--}}
                        {{--<div class="form-group">--}}
                        {{--<select class="form-control" name="country">--}}
                        {{--<option value="">Select Country</option>--}}
                        {{--@if(count($countries)>0)--}}
                        {{--@foreach($countries as $code=>$country)--}}
                        {{--<option value="{{ $code }}">{{ $country }}</option>--}}
                        {{--@endforeach--}}
                        {{--@endif--}}
                        {{--</select>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        <div class="col-lg-3"></div>
                        {{--<div class="col-lg-6 text-center">--}}
                        {{--<div class="dtpick text-center">--}}
                        {{--<input type="text" placeholder="DD" name="dob_day" id="dob_day">--}}
                        {{--<input type="text" placeholder="MM" name="dob_month" id="dob_month">--}}
                        {{--<input type="text" placeholder="YYYY" name="dob_year" id="dob_year"></div>--}}
                        {{--</div>--}}
                        <div class="col-lg-3"></div>
                    </div>

                    <div class="ageerror" style="color: red"></div>

                </div>
                <div class="modal-footer text-center">
                    <div class="row justify-content-center w-100">
                        <div class="col-lg-10">
                            <div class="row justify-content-center w-100">
                                <div class="col-lg-6">
                                    <button type="button" class="ageconfirmNo my-2   text-uppercase flight signbtn ">
                                        NO, I'M UNDER 18
                                    </button>
                                </div>
                                <div class="col-lg-6">
                                    <button type="submit" class=" my-2  text-uppercase flight  signbtn ">
                                        YES, I'M 18 OR OLDER
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>