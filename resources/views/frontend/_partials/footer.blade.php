<footer>
    <!-- features -->
    <section class="featuresfooter">
        <div class="container">
            <div class="row">

                <div class="col-sm-6 featfootcol">
                    <figure class="float-left "><img src="/frontend/img/securepayment.png" alt="" title=""></figure>
                    <article>
                        <p class="fmed">100% Secure Payment</p>
                        <p class="flgray flight">We Supports Cards, Wallets</p>
                    </article>
                </div>
                <div class="col-lg-4 col-sm-6 featfootcol">
                    <figure class="float-left"><img src="/frontend/img/supporticon.png" alt="" title=""></figure>
                    <article>
                        <p class="fmed">Support</p>
                        <p class="flgray flight">(Mon - Sat 11:00 PM to 7:00 PM)</p>
                    </article>
                </div>
            </div>
        </div>
    </section>
    <!--/ features -->
    <!--news letters footer-->
    <section class="nletterfooter">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 newslettercol">
                    <div class="row">
                        <div class="col-lg-6">
                            <article class="float-left">
                                <h5 class="text-uppercase fbold py-2">Let's Stay in Touch</h5>
                                <p>Get updates on sales specials and more </p>
                            </article>
                        </div>
                        <div class="col-lg-6">
                            <form class="newsform w-100 mt-2" action="{{ route('saveSubscribers') }}"
                                  id="saveSubscribers">

                                <div class="row no-gutters">
                                    <div class="col-md-11 col-sm-11 col-11">
                                        <input class="w-100 px-2 py-1" type="text" placeholder="Enter Email Address"
                                               name="nl_email"></div>
                                    <div class="col-md-1 col-sm-1 col-1">
                                        <input class="btnsubscribe" type="submit" value="">
                                    </div>
                                    <div class="subscribersformStatus">
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-6">
                            <article class="float-left pl50">
                                <h5 class="text-uppercase fbold py-2">Follow</h5>
                                <p>We Want to hear from you! </p>
                            </article>
                        </div>
                        <div class="col-lg-6">
                            <ul class="nav footersocialnav pt-3">
                                <li class="nav-item">
                                    <a href="https://www.facebook.com/skvape.hyd/" class="nav-link" target="_blank">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://www.instagram.com" class="nav-link" target="_blank">
                                        <i class="fab fa-instagram"></i></a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://twitter.com/Skvapes" class="nav-link" target="_blank">
                                        <i class="fab fa-twitter"></i></a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://www.youtube.com/channel/UCHMFypTQfmTkH_pMaV-CJFg" class="nav-link" target="_blank">
                                        <i class="fab fa-youtube"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ news letters footer-->
    <!--clients logos-->
    <section class="footerclients">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-sm-4 text-center align-self-center">
                    <img class="img-fluid"
                         src="/frontend/img/data/footerclient01.png"
                         alt="" title="">
                </div>
                <div class="col-lg-2 col-sm-4 text-center align-self-center">
                    <img class="img-fluid"
                         src="/frontend/img/data/footerclient02.png"
                         alt="" title="">
                </div>
                <div class="col-lg-2 col-sm-4 text-center align-self-center">
                    <img class="img-fluid"
                         src="/frontend/img/data/footerclient03.png"
                         alt="" title="">
                </div>
                <div class="col-lg-2 col-sm-4 text-center align-self-center">
                    <img class="img-fluid"
                         src="/frontend/img/data/footerclient04.png"
                         alt="" title="">
                </div>
                <div class="col-lg-2 col-sm-4 text-center align-self-center">
                    <img class="img-fluid"
                         src="/frontend/img/data/footerclient05.png"
                         alt="" title="">
                </div>
                <div class="col-lg-2 col-sm-4 text-center align-self-center">
                    <img class="img-fluid"
                         src="/frontend/img/data/footerclient06.png"
                         alt="" title="">
                </div>
            </div>
        </div>
    </section>
    <!--/clients logos-->
    <section class="footernav">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <article class="pb-2">
                        <h6 class="h6 fwhite fbold text-uppercase pb-3">About us</h6>
                        <p>Our Stance on Electronic Cigarettes Many people, who have successfully quit, have done so
                            with the help of electronic cigarettes.</p>
                    </article>
                </div>
                <div class="col-lg-9 col-sm-12">
                    <div class="row">
                        <div class="col-lg-3 col-sm-3">
                            <h6 class="h6 fwhite fbold text-uppercase pb-3">Quick Link</h6>
                            @if(count(getCategories())>0)
                                <ul class="footnav">
                                    @foreach(getCategories() as $catkey=>$category)
                                        <li>
                                            <a href="{{ route('userproducts',['category'=>$catkey]) }}">{{ strtolower($category) }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                        <div class="col-lg-3 col-sm-3">
                            <h6 class="h6 fwhite fbold text-uppercase pb-3">Support</h6>
                            <ul class="footnav">
                                <li><a href="{{ route('about') }}">About Us</a></li>
                                <li><a href="{{ route('faq') }}">FAQ</a></li>
                                <li><a href="{{ route('warranty') }}">Warranty</a></li>
                                <li><a href="{{ route('WholesaleOpportunity') }}">Wholesale Opportunity</a></li>
                                <li><a href="{{ route('support') }}">Support</a></li>
                                <li><a href="{{ route('terms') }}">terms</a></li>
                                <li><a href="{{ route('privacy') }}">privacy</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-3">
                            <h6 class="h6 fwhite fbold text-uppercase pb-3">Contact Us</h6>
                            <ul class="footnav">
                                <li><a href="{{ route('support') }}">Email</a></li>
                                <li><a href="{{ route('careers') }}">Careers</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-3">
                            <h6 class="h6 fwhite fbold text-uppercase pb-3"> +91 912 133 3383</h6>
                            <ul class="footnav">
                                <li>
                                    <p class="fmed">Service Hours:</p>
                                    <p>Monday - Saturday </p>
                                    <p>11:00 AM To 7:00 PM</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--copy rights -->
        <section class="copyrights">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <p>Copyright © 2018 . All Rights Reserved</p>
                    </div>
                    <div class="col-lg-6 text-right">
                        <div class="dropup">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">Change Location </a>
                            <div class="dropdown-menu">
                                @if(count($countries)>0)
                                    @foreach($countries as $code=>$country)
                                        <a class="dropdown-item" href="{{ url('/'.$code.'') }}">{{ $country }}</a>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/ copy rights --><a id="movetop" class="movetop" href="#"><i
                    class="fas fa-arrow-up"></i></a>
    </section>
</footer>
