<!--script files-->
<script type="text/javascript" src="/frontend/assets/js/main.js"></script>
<script type="text/javascript">




    //popover and tooltip script
    $(function () {

        //add class to onscroll  to header to fix top
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $(".fixed-top").addClass("fixed-theme");
            } else {
                $(".fixed-top").removeClass("fixed-theme");
            }
        });
        $.validator.addMethod("check_date_of_birth", function (value, element) {

            var day = $("#dob_day").val();
            var month = $("#dob_month").val();
            var year = $("#dob_year").val();
            var age = 21;

            var mydate = new Date();
            mydate.setFullYear(year, month - 1, day);

            var currdate = new Date();
            currdate.setFullYear(currdate.getFullYear() - age);

            console.log(currdate > mydate);
            return currdate > mydate;

        });

        function skvapes_underage() {
            if (confirm('Intended for sale to adults 21 years or older. If you are not legally able to purchase tobacco products in the state where you live, please do not enter this site.')) {
                window.opener = null;
                window.open('', '_self');
                window.close();
                open('http://www.google.com', '_self').close();
            }
        }
        $('.ageconfirmNo').on('click', function () {
            skvapes_underage()
        });

        $("#saveRestrictions").validate({
            rules: {
                // country: {
                //     required: true
                // },
                // dob_year: {
                //     required: true,
                //     check_date_of_birth: true
                // }
            },
            messages: {
                // dob_year: {
                //     required: 'Please enter Year',
                //     check_date_of_birth: 'Online Purchaser Must Have 21 and Above age.'
                // }
            },
            submitHandler: function (form) {
                console.log($(form).serialize());

                $.ajax({
                    type: "POST",
                    url: "{{ route('saveRestrictions') }}",
                    data: $(form).serialize(),
                    success: function (response) {
                        console.log(response.status);

                        if (response.status == 1) {
                            $('#myModal').modal('toggle');
                        } else {
                            $('.ageerror').html('Online Purchaser Must Have 21 and Above age .')
                            // $('.ageerror').html('Your Age is ' + response.age + ' Years. Online Purchaser Must Have 18+ Above.')
                        }
                    }
                });
                return false; // required to block normal submit since you used ajax
            }
        });
        $('#myModal').modal({
            backdrop: 'static',
            keyboard: false
        });
        $('.datepicker').datepicker({
                dateFormat: 'dd-mm-yy',
                changeMonth: true,
                changeYear: true,
                yearRange: "-73:+0",
            }
        );
        $('#saveSubscribers').validate({
            ignore: [],
            errorClass: 'help-block animation-slideDown text-danger', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',
            rules: {
                nl_email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                nl_email: {
                    required: 'Please enter email'
                }
            },
            submitHandler: function (form) {
                $('.subscribersformStatus').html('');
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: "{{ route('saveSubscribers') }}",
                    data: $(form).serialize(),
                    success: function (response) {
                        if (response.status == 1) {
                            $('#saveSubscribers')[0].reset();
                            $('.subscribersformStatus').html('<div class="alert alert-success">\n' +
                                '    <a href="#" class="close" data-dismiss="alert">&times;</a>\n' +
                                '    <strong>Success!</strong> Your message has been sent successfully.\n' +
                                '</div>');
                        } else {
                            $('.subscribersformStatus').html('<div class="alert alert-danger">\n' +
                                '    <a href="#" class="close" data-dismiss="alert">&times;</a>\n' +
                                '    <strong>' + response.errors + '</strong>\n' +
                                '</div>');
                        }
                    }
                });
                return false; // required to block normal submit since you used ajax
            }
        });
        $('#support').validate({
            ignore: [],
            errorClass: 'help-block animation-slideDown text-danger', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',
            rules: {
                name: {
                    required: true
                },
                phone: {
                    required: true,
                    number: true
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                name: {
                    required: 'Please enter name'
                },
                phone: {
                    required: 'Please enter phone',
                    number: 'Please enter a valid phone number'
                },
                email: {
                    required: 'Please enter email'
                }
            },
            submitHandler: function (form) {
                $('.contactusformStatus').html('');
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: "{{ route('support') }}",
                    data: $(form).serialize(),
                    success: function (response) {
                        if (response.status == 1) {
                            $('#support')[0].reset();
                            $('.contactusformStatus').html('<div class="alert alert-success">\n' +
                                '    <a href="#" class="close" data-dismiss="alert">&times;</a>\n' +
                                '    <strong>Success!</strong> Your message has been sent successfully.\n' +
                                '</div>');
                        } else {
                            $('.contactusformStatus').html('<div class="alert alert-danger">\n' +
                                '    <a href="#" class="close" data-dismiss="alert">&times;</a>\n' +
                                '    <strong>' + response.errors + '</strong>\n' +
                                '</div>');
                        }
                    }
                });
                return false; // required to block normal submit since you used ajax
            }
        });
        $('[data-toggle="popover"]').popover({
            html: true,
            content: function () {
                return $('#popover-content').html();
            }
        });
        $('[data-toggle="tooltip"]').tooltip()

        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#movetop').fadeIn('fast');
            } else {
                $('#movetop').fadeOut('fast');
            }
        });

        // Add to cart

        $('.addToCart').on('click', function () {
            var id = $(this).data('id');
            var id = $(this).data('id');
            var qty = 1;
            if (id != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '{{ route('addToCart') }}',
                    type: 'POST',
                    // data: {'id': id, 'qty': 1},
                    data: 'id=' + id + '&qty=' + qty,
                    success: function (response) {
                        $('#cartcontents').text(response);
                        notifications('Item added to the cart');
                    }
                });
            }
        });
        $('.addToCartInProduct').on('click', function () {
            var id = $(this).data('id');
            var qty = $('.productqty').val();
            var color = $('.selectcolor').val();

            var size = $('.selectsize').val();

            if( typeof size === 'undefined' || size === null ){
                var size= '';
            }
            if (id != '' && qty != 0) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '{{ route('addToCart') }}',
                    type: 'POST',
                    // data: {'id': id, 'qty': 1},
                    data: 'id=' + id + '&qty=' + qty + '&color=' + color + '&size=' + size,
                    success: function (response) {
                        $('#cartcontents').text(response);
                        notifications('Item added to the cart');
                    }
                });
            }
        });

        // Add to cart

        $('.addToWishList').on('click', function () {
            var id = $(this).data('id');
            var userid = $(this).data('userid');
            if (userid) {
                if (id != '') {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                        url: '{{ route('addToWishList') }}',
                        type: 'POST',
                        // data: {'id': id, 'qty': 1},
                        data: 'id=' + id,
                        success: function (response) {
                            if (response == 1) {
                                $('.productaddwishlist' + id).addClass("likeactive");
                                notifications('Item added from wishlist');
                            } else if (response == 2) {
                                $('.productaddwishlist' + id).removeClass("likeactive");
                                notifications('Removed from wishlist');
                            } else {
                                alert("Try again");
                            }
                        }
                    });
                }
            } else {
                alert("Please login to add products to wishlist");
            }

        });
        $('.removeCartItem').on('click', function () {
            var id = $(this).data('id');

            if (id != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '{{ route('removeItemFromCart') }}',
                    type: 'POST',
                    data: 'id=' + id,
                    success: function (response) {
                        location.reload();
                    }
                });
            }
        });

        $('.updateCartItem').on('click', function () {
            var id = $(this).data('id');
            var qty = $('.itemQty' + id).val();

            if (id != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '{{ route('updateItemFromCart') }}',
                    type: 'POST',
                    data: 'id=' + id + '&qty=' + qty,
                    success: function (response) {
                        location.reload();
                    }
                });
            }
        });

    });
    function notifications($title) {
        $.notify({
            // options
            icon: 'glyphicon glyphicon-warning-sign',
            title: $title,
            message: '',

        }, {
            type: "success",
            allow_dismiss: true,
            placement: {
                from: "top",
                align: "center"
            },
            delay: 500,
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            }
        });
    }

    $(window).on('load', function () {
        $('#myModal').modal('show');
    });
    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $(".fixed-top").addClass("fixed-theme");
        } else {
            $(".fixed-top").removeClass("fixed-theme");
        }
    });

    $(document).on('click', '#movetop', function () {
        $("html").animate({
            scrollTop: 0
        }, 500);
        return false;
    });

    //Horizontal Tab
    $('#parentHorizontalTab').easyResponsiveTabs({
        type: 'default', //Types: default, vertical, accordion
        width: 'auto', //auto or any width like 600px
        fit: true, // 100% fit in a container
        tabidentify: 'hor_1', // The tab groups identifier
        activate: function (event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#nested-tabInfo');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
        }
    });
    //vertical tab plug addon
    $('#parentVerticalTab').easyResponsiveTabs({
        type: 'vertical', //Types: default, vertical, accordion
        width: 'auto', //auto or any width like 600px
        fit: true, // 100% fit in a container
        closed: 'accordion', // Start closed if in accordion view
        tabidentify: 'hor_1', // The tab groups identifier
        activate: function (event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#nested-tabInfo2');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
        }
    });
    //input file
    document.querySelector("html").classList.add('js');
    var fileInput = document.querySelector(".input-file"),
        button = document.querySelector(".input-file-trigger"),
        the_return = document.querySelector(".file-return");

</script>
