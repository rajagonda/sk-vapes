@extends('backend.layout')
@section('title', $title)

@section('headerStyles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
@endsection

@section('content')
    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'brands'=>'Brands',
               ''=>'Add New'
               ),'Add New Brand'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        {{--                        {!!  dump($brand)  !!}--}}

                        <div class="card-body card-block">

                            <form method="POST" id="brands" action="{{ route('addNewBrands') }}"
                                  accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="brand_id" value="{{ $brand_id }}">
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class="form-control-label">Country</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="brand_country" id="brand_country" class="form-control">
                                            <option value="">Select</option>
                                            <?php
                                            $countries = getCountries(); ?>
                                            @foreach($countries as $ks=>$s)
                                                <option value="{{ $ks }}" {{ (!empty(old('brand_country')) && old('brand_country')==$ks)  ? 'selected' : ((($brand) && ($brand->brand_country == $ks)) ? 'selected' : '') }}>
                                                    {{ $s }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('brand_country'))
                                            <span class="text-danger help-block">{{ $errors->first('brand_country') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class="form-control-label">category</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <?php
                                        $categories = getCategoriesByID();

                                        if ($brand) {
                                            $selectedids = explode(',', $brand->brand_catid);

                                        }

                                        //                                       dump($selectedids)
                                        ?>

                                        <select name="brand_catid[]" id="brand_catid" multiple
                                                class="form-control select2">
                                            <option value="">Select</option>

                                            @foreach($categories as $ks=>$s)
                                                <option value="{{ $ks }}" {{ (!empty(old('brand_catid')) && old('brand_catid')==$ks)  ? 'selected' : ((($brand && is_array($selectedids)) && (in_array($ks, $selectedids))) ? 'selected' : '') }}>
                                                    {{ $s }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('brand_country'))
                                            <span class="text-danger help-block">{{ $errors->first('brand_country') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Name</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="brand_name" type="text"
                                               placeholder="Brand name"
                                               name="brand_name"
                                               value="{{ !empty(old('brand_name')) ? old('brand_name') : ((($brand) && ($brand->brand_name)) ? $brand->brand_name : '') }}">
                                        @if ($errors->has('brand_name'))
                                            <span class="text-danger help-block">{{ $errors->first('brand_name') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Home Page Title</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="brand_homepage_title" type="text"
                                               placeholder="Home Page Title"
                                               name="brand_homepage_title"
                                               value="{{ !empty(old('brand_homepage_title')) ? old('brand_homepage_title') : ((($brand) && ($brand->brand_homepage_title)) ? $brand->brand_homepage_title : '') }}">
                                        @if ($errors->has('brand_homepage_title'))
                                            <span class="text-danger help-block">{{ $errors->first('brand_homepage_title') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Order number</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="brand_order" type="text"
                                               placeholder="Order number"
                                               name="brand_order"
                                               value="{{ !empty(old('brand_order')) ? old('brand_order') : ((($brand) && ($brand->brand_order)) ? $brand->brand_order : '') }}">
                                        @if ($errors->has('brand_order'))
                                            <span class="text-danger help-block">{{ $errors->first('brand_order') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Set Future</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="brand_future" type="checkbox"
                                               placeholder="Order number"
                                               name="brand_future"
                                               value="1" {{ !empty(old('brand_future')) ? old('brand_future') : ((($brand) && ($brand->brand_future)) ? 'checked' : '') }}>
                                        @if ($errors->has('brand_future'))
                                            <span class="text-danger help-block">{{ $errors->first('brand_future') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class="form-control-label">Brand Image</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control {{ (($brand) && ($brand->brand_image)) ? '' : 'brandimage'}}"
                                               id="brandimage" type="file" name="brand_image"/>
                                        <small>Size: 500/500</small>
                                        @if ($errors->has('brand_image'))
                                            <span class="text-danger">{{ $errors->first('brand_image') }}</span>
                                        @endif

                                        @if(($brand) && ($brand->brand_image))
                                            <div class="imagediv{{ $brand->brand_id }}">
                                                <img width="100" src="/uploads/brands/thumbs/{{$brand->brand_image}}"/>
                                            </div>
                                        @endif

                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Status</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="brand_status" id="brand_status" class="form-control">
                                            <?php
                                            $status = allStatuses('general'); ?>
                                            @foreach($status as $ks=>$s)
                                                <option value="{{ $ks }}" {{ (!empty(old('brand_status')) && old('brand_status')==$ks)  ? 'selected' : ((($brand) && ($brand->brand_status == $ks)) ? 'selected' : '') }}
                                                >
                                                    {{ $s }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('brand_status'))
                                            <span class="text-danger help-block">{{ $errors->first('brand_status') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-danger btn-sm">
                                        <i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

    <script src="{{url('backend/modules/brands/js/brands.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        $(function () {

            $('.select2').select2()
        })
    </script>

@endsection