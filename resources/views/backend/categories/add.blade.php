@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')
    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'categories'=>'Categories',
               ''=>'Add New'
               ),'Add New Category'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="categories" action="{{ route('addNewCategories') }}"
                                  accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="category_id" value="{{ $category_id }}">
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Category Name</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="category_name" type="text"
                                               placeholder="Category Name"
                                               name="category_name"
                                               value="{{ !empty(old('category_name')) ? old('category_name') : ((($category) && ($category->category_name)) ? $category->category_name : '') }}">
                                        @if ($errors->has('category_name'))
                                            <span class="text-danger help-block">{{ $errors->first('category_name') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Category Order</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="category_order" type="number" min="0" max="100"
                                               placeholder="Category order"
                                               name="category_order"
                                               value="{{ !empty(old('category_order')) ? old('category_order') : ((($category) && ($category->category_order)) ? $category->category_order : 0) }}">
                                        @if ($errors->has('category_order'))
                                            <span class="text-danger help-block">{{ $errors->first('category_order') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class="form-control-label">Category Image</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control {{ (($category) && ($category->category_image)) ? '' : 'categoryimage'}}"
                                               id="categoryimage" type="file" name="category_image" />
                                        <small>Size: 1400/500</small>
                                        @if ($errors->has('category_image'))
                                            <span class="text-danger">{{ $errors->first('category_image') }}</span>
                                        @endif
                                        @if(($category) && ($category->category_image))
                                            <div class="imagediv{{ $category->category_id }}">
                                                <img width="150"
                                                     src="/uploads/categories/thumbs/{{$category->category_image}}"/>
                                                <a href="#" id="{{$category->category_id}}"
                                                   class="delete_category_image btn btn-danger btn-xs"
                                                   onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                                            class="fa fa-trash-o" aria-hidden="true"></i>Delete </a>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Status</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="category_status" id="category_status" class="form-control">
                                            <?php
                                            $status = allStatuses('general'); ?>
                                            @foreach($status as $ks=>$s)
                                                <option value="{{ $ks }}" {{ (!empty(old('category_status')) && old('category_status')==$ks)  ? 'selected' : ((($category) && ($category->category_status == $ks)) ? 'selected' : '') }}
                                                >
                                                    {{ $s }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('category_status'))
                                            <span class="text-danger help-block">{{ $errors->first('category_status') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-danger btn-sm">
                                        <i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

    <script src="{{url('backend/modules/categories/js/categories.js')}}"></script>

@endsection