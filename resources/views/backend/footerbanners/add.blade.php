@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')
    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'footerBanners'=>'Footer Banners',
               ''=>'Add New'
               ),'Add New Footer Banner'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="banners" action="{{ route('addNewFooterBanners') }}"
                                  accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="banner_id" value="{{ $banner_id }}">
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class="form-control-label">Country</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="banner_country" id="banner_country" class="form-control">
                                            <option value="">Select</option>
                                            <?php
                                            $countries = getCountries(); ?>
                                            @foreach($countries as $ks=>$s)
                                                <option value="{{ $ks }}" {{ (!empty(old('banner_country')) && old('banner_country')==$ks)  ? 'selected' : ((($banner) && ($banner->banner_country == $ks)) ? 'selected' : '') }}>
                                                    {{ $s }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('banner_country'))
                                            <span class="text-danger help-block">{{ $errors->first('banner_country') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Banner Title</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="banner_title" type="text"
                                               placeholder="Banner Title"
                                               name="banner_title"
                                               value="{{ !empty(old('banner_title')) ? old('banner_title') : ((($banner) && ($banner->banner_title)) ? $banner->banner_title : '') }}">
                                        @if ($errors->has('banner_title'))
                                            <span class="text-danger help-block">{{ $errors->first('banner_title') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Banner Link</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="banner_link" type="text"
                                               placeholder="Banner Link"
                                               name="banner_link"
                                               value="{{ !empty(old('banner_link')) ? old('banner_link') : ((($banner) && ($banner->banner_link)) ? $banner->banner_link : '') }}">
                                        @if ($errors->has('banner_link'))
                                            <span class="text-danger help-block">{{ $errors->first('banner_link') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class="form-control-label">Banner Image</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control {{ (($banner) && ($banner->banner_image)) ? '' : 'bannerimage'}}"
                                               id="bannerimage" type="file" name="banner_image"/>
                                        <small>Size: 1400/500</small>
                                        @if ($errors->has('banner_image'))
                                            <span class="text-danger">{{ $errors->first('banner_image') }}</span>
                                        @endif
                                    </div>
                                </div>
                                @if(($banner) && ($banner->banner_image))
                                    <div class="imagediv{{ $banner->banner_id }}">
                                        <img src="/uploads/banners/thumbs/{{$banner->banner_image}}"/>
                                        {{--<a href="#" id="{{$banner->banner_id}}"--}}
                                        {{--class="delete_banner_image btn btn-danger btn-xs"--}}
                                        {{--onclick="return confirm(&quot;Confirm delete?&quot;)"><i--}}
                                        {{--class="fa fa-trash-o" aria-hidden="true"></i>Delete </a>--}}
                                    </div>
                                @endif
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Status</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="banner_status" id="banner_status" class="form-control">
                                            <?php
                                            $status = allStatuses('general'); ?>
                                            @foreach($status as $ks=>$s)
                                                <option value="{{ $ks }}" {{ (!empty(old('banner_status')) && old('banner_status')==$ks)  ? 'selected' : ((($banner) && ($banner->banner_status == $ks)) ? 'selected' : '') }}
                                                >
                                                    {{ $s }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('banner_status'))
                                            <span class="text-danger help-block">{{ $errors->first('banner_status') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-danger btn-sm">
                                        <i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

    <script src="{{url('backend/modules/banners/js/footerbanners.js')}}"></script>

@endsection