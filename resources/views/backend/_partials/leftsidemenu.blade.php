<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu"
                    aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            {{--<a class="navbar-brand" href="{{ route('dashboard') }}"><img src="/backend/images/logo.png" alt="Logo"></a>--}}
            {{--<a class="navbar-brand hidden" href="{{ route('dashboard') }}"><img src="/backend/images/logo.png"--}}
            {{--alt="Logo"></a>--}}
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="@if ($active_menu == 'dashboard')active @endif">
                    <a href="{{ route('dashboard') }}"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                </li>
                <li class="menu-item-has-children dropdown @if ($active_menu == 'countries')active @endif">
                    <a href="{{ route('countries') }}" class="" > <i class="menu-icon fa fa-table"></i>Countries</a>

                </li>
                <li class="menu-item-has-children dropdown @if ($active_menu == 'banners')active @endif">
                    <a href="{{ route('banners') }}" class="" > <i class="menu-icon fa fa-table"></i>Banners</a>

                </li>

                <li class="menu-item-has-children dropdown @if ($active_menu == 'brands')active @endif">
                    <a href="{{ route('brands') }}" class=""  > <i class="menu-icon fa fa-table"></i>Brands</a>

                </li>
                <li class="menu-item-has-children dropdown @if ($active_menu == 'categories')active @endif">
                    <a href="{{ route('categories') }}" class=""  > <i class="menu-icon fa fa-table"></i>Categories</a>

                </li>
                <li class="menu-item-has-children dropdown @if ($active_menu == 'products')active @endif">
                    <a href="{{ route('products') }}" class=""  > <i class="menu-icon fa fa-table"></i>Products</a>

                </li>

                <li class="menu-item-has-children dropdown @if ($active_menu == 'users')active @endif">
                    <a href="{{ route('users') }}" class=""  > <i class="menu-icon fa fa-table"></i>Users</a>

                </li>
                <li class="menu-item-has-children dropdown @if ($active_menu == 'footerBanners')active @endif">
                    <a href="{{ route('footerBanners') }}" class="" > <i class="menu-icon fa fa-table"></i>Footer Banners</a>

                </li>

                <li class="@if ($active_menu == 'orders')active @endif">
                    <a href="{{ route('adminorders') }}"> <i class="menu-icon fa fa-dashboard"></i>Orders</a>
                </li>

                <li class="@if ($active_menu == 'newsletter')active @endif">
                    <a href="{{ route('newsLetters') }}"> <i class="menu-icon fa fa-dashboard"></i>News Letters </a>
                </li>
               <li class="@if ($active_menu == 'contactus')active @endif">
                    <a href="{{ route('contactus') }}"> <i class="menu-icon fa fa-dashboard"></i>Contact Us</a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside><!-- /#left-panel -->
