@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')

    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               ''=>'Change Password'
               ),'Change Password'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">

            <div class="row">
                <div class="col-md-12">

                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                        @if (Session::has('flash_message_error'))
                        <br/>
                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message_error' ) }}</strong>
                        </div>
                    @endif

                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="changePassword" action="{{ route('changePassword') }}"
                                  accept-charset="UTF-8" class="form-horizontal">
                                {{ csrf_field() }}

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Old Password</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input class="form-control" type="password" placeholder="Old Password"
                                               name="password" id="password"
                                               value="{{ !empty(old('password')) ? old('password') : '' }}">
                                        @if ($errors->has('password'))
                                            <span class="text-danger help-block">{{ $errors->first('password') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">New Password</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input class="form-control" type="password" placeholder="New Password"
                                               name="new_password" id="new_password"
                                               value="{{ !empty(old('new_password')) ? old('new_password') : '' }}">
                                        @if ($errors->has('new_password'))
                                            <span class="text-danger help-block">{{ $errors->first('new_password') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Confirm Password</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input class="form-control" type="password" placeholder="Confirm Password"
                                               name="confirm_password" id="confirm_password"
                                               value="{{ !empty(old('confirm_password')) ? old('confirm_password') : '' }}">
                                        @if ($errors->has('confirm_password'))
                                            <span class="text-danger help-block">{{ $errors->first('confirm_password') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-danger btn-sm">
                                        <i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')
    <script src="{{url('backend/modules/users/js/changePassword.js')}}"></script>
@endsection