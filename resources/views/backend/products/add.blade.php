@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')
    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'products'=>'Products',
               ''=>'Add New'
               ),'Add New Product'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif

                    @include('backend.products.nav')
                    <div class="tab-content">
                        <div class="tab-pane fade show active">
                            <div class="card">

                                <div class="card-body card-block">

                                    <form method="POST" id="products" action="{{ route('addNewProducts') }}"
                                          accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="product_id" value="{{ $product_id }}">
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="hf-email" class="form-control-label">Country</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select name="product_country" id="product_country"
                                                        class="form-control">
                                                    <option value="">Select</option>
                                                    <?php
                                                    $countries = getCountries(); ?>
                                                    @foreach($countries as $ks=>$s)
                                                        <option value="{{ $ks }}" {{ (!empty(old('product_country')) && old('product_country')==$ks)  ? 'selected' : ((($product) && ($product->product_country == $ks)) ? 'selected' : '') }}>
                                                            {{ $s }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('product_country'))
                                                    <span class="text-danger help-block">{{ $errors->first('product_country') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="hf-email" class="form-control-label">Category</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select name="product_category" id="product_category"
                                                        class="form-control">
                                                    <option value="">Select</option>
                                                    <?php
                                                    $categories = getCategoriesByID(); ?>
                                                    @foreach($categories as $ks=>$s)
                                                        <option value="{{ $ks }}" {{ (!empty(old('product_category')) && old('product_category')==$ks)  ? 'selected' : ((($product) && ($product->product_category == $ks)) ? 'selected' : '') }}>
                                                            {{ $s }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('product_category'))
                                                    <span class="text-danger help-block">{{ $errors->first('product_category') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="hf-email" class="form-control-label">Brand</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select name="product_brand" id="product_brand" class="form-control">
                                                    <option value="">Select</option>
                                                    <?php
                                                    $brands = getBrandsByID(); ?>
                                                    @foreach($brands as $ks=>$s)
                                                        <option value="{{ $ks }}" {{ (!empty(old('product_brand')) && old('product_brand')==$ks)  ? 'selected' : ((($product) && ($product->product_brand == $ks)) ? 'selected' : '') }}>
                                                            {{ $s }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('product_brand'))
                                                    <span class="text-danger help-block">{{ $errors->first('product_brand') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="hf-email" class=" form-control-label">Product name</label>
                                            </div>
                                            <div class="col-12 col-md-9">

                                                <input class="form-control" id="product_name" type="text"
                                                       placeholder="Product name"
                                                       name="product_name"
                                                       value="{{ !empty(old('product_name')) ? old('product_name') : ((($product) && ($product->product_name)) ? $product->product_name : '') }}">
                                                @if ($errors->has('product_name'))
                                                    <span class="text-danger help-block">{{ $errors->first('product_name') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="hf-email" class="form-control-label">Image</label>
                                            </div>
                                            <div class="col-12 col-md-9">

                                                <input class="form-control {{ (($product) && ($product->product_image)) ? '' : 'productimage'}}"
                                                       id="productimage" type="file" name="product_image" />
                                                <small>Size: 800/600</small>
                                                @if ($errors->has('product_image'))
                                                    <span class="text-danger">{{ $errors->first('product_image') }}</span>
                                                @endif

                                                @if(($product) && ($product->product_image))
                                                    <div class="imagediv{{ $product->product_id }}">
                                                        <img src="/uploads/products/thumbs/{{$product->product_image}}" width="50"/>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="hf-email" class="form-control-label">gallery images</label>
                                            </div>
                                            <div class="col-12 col-md-9">

                                                <input class="form-control" id="productimage" type="file"
                                                       name="productimages[]" multiple />
                                                <small>Size: 800/600</small>
                                                @if ($errors->has('productimages[]'))
                                                    <span class="text-danger">{{ $errors->first('productimages[]') }}</span>
                                                @endif

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        @if(count($product_images)>0)
                                                            @foreach($product_images as $item)
                                                                <div class="col-md-2 imagediv{{ $item->pi_id }}">
                                                                    <img width="50" class="img-responsive" src="/uploads/products/thumbs/{{ $item->pi_image_name }}"/>
                                                                    <a href="#" id="{{$item->pi_id}}"
                                                                       class="delete_product_image btn btn-danger btn-xs"
                                                                       onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                                                                class="fa fa-trash-o" aria-hidden="true"></i>Delete </a>
                                                                </div>
                                                            @endforeach
                                                        @else
                                                            <div class="col-md-2">
                                                                No records found
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="hf-email" class=" form-control-label">Product Type</label>
                                            </div>
                                            <div class="col-12 col-md-9">

                                                <input class="form-control" id="product_type" type="text"
                                                       placeholder="Product Type"
                                                       name="product_type"
                                                       value="{{ !empty(old('product_type')) ? old('product_type') : ((($product) && ($product->product_type)) ? $product->product_type : '') }}">
                                                @if ($errors->has('product_type'))
                                                    <span class="text-danger help-block">{{ $errors->first('product_type') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="hf-email" class=" form-control-label">Product
                                                    Availability</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select name="product_availability" id="product_availability"
                                                        class="form-control">
                                                    <?php
                                                    $availability = availability(); ?>
                                                    @foreach($availability as $ks=>$s)
                                                        <option value="{{ $ks }}" {{ (!empty(old('product_availability')) && old('product_availability')==$ks)  ? 'selected' : ((($product) && ($product->product_availability == $ks)) ? 'selected' : '') }}
                                                        >
                                                            {{ $s }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('product_availability'))
                                                    <span class="text-danger help-block">{{ $errors->first('product_availability') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="hf-email" class=" form-control-label">Product Color</label>
                                            </div>
                                            <div class="col-12 col-md-9">

                                                <input class="form-control tags" id="product_color" type="text"
                                                       placeholder="Product Color"
                                                       name="product_color"
                                                       value="{{ !empty(old('product_color')) ? old('product_color') : ((($product) && ($product->product_color)) ? $product->product_color : '') }}">
                                                @if ($errors->has('product_color'))
                                                    <span class="text-danger help-block">{{ $errors->first('product_color') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="hf-email" class=" form-control-label">Product Size (Capacity/Mg)</label>
                                            </div>
                                            <div class="col-12 col-md-9">

                                                <input class="form-control tags" id="product_size" type="text"
                                                       placeholder="Product Color"
                                                       name="product_size"
                                                       value="{{ !empty(old('product_size')) ? old('product_size') : ((($product) && ($product->product_size)) ? $product->product_size : '') }}">
                                                @if ($errors->has('product_size'))
                                                    <span class="text-danger help-block">{{ $errors->first('product_size') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="hf-email" class=" form-control-label">Product Real
                                                    Price</label>
                                            </div>
                                            <div class="col-12 col-md-9">

                                                <input class="form-control" id="product_real_price" type="text"
                                                       placeholder="Product Real Price"
                                                       name="product_real_price"
                                                       value="{{ !empty(old('product_real_price')) ? old('product_real_price') : ((($product) && ($product->product_real_price)) ? $product->product_real_price : '') }}">
                                                @if ($errors->has('product_real_price'))
                                                    <span class="text-danger help-block">{{ $errors->first('product_real_price') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="hf-email" class=" form-control-label">Product Offer
                                                    Price</label>
                                            </div>
                                            <div class="col-12 col-md-9">

                                                <input class="form-control" id="product_price" type="text"
                                                       placeholder="Product Price"
                                                       name="product_price"
                                                       value="{{ !empty(old('product_price')) ? old('product_price') : ((($product) && ($product->product_price)) ? $product->product_price : '') }}">
                                                @if ($errors->has('product_color'))
                                                    <span class="text-danger help-block">{{ $errors->first('product_price') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="hf-email" class=" form-control-label">Product Shipping
                                                    Price</label>
                                            </div>
                                            <div class="col-12 col-md-9">

                                                <input class="form-control" id="product_shipping_price" type="text"
                                                       placeholder="Product Shipping Price"
                                                       name="product_shipping_price"
                                                       value="{{ !empty(old('product_shipping_price')) ? old('product_shipping_price') : ((($product) && ($product->product_shipping_price)) ? $product->product_shipping_price : 0) }}">
                                                @if ($errors->has('product_shipping_price'))
                                                    <span class="text-danger help-block">{{ $errors->first('product_shipping_price') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row form-group">

                                            <div class="col-md-6 ">

                                                <input class="form-control" id="product_is_best" type="checkbox"
                                                       name="product_is_best"
                                                       value="1" {{ (!empty(old('product_is_best')) && old('product_is_best')==1)  ? 'selected' : ((($product) && ($product->product_is_best == 1)) ? 'checked' : '') }}>

                                                Is a best product ?
                                            </div>

                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="hf-email" class=" form-control-label">Description</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                        <textarea name="product_description" id="product_description" rows="15"
                                                  placeholder="Description..."
                                                  class="form-control ckeditor">{!! !empty(old('product_description')) ? old('product_description') : ((($product) && ($product->product_description)) ? $product->product_description : '') !!}</textarea>
                                                @if ($errors->has('product_description'))
                                                    <span class="text-danger help-block">{{ $errors->first('product_description') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="hf-email" class=" form-control-label">Status</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select name="product_status" id="product_status" class="form-control">
                                                    <?php
                                                    $status = allStatuses('general'); ?>
                                                    @foreach($status as $ks=>$s)
                                                        <option value="{{ $ks }}" {{ (!empty(old('product_status')) && old('product_status')==$ks)  ? 'selected' : ((($product) && ($product->product_status == $ks)) ? 'selected' : '') }}
                                                        >
                                                            {{ $s }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('product_status'))
                                                    <span class="text-danger help-block">{{ $errors->first('product_status') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-primary btn-sm">
                                                <i class="fa fa-dot-circle-o"></i> Submit
                                            </button>
                                            <button type="reset" class="btn btn-danger btn-sm">
                                                <i class="fa fa-ban"></i> Reset
                                            </button>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

    <script src="{{url('backend/modules/products/js/products.js')}}"></script>

    <script src="https://cdn.ckeditor.com/ckeditor5/11.1.1/classic/ckeditor.js"></script>
    <script>
        $(function () {
            $('.tags').tagsInput({width:'auto'});
        });

        ClassicEditor
            .create(document.querySelector('.ckeditor'), {
                language: 'zh-cn',
                image: {
                    toolbar: ['imageTextAlternative', '|', 'imageStyle:alignLeft', 'imageStyle:full', 'imageStyle:alignRight'],
                    styles: ['full', 'side', 'alignLeft',

                        // This represents an image aligned to the right.
                        'alignRight']
                },
                ckfinder: {
                    uploadUrl: '{{route('editorImageupload')}}'
                }
            })
            .then(editor => {
                editor.ui.view.editable.editableElement.style.height = '300px';
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
    </script>
    {{--<script src="/vendor/jezielmartins/laravel-ckeditor/ckeditor.js"></script>--}}
    {{--<script src="/vendor/jezielmartins/laravel-ckeditor/adapters/jquery.js"></script>--}}
    <script>
    {{--$(function () {--}}
    {{--$('.ckeditor').ckeditor();--}}
    {{--})--}}
    {{--// $('.textarea').ckeditor(); // if class is prefered.--}}
    {{--</script>--}}
    {{--<script>--}}

    {{--$(function () {--}}
    {{--ClassicEditor--}}
    {{--.create( document.querySelector( '.ckeditor' ) )--}}
    {{--.then( editor => {--}}
    {{--// Store it in more "global" context.--}}
    {{--appEditor = editor;--}}
    {{--} )--}}
    {{--.catch( error => {--}}
    {{--console.error( error );--}}
    {{--} );--}}
    {{--})--}}

    {{--// let appEditor;--}}

    {{--// ClassicEditor--}}
    {{--//     .create( document.querySelector( '.ckeditor' ) )--}}
    {{--//     .then( editor => {--}}
    {{--//         window.editor = editor;--}}
    {{--//     } )--}}
    {{--//     .catch( err => {--}}
    {{--//         console.error( err.stack );--}}
    {{--//     } );--}}
    {{--</script>--}}

@endsection