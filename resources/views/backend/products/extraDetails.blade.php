@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

    <style>

        .productview-body ol, .productview-body ol li, .productview-body ul, .productview-body ul li {
            list-style: inherit;
        }

        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            display: none;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #3c8dbc;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>

@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              'products'=>'Products',
              ''=>'Add Product Information'
              ),'Add Product Information'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif

                    @include('backend.products.nav')
                    <div class="tab-content">
                        <div class="tab-pane fade show active">
                            <div class="card">
                                <div class="card-header">
                                    <strong class="card-title">Products</strong>
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-primary float-right" data-toggle="modal"
                                            data-target="#addProducts">
                                        Add Extra Information
                                    </button>

                                    <!-- Modal -->
                                    <div class="modal fade" id="addProducts" tabindex="-1" role="dialog"
                                         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLongTitle">Add Product Extra
                                                        Information</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form method="post" id="addProductExtraInfo"
                                                      action="{{ route('addProductExtraInfo',['id'=>$product_id]) }}"
                                                      class="form-horizontal addProductExtraInfo"
                                                      enctype="multipart/form-data">
                                                    <div class="modal-body">

                                                        {{ csrf_field() }}

                                                        <input type="hidden" name="pe_id"/>

                                                        <div class="row form-group">
                                                            <div class="col col-md-3">
                                                                <label for="hf-email"
                                                                       class=" form-control-label">Title</label>
                                                            </div>
                                                            <div class="col-12 col-md-9">
                                                                <input class="form-control" id="pe_title" type="text"
                                                                       placeholder="Product Title"
                                                                       name="pe_title">
                                                                @if ($errors->has('pe_title'))
                                                                    <span class="text-danger help-block">{{ $errors->first('pe_title') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <div class="row form-group">
                                                            <div class="col col-md-3">
                                                                <label for="hf-email"
                                                                       class=" form-control-label">Position</label>
                                                            </div>
                                                            <div class="col-12 col-md-9">
                                                                {{--<input class="form-control" id="pe_position" type="text"--}}
                                                                {{--placeholder="Product position"--}}
                                                                {{--name="pe_position">--}}

                                                                <select class="form-control" name="pe_position">
                                                                    <option>Select</option>

                                                                    @foreach(productDescPosottion() as $key=>$item)
                                                                        <option value="{{$key}}">{{$item}}</option>
                                                                    @endforeach
                                                                </select>

                                                                @if ($errors->has('pe_position'))
                                                                    <span class="text-danger help-block">{{ $errors->first('pe_position') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <div class="row form-group">
                                                            <div class="col col-md-3">
                                                                <label for="hf-email"
                                                                       class="form-control-label">Image</label>
                                                            </div>
                                                            <div class="col-12 col-md-9">

                                                                <input class="form-control productExtraimage"
                                                                       id="pe_image" type="file" name="pe_image">
                                                                @if ($errors->has('pe_image'))
                                                                    <span class="text-danger">{{ $errors->first('pe_image') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <div class="row form-group">
                                                            <div class="col col-md-3">
                                                                <label for="hf-email"
                                                                       class=" form-control-label">Description</label>
                                                            </div>
                                                            <div class="col-12 col-md-9">
                                                         <textarea name="pe_description" id="pe_description"
                                                                   rows="9"
                                                                   placeholder="Description..."
                                                                   class="form-control ckeditor"></textarea>
                                                                @if ($errors->has('pe_description'))
                                                                    <span class="text-danger help-block">{{ $errors->first('pe_description') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <div class="row form-group">
                                                            <div class="col col-md-3">
                                                                <label for="hf-email"
                                                                       class=" form-control-label">Status</label>
                                                            </div>
                                                            <div class="col-12 col-md-9">
                                                                <select name="pe_status" id="pe_status"
                                                                        class="form-control">
                                                                    <?php
                                                                    $status = allStatuses('general'); ?>
                                                                    @foreach($status as $ks=>$s)
                                                                        <option value="{{ $ks }}">
                                                                            {{ $s }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                                @if ($errors->has('pe_status'))
                                                                    <span class="text-danger help-block">{{ $errors->first('pe_status') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">
                                                            Close
                                                        </button>
                                                        <input type="submit" class="btn btn-primary" value="Submit"/>

                                                        {{--<button type="submit" class="btn btn-primary">Submit</button>--}}
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Title</th>
                                            <th scope="col">Image</th>
                                            <th scope="col">Position</th>
                                            {{--<th scope="col">Description</th>--}}
                                            <th scope="col">Status</th>
                                            <th scope="col">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($product_details)>0)
                                            @foreach($product_details as $item)
                                                <tr>
                                                    <th scope="row">{{ $loop->iteration }}</th>
                                                    <td>{{ $item->pe_title }}</td>
                                                    <td>
                                                        <img width="50" src="/uploads/products/{{ $item->pe_image }}"
                                                             class="img-fluid" alt="" title="">

                                                    </td>
                                                    <td>{{ productDescPosottion($item->pe_position) }}</td>
{{--                                                    <td>{!! $item->pe_description !!}</td>--}}
                                                    <td>{{ allStatuses('general',$item->pe_status) }}</td>
                                                    <td>
                                                        <div class="dropdown">
                                                            <a class="btn btn-outline-primary dropdown-toggle" href="#"
                                                               role="button"
                                                               data-toggle="dropdown">
                                                                <i class="fa fa-ellipsis-h"></i>
                                                            </a>
                                                            <div class="dropdown-menu dropdown-menu-right">
                                                                <!-- Button trigger modal -->

                                                                <a href="#" class="dropdown-item"
                                                                   data-toggle="modal"
                                                                   data-target="#editProducts{{ $item->pe_id }}"><i
                                                                            class="fa fa-pencil"></i> Edit</a>
                                                                <form method="POST" id="products"
                                                                      action="{{ route('productExtraInfo',['id'=>$item->pe_product_id]) }}"
                                                                      accept-charset="UTF-8" class="form-horizontal"
                                                                      style="display:inline">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name="pe_id"
                                                                           value="{{ $item->pe_id }}"/>
                                                                    <button type="submit" class="dropdown-item"
                                                                            title="Delete Product"
                                                                            onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                                        <i
                                                                                class="fa fa-trash"></i> Delete
                                                                    </button>

                                                                </form>
                                                            </div>
                                                        </div>

                                                        <!-- Modal -->
                                                        <div class="modal fade" id="editProducts{{ $item->pe_id }}"
                                                             tabindex="-1" role="dialog"
                                                             aria-labelledby="exampleModalCenterTitle"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog modal-dialog-centered modal-lg"
                                                                 role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title"
                                                                            id="exampleModalLongTitle{{ $item->pe_id }}">
                                                                            Add
                                                                            Product Extra
                                                                            Information</h5>
                                                                        <button type="button" class="close"
                                                                                data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form method="POST" class="addProductExtraInfo"
                                                                          action="{{ route('addProductExtraInfo',['id'=>$product_id]) }}"
                                                                          accept-charset="UTF-8" class="form-horizontal"
                                                                          enctype="multipart/form-data">
                                                                        <div class="modal-body">

                                                                            {{ csrf_field() }}

                                                                            <input type="hidden" name="pe_id"
                                                                                   value="{{ $item->pe_id }}"/>

                                                                            <div class="row form-group">
                                                                                <div class="col col-md-3">
                                                                                    <label for="hf-email"
                                                                                           class=" form-control-label">Title</label>
                                                                                </div>
                                                                                <div class="col-12 col-md-9">
                                                                                    <input class="form-control"
                                                                                           type="text"
                                                                                           placeholder="Product Title"
                                                                                           name="pe_title"
                                                                                           value="{{ !empty(old('pe_title')) ? old('pe_title') : ((($item) && ($item->pe_title)) ? $item->pe_title : '') }}">
                                                                                    @if ($errors->has('pe_title'))
                                                                                        <span class="text-danger help-block">{{ $errors->first('pe_title') }}</span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>

                                                                            <div class="row form-group">
                                                                                <div class="col col-md-3">
                                                                                    <label for="hf-email"
                                                                                           class=" form-control-label">Position</label>
                                                                                </div>
                                                                                <div class="col-12 col-md-9">

                                                                                    <select name="pe_position" class="form-control">
                                                                                        <option>Select</option>
                                                                                        @foreach(productDescPosottion() as $po_key => $value)
                                                                                            <option value="{{$po_key}}" {{ (!empty(old('pe_position')) && old('pe_position')==$po_key)  ? 'selected' : ((($item) && ($item->pe_position == $po_key)) ? 'selected' : '') }} >{{ $value  }}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                    {{--@if (count(productDescPosottion()))--}}
                                                                                    {{--<select name="pe_position" class="form-control">--}}
                                                                                    {{--@foreach(productDescPosottion() as $key => $item)--}}
                                                                                    {{--<option>asdasd</option>--}}
                                                                                    {{--@endforeach--}}
                                                                                    {{--</select>--}}
                                                                                    {{--@else--}}
                                                                                    {{--<input class="form-control"--}}
                                                                                           {{--type="text"--}}
                                                                                           {{--placeholder="Product position"--}}
                                                                                           {{--name="pe_position"--}}
                                                                                           {{--value="{{ !empty(old('pe_position')) ? old('pe_position') : ((($item) && ($item->pe_position)) ? $item->pe_position : '') }}">--}}

                                                                                    {{--@endif--}}

                                                                                    @if ($errors->has('pe_position'))
                                                                                        <span class="text-danger help-block">{{ $errors->first('pe_position') }}</span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>

                                                                            <div class="row form-group">
                                                                                <div class="col col-md-3">
                                                                                    <label for="hf-email"
                                                                                           class="form-control-label">Image</label>
                                                                                </div>
                                                                                <div class="col-12 col-md-9">

                                                                                    <input class="form-control {{ (($item) && ($item->pe_image)) ? '' : 'productExtraimage'}}"
                                                                                           type="file" name="pe_image"
                                                                                           id="productExtraimage{{ $item->pe_id }}">
                                                                                    @if ($errors->has('pe_image'))
                                                                                        <span class="text-danger">{{ $errors->first('pe_image') }}</span>
                                                                                    @endif
                                                                                    @if(($item) && ($item->pe_image))
                                                                                        <div class="imagediv{{ $item->pe_id }}">
                                                                                            <img width="100"
                                                                                                 src="/uploads/products/thumbs/{{$item->pe_image}}"/>
                                                                                            {{--<a href="#"--}}
                                                                                            {{--id="{{$item->pe_id}}"--}}
                                                                                            {{--class="delete_productExtra_image btn btn-danger btn-xs"--}}
                                                                                            {{--onclick="return confirm(&quot;Confirm delete?&quot;)"><i--}}
                                                                                            {{--class="fa fa-trash-o"--}}
                                                                                            {{--aria-hidden="true"></i>Delete--}}
                                                                                            {{--</a>--}}
                                                                                        </div>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                            <div class="row form-group">
                                                                                <div class="col col-md-3">
                                                                                    <label for="hf-email"
                                                                                           class=" form-control-label">Description</label>
                                                                                </div>
                                                                                <div class="col-12 col-md-9">
                                                         <textarea name="pe_description"
                                                                   rows="9"
                                                                   placeholder="Description..."
                                                                   class="form-control ckeditor">{{ !empty(old('pe_description')) ? old('pe_description') : ((($item) && ($item->pe_description)) ? $item->pe_description : '') }}</textarea>
                                                                                    @if ($errors->has('pe_description'))
                                                                                        <span class="text-danger help-block">{{ $errors->first('pe_description') }}</span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>

                                                                            <div class="row form-group">
                                                                                <div class="col col-md-3">
                                                                                    <label for="hf-email"
                                                                                           class=" form-control-label">Status</label>
                                                                                </div>
                                                                                <div class="col-12 col-md-9">
                                                                                    <select name="pe_status"
                                                                                            class="form-control">
                                                                                        <?php
                                                                                        $status = allStatuses('general'); ?>
                                                                                        @foreach($status as $ks=>$s)
                                                                                            <option value="{{ $ks }}" {{ (!empty(old('pe_status')) && old('pe_status')==$ks)  ? 'selected' : ((($item) && ($item->pe_status == $ks)) ? 'selected' : '') }}>
                                                                                                {{ $s }}
                                                                                            </option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                    @if ($errors->has('pe_status'))
                                                                                        <span class="text-danger help-block">{{ $errors->first('pe_status') }}</span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button"
                                                                                    class="btn btn-secondary"
                                                                                    data-dismiss="modal">
                                                                                Close
                                                                            </button>
                                                                            <button type="submit"
                                                                                    class="btn btn-primary">
                                                                                Submit
                                                                            </button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="7">No records found</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-10">
                    <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                         aria-live="polite">Showing {{ $product_details->firstItem() }}
                        to {{ $product_details->lastItem() }} of {{ $product_details->total() }} entries
                    </div>
                </div>
                <div class="col-md-1 text-right">
                    <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                        {!! $product_details->appends(['filters' => Request::get('filters'),'search' => Request::get('search'),'status' => Request::get('pe_status')])->render() !!}
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')
    <script src="{{url('backend/modules/products/js/products.js')}}"></script>

    <script src="https://cdn.ckeditor.com/ckeditor5/11.1.1/classic/ckeditor.js"></script>
    <script>

        $('.ckeditor').each(function (e) {
            ClassicEditor
                .create(this, {
                    language: 'zh-cn',
                    image: {
                        toolbar: ['imageTextAlternative', '|', 'imageStyle:alignLeft', 'imageStyle:full', 'imageStyle:alignRight'],
                        styles: ['full', 'side', 'alignLeft',

                            // This represents an image aligned to the right.
                            'alignRight']
                    },
                    ckfinder: {
                        uploadUrl: '{{route('editorImageupload')}}'
                    }
                })
                .then(editor => {
                    editor.ui.view.editable.editableElement.style.height = '300px';
                    console.log(editor);
                })
                .catch(error => {
                    console.error(error);
                });
        });

        // $('.ckeditor').each(function () {

        // });
    </script>
@endsection