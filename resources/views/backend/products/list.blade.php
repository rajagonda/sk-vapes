@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            display: none;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #3c8dbc;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>

@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              ''=>'Products'
              ),'Products'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif

                    <form method="GET" action="{{ route('products') }}" accept-charset="UTF-8"
                          class="navbar-form navbar-right" role="search">
                        <input type="hidden" name="search" value="search">

                        <div class="row">
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="product_name"
                                       placeholder="Enter product name to Search..."
                                       value="{{ app('request')->input('product_name') }}">
                            </div>
                            <div class="col-md-3">
                                <select name="search_category" id="search_category"
                                        class="form-control">
                                    <option value="">Select</option>
                                    <?php
                                    $categories = getCategories(); ?>
                                    @foreach($categories as $ks=>$s)
                                        <option value="{{ $ks }}" {{ (app('request')->input('search_category')==$ks)  ? 'selected' : ''}}>
                                            {{ $s }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-1">
                                             <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>

                    </form>

                    <br/>
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Products</strong>
                            <a href="{{route('addNewProducts')}}" class="btn btn-primary btn-xs" style="float:right;">Add
                                Products
                            </a>

                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Image</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Category</th>
                                    <th scope="col">Type</th>
                                    <th scope="col">Avbailability</th>
                                    {{--<th scope="col">Color</th>--}}
                                    <th scope="col">Price</th>
                                    {{--<th scope="col">Shipping Price</th>--}}
                                    {{--<th scope="col">Description</th>--}}
                                    <th scope="col">Country</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($products)>0)
                                    @foreach($products as $item)
                                        <tr>
                                            <th scope="row">{{ $loop->iteration }}</th>
                                            <td>
                                                @if ($item->product_image)
                                                    <img width="50"
                                                         src="{{ '/uploads/products/thumbs/'.$item->product_image }}"/>
                                                @else
                                                    ---
                                                @endif
                                            </td>
                                            <td>{{ $item->product_name }}</td>
                                            <td>{{ (isset($item->getCategory))?$item->getCategory->category_name:'' }}</td>
                                            <td>{{ $item->product_type }}</td>
                                            <td>{{ availability($item->product_availability) }}</td>
                                            {{--                                            <td>{{ $item->product_color }}</td>--}}
                                            <td>{{ $item->product_price }}</td>
                                            {{--                                            <td>{{ $item->product_shipping_price }}</td>--}}
                                            {{--                                            <td>{!! $item->product_description !!}</td>--}}
                                            <td>{{ $item->getCountry->country_name }}</td>
                                            <td>{{ allStatuses('general',$item->product_status) }}</td>
                                            <td>
                                                <div class="dropdown">
                                                    <a class="btn btn-outline-primary dropdown-toggle" href="#"
                                                       role="button"
                                                       data-toggle="dropdown">
                                                        <i class="fa fa-ellipsis-h"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        {{--<a class="dropdown-item"--}}
                                                        {{--href="{{ route('addProductImages',['id'=>$item->product_id]) }}"><i--}}
                                                        {{--class="fa fa-plus"></i> Product Images</a>--}}
                                                        {{--<a class="dropdown-item"--}}
                                                        {{--href="{{ route('productExtraInfo',['id'=>$item->product_id]) }}"><i--}}
                                                        {{--class="fa fa-plus"></i> Add Product Intro</a>--}}
                                                        <a class="dropdown-item"
                                                           href="{{ route('addNewProducts',['id'=>$item->product_id]) }}"><i
                                                                    class="fa fa-pencil"></i> Edit</a>
                                                        <form method="POST" id="products"
                                                              action="{{ route('products') }}"
                                                              accept-charset="UTF-8" class="form-horizontal"
                                                              style="display:inline">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="product_id"
                                                                   value="{{ $item->product_id }}"/>
                                                            <button type="submit" class="dropdown-item"
                                                                    title="Delete Product"
                                                                    onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                                <i
                                                                        class="fa fa-trash"></i> Delete
                                                            </button>

                                                        </form>

                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="12">No records found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-10">
                    <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                         aria-live="polite">Showing {{ $products->firstItem() }}
                        to {{ $products->lastItem() }} of {{ $products->total() }} entries
                    </div>
                </div>
                <div class="col-md-1 text-right">
                    <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                        {!! $products->appends(['search' => Request::get('search'),'product_name' => Request::get('product_name'),'search_category' => Request::get('search_category')])->render() !!}
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

@endsection