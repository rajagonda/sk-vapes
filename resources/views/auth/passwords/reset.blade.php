<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SK Vapes</title>
    @include('frontend._partials.stylesheets')
</head>

<body>
<!-- sign in -->
<section class="signin">
    <div class="signbgcol text-center">
        <article class="mx-auto">
            <h3 class="fwhite text-uppercase h4 flight">{{ __('message.login.welcome') }}</h3>
            <h1 class="fbold fwhite h1">SK VAPES</h1>
            <p class="fwhite">{{ __('message.login.subtext') }}</p>
        </article>
    </div>
    <!-- sign right section -->
    <div class="signright">
        <div class="signrtin mx-auto">
            <div class="signheader text-center">
                <a href="{{ route('home') }}" class="d-none d-sm-block"><img class="signbrand" src="/frontend/img/logo.png" alt="" title=""></a>
                <h3 class="h3 flight py-1">Reset Your Password</h3>
            </div>
            <form method="POST" action="{{ route('password.update') }}">
                {{ csrf_field() }}

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                    <div class="col-md-6">
                        <input id="email" type="email"
                               class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                               value="{{ $email ?? old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                    <div class="col-md-6">
                        <input id="password" type="password"
                               class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
                               required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password-confirm"
                           class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                               required>
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn signbtn fmed">
                            {{ __('Reset Password') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <!--/ sign social -->
    </div>
    <!--/ sign right section -->
</section>
<!--/ sign in -->

@auth
@else
    @if (empty(session('restrictData')))
        <!-- on page loading windows -->
        @include('frontend._partials.ageRuleModel')
    @endif
@endauth

@include('frontend._partials.scripts')
</body>

</html>
