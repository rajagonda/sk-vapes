<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SK Vapes</title>
    @include('frontend._partials.stylesheets')
</head>

<body>
<!-- sign in -->
<section class="signin">
    <div class="signbgcol text-center">
        <article class="mx-auto">
            <h3 class="fwhite text-uppercase h4 flight">{{ __('message.login.welcome') }}</h3>
            <h1 class="fbold fwhite h1">SK VAPES</h1>
            <p class="fwhite">{{ __('message.login.subtext') }}</p>
        </article>
    </div>
    <!-- sign right section -->
    <div class="signright">
        <div class="signrtin mx-auto">
            <div class="signheader text-center">
                <a href="{{ route('home') }}"><img class="signbrand" src="/frontend/img/logo.png" alt="" title=""></a>
                <h3 class="h3 flight py-1">Forgot Password</h3>
            </div>
            @if (session('status')=='passwords.sent')
                <div class="alert alert-success" role="alert">
                    Please Check your email to reset the password
                </div>
            @endif
            <form class="formsign" method="POST" action="{{ route('password.email') }}">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="email" class="label text-uppercase">{{ __('E-Mail Address') }}</label>

                    <div class="">
                        <input id="email" type="email"
                               class="form-control mt-2{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                               value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group text-right">
                    <a href="{{ route('userelogin') }}"> Back to Login</a>
                </div>

                <div class="form-group row mb-0 justify-content-center">
                    <div class="col-md-6 text-center">
                        <button type="submit" class="btn signbtn fmed ">
                            {{ __('Reset Password') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <!--/ sign social -->
    </div>
    <!--/ sign right section -->
</section>
<!--/ sign in -->

@auth
@else
    @if (empty(session('restrictData')))
        <!-- on page loading windows -->
        @include('frontend._partials.ageRuleModel')
    @endif
@endauth

@include('frontend._partials.scripts')
</body>

</html>
