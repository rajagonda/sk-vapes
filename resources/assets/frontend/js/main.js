require('../../../js/bootstrap');
require('jquery-validation');
// require('bootstrap.bundle.min');
require('./easyResponsiveTabs');
require('./html5-shiv');
require('./jquery-steps');
require('./jquery.basictable.min');
// require('./popper.min');
require('./simplegallery.min');
require('./vertical_carousal.min');
// require('./notifIt');
require('bootstrap4-notify');
// require('bootstrap-datepicker/dist/js/bootstrap-datepicker.min');
// require('datepicker-bootstrap/js/datepicker.min');
require('@fortawesome/fontawesome-free/js/all.min');

require('jquery-ui/ui/widgets/autocomplete');

//jquery-ui
require('./wowslider');
require ('jquery-ui/ui/widgets/datepicker');
   $(document).ready(function() {
        $('#table').basictable();

        $('#table-breakpoint').basictable({
            breakpoint: 768
        });

        $('#table-container-breakpoint').basictable({
            containerBreakpoint: 485
        });

        $('#table-swap-axis').basictable({
            swapAxis: true
        });

        $('#table-force-off').basictable({
            forceResponsive: false
        });

        $('#table-no-resize').basictable({
            noResize: true
        });

        $('#table-two-axis').basictable();

        $('#table-max-height').basictable({
            tableWrapper: true
        });
    });

