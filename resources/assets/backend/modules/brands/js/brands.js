jQuery(document).ready(function () {
    $('.delete_brand_image').on('click', function () {
        var image = $(this).attr('id');
        if (image != '') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                url: '/admin/ajax/deletebrandimage',
                type: 'POST',
                data: {'image': image},
                success: function (response) {
                    $('.imagediv' + image).remove();
                    $('#brandimage').addClass('brandimage');
                    imageRules();
                }
            });
        }
    });
    jQuery('#brands').validate({
        ignore: [],
        errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
        errorElement: 'div',
        errorPlacement: function (error, e) {
            e.parents('.form-group').append(error);
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
            $(e).closest('.text-danger').remove();
        },
        success: function (e) {
            // You can use the following if you would like to highlight with green color the input after successful validation!
            e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
            e.closest('.text-danger').remove();
        },
        rules: {
            brand_name: {
                required: true
            },
            brand_catid: {
                required: true
            },
            brand_country: {
                required: true
            },
            brand_status: {
                required: true
            }
        },
        messages: {
            brand_name: {
                required: 'Enter brand name'
            },
            brand_catid: {
                required: 'Enter category name'
            },
            brand_country: {
                required: 'Please select country'
            },
            brand_status: {
                required: 'Please select status'
            }
        },
    });
    imageRules();
});

function imageRules() {
    $(".brandimage").rules("add", {
        required: true,
        messages: {
            required: "Upload image to save"
        }
    });
}