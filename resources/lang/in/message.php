<?php
/**
 * Created by PhpStorm.
 * User: idream
 * Date: 25-Oct-18
 * Time: 1:59 PM
 */
// Header strings

return [

    // Home page

    'header.welcome' => 'WELCOME TO SKVAPES',
    'header.signin' => 'Sign In',
    'header.cart' => 'Cart',
    // Login page

    'login.signinInfo' => 'Enter your details below',
    'login.email' => 'Email Adress',
    'login.password' => 'Password',
    'login.forgotpassword' => 'Forgot Password',
    'login.login' => 'Login',
    'login.signup' => 'Sign Up',
    'login.account' => 'Don\'t Have an Account?',
    'login.welcome' => 'Welcome to',
    'login.subtext' => 'Liquids & Accessories',

];
