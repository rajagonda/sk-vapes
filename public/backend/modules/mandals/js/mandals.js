/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 56);
/******/ })
/************************************************************************/
/******/ ({

/***/ 56:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(57);
/***/ }),

/***/ 57:
/***/ (function(module, exports) {

jQuery(document).ready(function () {
    $('.delete_mandal_image').on('click', function () {

        var image = $(this).attr('id');
        if (image != '') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                url: '/admin/ajax/deletemandalimage',
                type: 'POST',
                data: { 'image': image },
                success: function success(response) {
                    $('.imagediv' + image).remove();
                    $('#mandalimage').addClass('mandalimage');
                    imageRules();
                }
            });
        }
    });

    $('#mandal_state_id').on('change', function () {
        var state = $('#mandal_state_id').val();
        $('#mandal_division_id').children('option').remove();
        if (state != '') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                url: '/admin/ajax/getDivisions',
                type: 'POST',
                data: { 'state': state },
                success: function success(response) {
                    var resultdata = $.parseJSON(response);
                    var length = Object.keys(resultdata.divisions).length;
                    if (length > 0) {
                        $('#mandal_division_id').append(new Option('Select', '', false, false));
                        $.each(resultdata.divisions, function (k, v) {
                            if ($('.mandal_division_id').val() == k) {
                                $('#mandal_division_id').append(new Option(v, k, true, true));
                            } else {
                                $('#mandal_division_id').append(new Option(v, k, false, false));
                            }
                        });
                    } else {
                        $('#mandal_division_id').append(new Option('There is no divisions', '', false, false));
                    }
                }
            });
        } else {
            $('#mandal_division_id').append(new Option('Select state first', '', false, false));
        }
    }).change();

    jQuery('#mandals').validate({
        ignore: [],
        errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
        errorElement: 'div',
        errorPlacement: function errorPlacement(error, e) {
            e.parents('.form-group').append(error);
        },
        highlight: function highlight(e) {
            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
            $(e).closest('.text-danger').remove();
        },
        success: function success(e) {
            // You can use the following if you would like to highlight with green color the input after successful validation!
            e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
            e.closest('.text-danger').remove();
        },
        rules: {
            mandal_state_id: {
                required: true
            },
            mandal_division_id: {
                required: true
            },
            mandal_name: {
                required: true
            },
            mandal_status: {
                required: true
            }
        },
        messages: {
            mandal_state_id: {
                required: 'Please select state'
            },
            mandal_division_id: {
                required: 'Please select division'
            },
            mandal_name: {
                required: 'Please enter name'
            },
            mandal_status: {
                required: 'Please select status'
            }
        }
    });
    imageRules();
});
function imageRules() {
    $(".mandalimage").rules("add", {
        required: true,
        messages: {
            required: "Upload image to save"
        }
    });
}

/***/ })

/******/ });